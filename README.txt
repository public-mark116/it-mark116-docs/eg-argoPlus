Requisiti dell�applicazione web ARGO:
�	DBMS Postgress
�	Python V. 3
�	Django V. 1.11

Per poter essere installata l�applicazione web ARGO necessita di un database denominato �djangodb� accessibile mediante l�utente:
�	Username: djangouser
�	Password: djangopsw

I dati di accesso possono essere modificati all�interno del file: �\myProj\myProj\settings.py�.

Per eseguire l�installazione dell�applicazione sar� necessario avviare il file denominato �install.bat� dopo aver creato il sopracitato database e il relativo utente. Durante l�installazione verranno generate tutte le tabelle necessarie al funzionamento dell�applicazione e verranno inseriti alcuni dati di sistema. L�installazione si concluder� con la creazione dell�utente (con privilegi di superuser) che dovr� essere utilizzato per accedere al sistema.
Infine, sar� necessario aprire il file settings.py presente all�indirizzo �\myProj\myProj\settings.py�, identificare le costanti �ADMIN CONSTANTS� ed inserire i valori corretti:
�	EMAIL_FROM, contiene l�indirizzo che verr� utilizzato per spedire le email;
�	EMAIL_HOST, contiene l�indirizzo del server responsabile dell�invio delle email;
�	EMAIL_HOST_USER, contiene l�username di login al server;
�	EMAIL_HOST_PASSWORD, contiene la password di login al server;
�	EMAIL_PORT, contiene la porta da utilizzare per inviare le email;
�	EMAIL_USE_TLS, viene settato a True se il server di invio email utilizza il TSL.

Una volta completata l�installazione sar� possibile avviare l�applicazione eseguendo il file �execute.bat�.

All�interno della cartella �Dati Demo� � possibile reperire un file json (denominato �movie_plots - V5 Modificate Etichette.json�) contenente 400 task d�esempio per un campagna a sfondo cinematografico.


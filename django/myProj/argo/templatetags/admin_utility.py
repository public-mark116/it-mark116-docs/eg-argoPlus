from django import template

register = template.Library()

@register.simple_tag
def cleanString(str):
    str = str.replace(' ', '')
    str = str.replace('-', '')
    str = str.lower()
    
    return str
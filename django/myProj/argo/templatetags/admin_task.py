from django import template
from django.db import connection
from argo.models import *
import datetime
import json

register = template.Library()

@register.simple_tag
def getTaskAnswers(task_id):
    cursor = connection.cursor()
    cursor.execute('''SELECT answer, first_name, last_name FROM argo_worker_task INNER JOIN auth_user ON auth_user.id = argo_worker_task.worker_id WHERE argo_worker_task.task_id = ''' + str(task_id) + ''' ORDER BY answer''')
    result = cursor.fetchall()
    
    ret = []
    for r in result:
        raw = {}
        raw['answer_text']    = r[0]
        raw['worker_name']    = r[1]
        raw['worker_surname'] = r[2]
        raw['answer_num']     = Worker_Task.objects.filter(task_id=task_id).filter(answer=raw['answer_text']).count()
        ret.append(raw)

    return ret
from django import template
from django.db import connection
from argo.models import *
import datetime
import json

register = template.Library()

@register.simple_tag
def getWorkerCampaignLocalScore(workerCampaign_id):
    result = Worker_Campaign.objects.filter(id=workerCampaign_id)[0]
    return result.c_score
    
@register.simple_tag
def getWorkerCampaignLocalScoreCompareToOther(workerCampaign_id):
    worker_campaign = Worker_Campaign.objects.filter(id=workerCampaign_id)[0]
    
    num_workers     = Worker_Campaign.objects.filter(campaign_id=worker_campaign.campaign_id).exclude(id=workerCampaign_id).count()
    worker_score    = worker_campaign.c_score
    
    cursor = connection.cursor()
    cursor.execute('''SELECT COUNT(id) FROM argo_worker_campaign WHERE campaign_id = ''' + str(worker_campaign.campaign_id) + ''' AND c_score < ''' + str(worker_score))
    result_positive = cursor.fetchone()
    
    cursor = connection.cursor()
    cursor.execute('''SELECT COUNT(id) FROM argo_worker_campaign WHERE campaign_id = ''' + str(worker_campaign.campaign_id) + ''' AND c_score > ''' + str(worker_score))
    result_negative = cursor.fetchone()
    
    if num_workers==0:
        return (1, 0)
    elif result_negative!=None and result_negative[0]>0:
        return (-1, round(Decimal(result_negative[0]/(num_workers)*100), 2))
    else:
        return (1, round(Decimal(result_positive[0]/(num_workers)*100), 2))
   
@register.simple_tag
def getWorkerCampaignGlobalScore(workerCampaign_id):
    worker_campaign = Worker_Campaign.objects.filter(id=workerCampaign_id)[0]
    result          = Worker.objects.filter(id=worker_campaign.worker_id)[0]
    return result.w_score
    
@register.simple_tag
def getWorkerCampaignGlobalScoreCompareToOther(workerCampaign_id):
    worker_campaign = Worker_Campaign.objects.filter(id=workerCampaign_id)[0]
    
    num_workers   = Worker.objects.exclude(id=worker_campaign.worker_id).count()
    worker        = Worker.objects.filter(id=worker_campaign.worker_id)[0]
    worker_score  = worker.w_score
    
    
    cursor = connection.cursor()
    cursor.execute('''SELECT COUNT(id_id) FROM argo_worker WHERE w_score < ''' + str(worker_score))
    result_positive = cursor.fetchone()
    
    cursor = connection.cursor()
    cursor.execute('''SELECT COUNT(id_id) FROM argo_worker WHERE w_score > ''' + str(worker_score))
    result_negative = cursor.fetchone()
    
    if num_workers==0:
        return (1, 0)
    elif result_negative!=None and result_negative[0]>0:
        return (-1, round(Decimal(result_negative[0]/(num_workers)*100), 2))
    else:
        return (1, round(Decimal(result_positive[0]/(num_workers)*100), 2))
    
@register.simple_tag
def getWorkerCampaignNumOfAnswers(workerCampaign_id):
    worker_campaign = Worker_Campaign.objects.filter(id=workerCampaign_id)[0]
    result          = Worker_Task.objects.filter(worker_id=worker_campaign.worker_id).filter(task_id__campaign_id=worker_campaign.campaign_id).count()
    return result
    
@register.simple_tag
def getWorkerCampaignNumOfAnswersCompareToOther(workerCampaign_id):
    worker_campaign = Worker_Campaign.objects.filter(id=workerCampaign_id)[0]
    
    num_campaign_workers        = Worker_Campaign.objects.filter(campaign_id=worker_campaign.campaign_id).exclude(worker_id=worker_campaign.worker_id).count()
    num_campaign_worker_answers = Worker_Task.objects.filter(worker_id=worker_campaign.worker_id).filter(task_id__campaign_id=worker_campaign.campaign_id).count()
    
    cursor = connection.cursor()
    cursor.execute('''SELECT COUNT(count_answers) FROM (
                        SELECT COUNT(argo_worker_task.id) AS count_answers FROM argo_campaign
                        LEFT JOIN argo_task ON argo_task.campaign_id = argo_campaign.id
                        LEFT JOIN argo_worker_task ON argo_worker_task.task_id = argo_task.id
                        WHERE argo_campaign.id = ''' + str(worker_campaign.campaign_id) + ''' AND argo_worker_task.worker_id <> ''' + str(worker_campaign.worker_id) + ''' GROUP BY argo_worker_task.worker_id
                      ) sub_query
                      WHERE sub_query.count_answers < ''' + str(num_campaign_worker_answers))
    result_positive = cursor.fetchone()
    
    cursor = connection.cursor()
    cursor.execute('''SELECT COUNT(count_answers) FROM (
                        SELECT COUNT(argo_worker_task.id) AS count_answers FROM argo_campaign
                        LEFT JOIN argo_task ON argo_task.campaign_id = argo_campaign.id
                        LEFT JOIN argo_worker_task ON argo_worker_task.task_id = argo_task.id
                        WHERE argo_campaign.id = ''' + str(worker_campaign.campaign_id) + ''' AND argo_worker_task.worker_id <> ''' + str(worker_campaign.worker_id) + ''' GROUP BY argo_worker_task.worker_id
                      ) sub_query
                      WHERE sub_query.count_answers > ''' + str(num_campaign_worker_answers))
    result_negative = cursor.fetchone()
    
    if num_campaign_workers==0:
        return (1, 0)
    elif result_negative!=None and result_negative[0]>0:
        return (-1, round(Decimal(result_negative[0]/(num_campaign_workers)*100), 2))
    else:
        return (1, round(Decimal(result_positive[0]/(num_campaign_workers)*100), 2))
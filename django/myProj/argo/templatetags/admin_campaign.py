from django import template
from django.db import connection
from argo.models import *
import datetime
import json

register = template.Library()

@register.simple_tag
def getCampaignNumOfTasks(campaign_id):
    result = Task.objects.filter(campaign_id=campaign_id).count()
    return result
    
@register.simple_tag
def getCampaignNumOfTasksCompareToOther(campaign_id, requester_id=0):
    if(requester_id==0) or (User.objects.filter(pk=requester_id, groups__name__iexact='administrator').exists()):
        num_campaigns = Campaign.objects.exclude(id=campaign_id).count()
        num_tasks     = Task.objects.filter(campaign_id=campaign_id).count()
        
        cursor = connection.cursor()
        cursor.execute('''SELECT COUNT(sub_query.count_tasks) FROM (
                            SELECT COUNT(id) AS count_tasks FROM argo_task WHERE campaign_id <> ''' + str(campaign_id) + ''' GROUP BY campaign_id
                          ) sub_query
                          WHERE sub_query.count_tasks < ''' + str(num_tasks))
        result_positive = cursor.fetchone()
        
        cursor = connection.cursor()
        cursor.execute('''SELECT COUNT(sub_query.count_tasks) FROM (
                            SELECT COUNT(id) AS count_tasks FROM argo_task WHERE campaign_id <> ''' + str(campaign_id) + ''' GROUP BY campaign_id
                          ) sub_query
                          WHERE sub_query.count_tasks > ''' + str(num_tasks))
        result_negative = cursor.fetchone()
    else:
        num_campaigns = Campaign.objects.filter(requester_id=requester_id).exclude(id=campaign_id).count()
        num_tasks     = Task.objects.filter(campaign_id=campaign_id).filter(campaign_id__requester_id=requester_id).count()
        
        cursor = connection.cursor()
        cursor.execute('''SELECT COUNT(sub_query.count_tasks) FROM (
                            SELECT COUNT(argo_task.id) AS count_tasks FROM argo_task
                            INNER JOIN argo_campaign ON argo_campaign.id = argo_task.campaign_id AND argo_campaign.requester_id = ''' + str(requester_id) + '''
                            WHERE argo_task.campaign_id <> ''' + str(campaign_id) + ''' GROUP BY argo_task.campaign_id
                          ) sub_query
                          WHERE sub_query.count_tasks < ''' + str(num_tasks))
        result_positive = cursor.fetchone()
        
        cursor = connection.cursor()
        cursor.execute('''SELECT COUNT(sub_query.count_tasks) FROM (
                            SELECT COUNT(argo_task.id) AS count_tasks FROM argo_task
                            INNER JOIN argo_campaign ON argo_campaign.id = argo_task.campaign_id AND argo_campaign.requester_id = ''' + str(requester_id) + '''
                            WHERE argo_task.campaign_id <> ''' + str(campaign_id) + ''' GROUP BY argo_task.campaign_id
                          ) sub_query
                          WHERE sub_query.count_tasks > ''' + str(num_tasks))
        result_negative = cursor.fetchone()
        
    if num_campaigns==0:
        return (1, 0)
    elif result_negative!=None and result_negative[0]>0:
        return (-1, round(Decimal(result_negative[0]/(num_campaigns)*100), 2))
    else:
        return (1, round(Decimal(result_positive[0]/(num_campaigns)*100), 2))
   
@register.simple_tag
def getCampaignNumOfWorkers(campaign_id):
    result = Worker_Campaign.objects.filter(campaign_id=campaign_id).count()
    return result
    
@register.simple_tag
def getCampaignNumOfWorkersCompareToOther(campaign_id, requester_id=0):
    if(requester_id==0) or (User.objects.filter(pk=requester_id, groups__name__iexact='administrator').exists()):
        num_campaigns = Campaign.objects.exclude(id=campaign_id).count()
        num_workers   = Worker_Campaign.objects.filter(campaign_id=campaign_id).count()
        
        cursor = connection.cursor()
        cursor.execute('''SELECT COUNT(count_workers) FROM (
                            SELECT COUNT(argo_worker_campaign.id) AS count_workers FROM argo_campaign
                            LEFT JOIN argo_worker_campaign ON argo_worker_campaign.campaign_id = argo_campaign.id
                            WHERE argo_campaign.id <> ''' + str(campaign_id) + ''' GROUP BY argo_campaign.id
                          ) sub_query
                          WHERE sub_query.count_workers < ''' + str(num_workers))
        result_positive = cursor.fetchone()
        
        cursor = connection.cursor()
        cursor.execute('''SELECT COUNT(count_workers) FROM (
                            SELECT COUNT(argo_worker_campaign.id) AS count_workers FROM argo_campaign
                            LEFT JOIN argo_worker_campaign ON argo_worker_campaign.campaign_id = argo_campaign.id
                            WHERE argo_campaign.id <> ''' + str(campaign_id) + ''' GROUP BY argo_campaign.id
                          ) sub_query
                          WHERE sub_query.count_workers > ''' + str(num_workers))
        result_negative = cursor.fetchone()
    else:
        num_campaigns = Campaign.objects.filter(requester_id=requester_id).exclude(id=campaign_id).count()
        num_workers   = Worker_Campaign.objects.filter(campaign_id=campaign_id).filter(campaign_id__requester_id=requester_id).count()
        
        cursor = connection.cursor()
        cursor.execute('''SELECT COUNT(count_workers) FROM (
                            SELECT COUNT(argo_worker_campaign.id) AS count_workers FROM argo_campaign
                            LEFT JOIN argo_worker_campaign ON argo_worker_campaign.campaign_id = argo_campaign.id
                            WHERE argo_campaign.id <> ''' + str(campaign_id) + ''' AND requester_id = ''' + str(requester_id) + ''' GROUP BY argo_campaign.id
                          ) sub_query
                          WHERE sub_query.count_workers < ''' + str(num_workers))
        result_positive = cursor.fetchone()
        
        cursor = connection.cursor()
        cursor.execute('''SELECT COUNT(count_workers) FROM (
                            SELECT COUNT(argo_worker_campaign.id) AS count_workers FROM argo_campaign
                            LEFT JOIN argo_worker_campaign ON argo_worker_campaign.campaign_id = argo_campaign.id
                            WHERE argo_campaign.id <> ''' + str(campaign_id) + ''' AND requester_id = ''' + str(requester_id) + ''' GROUP BY argo_campaign.id
                          ) sub_query
                          WHERE sub_query.count_workers > ''' + str(num_workers))
        result_negative = cursor.fetchone()
    
    if num_campaigns==0:
        return (1, 0)
    elif result_negative!=None and result_negative[0]>0:
        return (-1, round(Decimal(result_negative[0]/(num_campaigns)*100), 2))
    else:
        return (1, round(Decimal(result_positive[0]/(num_campaigns)*100), 2))
    
@register.simple_tag
def getCampaignNumOfAnswers(campaign_id):
    result = Worker_Task.objects.filter(task_id__campaign_id=campaign_id).count()
    return result
    
@register.simple_tag
def getCampaignNumOfAnswersCompareToOther(campaign_id, requester_id=0):
    if(requester_id==0) or (User.objects.filter(pk=requester_id, groups__name__iexact='administrator').exists()):
        num_campaigns = Campaign.objects.exclude(id=campaign_id).count()
        num_answers   = Worker_Task.objects.filter(task_id__campaign_id=campaign_id).count()
        
        cursor = connection.cursor()
        cursor.execute('''SELECT COUNT(count_answers) FROM (
                            SELECT COUNT(argo_worker_task.id) AS count_answers FROM argo_campaign
                            LEFT JOIN argo_task ON argo_task.campaign_id = argo_campaign.id
                            LEFT JOIN argo_worker_task ON argo_worker_task.task_id = argo_task.id
                            WHERE argo_campaign.id <> ''' + str(campaign_id) + ''' GROUP BY argo_campaign.id
                          ) sub_query
                          WHERE sub_query.count_answers < ''' + str(num_answers))
        result_positive = cursor.fetchone()
        
        cursor = connection.cursor()
        cursor.execute('''SELECT COUNT(count_answers) FROM (
                            SELECT COUNT(argo_worker_task.id) AS count_answers FROM argo_campaign
                            LEFT JOIN argo_task ON argo_task.campaign_id = argo_campaign.id
                            LEFT JOIN argo_worker_task ON argo_worker_task.task_id = argo_task.id
                            WHERE argo_campaign.id <> ''' + str(campaign_id) + ''' GROUP BY argo_campaign.id
                          ) sub_query
                          WHERE sub_query.count_answers > ''' + str(num_answers))
        result_negative = cursor.fetchone()
    else:
        num_campaigns = Campaign.objects.filter(requester_id=requester_id).exclude(id=campaign_id).count()
        num_answers   = Worker_Task.objects.filter(task_id__campaign_id=campaign_id).filter(task_id__campaign_id__requester_id=requester_id).count()
        
        cursor = connection.cursor()
        cursor.execute('''SELECT COUNT(count_answers) FROM (
                            SELECT COUNT(argo_worker_task.id) AS count_answers FROM argo_campaign
                            LEFT JOIN argo_task ON argo_task.campaign_id = argo_campaign.id
                            LEFT JOIN argo_worker_task ON argo_worker_task.task_id = argo_task.id
                            WHERE argo_campaign.id <> ''' + str(campaign_id) + ''' AND requester_id = ''' + str(requester_id) + ''' GROUP BY argo_campaign.id
                          ) sub_query
                          WHERE sub_query.count_answers < ''' + str(num_answers))
        result_positive = cursor.fetchone()
        
        cursor = connection.cursor()
        cursor.execute('''SELECT COUNT(count_answers) FROM (
                            SELECT COUNT(argo_worker_task.id) AS count_answers FROM argo_campaign
                            LEFT JOIN argo_task ON argo_task.campaign_id = argo_campaign.id
                            LEFT JOIN argo_worker_task ON argo_worker_task.task_id = argo_task.id
                            WHERE argo_campaign.id <> ''' + str(campaign_id) + ''' AND requester_id = ''' + str(requester_id) + ''' GROUP BY argo_campaign.id
                          ) sub_query
                          WHERE sub_query.count_answers > ''' + str(num_answers))
        result_negative = cursor.fetchone()
        
    if num_campaigns==0:
        return (1, 0)
    elif result_negative!=None and result_negative[0]>0:
        return (-1, round(Decimal(result_negative[0]/(num_campaigns)*100), 2))
    else:
        return (1, round(Decimal(result_positive[0]/(num_campaigns)*100), 2))
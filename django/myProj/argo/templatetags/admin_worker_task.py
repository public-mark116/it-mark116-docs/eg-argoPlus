from django import template
from django.db import connection
from argo.models import *
import datetime
import json

register = template.Library()


@register.simple_tag
def getWorkerTaskWorkerData(workerTask_id):
    worker_task = Worker_Task.objects.select_related('task').filter(id=workerTask_id)[0]
    
    worker         = Worker.objects.filter(id_id=worker_task.worker_id)[0]
    workerCampaign = Worker_Campaign.objects.filter(worker_id=worker_task.worker_id).filter(campaign_id=worker_task.task.campaign_id)[0]
    
    return (worker.w_score, workerCampaign.c_score, workerCampaign.c_score>=worker.w_score)
    
@register.simple_tag
def getWorkerTaskCorrectAnswer(workerTask_id):
    worker_task = Worker_Task.objects.select_related('task').filter(id=workerTask_id)[0]
    
    if (len(worker_task.task.gold_answer)==0):
        return 0
    elif (worker_task.answer == worker_task.task.gold_answer):
        return 1
    else:
        return -1
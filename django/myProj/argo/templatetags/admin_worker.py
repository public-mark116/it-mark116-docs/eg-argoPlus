from django import template
from django.db import connection
from argo.models import *
import datetime
import json

register = template.Library()

@register.simple_tag
def getWorkerScore(id_worker):
    result = Worker.objects.filter(id_id=id_worker)[0]
    return result.w_score
    
@register.simple_tag
def getWorkerScoreCompareToOther(id_worker):
    num_workers  = Worker.objects.exclude(id_id=id_worker).count()
    worker       = Worker.objects.filter(id_id=id_worker)[0]
    worker_score = worker.w_score
    
    cursor = connection.cursor()
    cursor.execute('''SELECT COUNT(id_id) FROM argo_worker WHERE w_score < ''' + str(worker_score))
    result_positive = cursor.fetchone()
    
    cursor = connection.cursor()
    cursor.execute('''SELECT COUNT(id_id) FROM argo_worker WHERE w_score > ''' + str(worker_score))
    result_negative = cursor.fetchone()
    
    if num_workers==0:
        return (1, 0)
    elif result_negative!=None and result_negative[0]>0:
        return (-1, round(Decimal(result_negative[0]/(num_workers)*100), 2))
    else:
        return (1, round(Decimal(result_positive[0]/(num_workers)*100), 2))
   
@register.simple_tag
def getWorkerNumOfCampaigns(id_worker):
    result = Worker_Campaign.objects.filter(worker_id=id_worker).count()
    return result
    
@register.simple_tag
def getWorkerNumOfCampaignsCompareToOther(id_worker):
    num_workers   = Worker.objects.exclude(id_id=id_worker).count()
    num_campaigns = Worker_Campaign.objects.filter(worker_id=id_worker).count()
    
    cursor = connection.cursor()
    cursor.execute('''SELECT COUNT(count_campaigns) FROM (
                        SELECT COUNT(argo_worker_campaign.id) AS count_campaigns FROM argo_worker
                        LEFT JOIN argo_worker_campaign ON argo_worker_campaign.worker_id = argo_worker.id_id
                        WHERE argo_worker.id_id <> ''' + str(id_worker) + ''' GROUP BY argo_worker.id_id
                      ) sub_query
                      WHERE sub_query.count_campaigns < ''' + str(num_campaigns))
    result_positive = cursor.fetchone()
    
    cursor = connection.cursor()
    cursor.execute('''SELECT COUNT(count_campaigns) FROM (
                        SELECT COUNT(argo_worker_campaign.id) AS count_campaigns FROM argo_worker
                        LEFT JOIN argo_worker_campaign ON argo_worker_campaign.worker_id = argo_worker.id_id
                        WHERE argo_worker.id_id <> ''' + str(id_worker) + ''' GROUP BY argo_worker.id_id
                      ) sub_query
                      WHERE sub_query.count_campaigns > ''' + str(num_campaigns))
    result_negative = cursor.fetchone()
    
    if num_workers==0:
        return (1, 0)
    elif result_negative!=None and result_negative[0]>0:
        return (-1, round(Decimal(result_negative[0]/(num_workers)*100), 2))
    else:
        return (1, round(Decimal(result_positive[0]/(num_workers)*100), 2))
    
@register.simple_tag
def getWorkerNumOfAnswers(id_worker):
    result = Worker_Task.objects.filter(worker_id=id_worker).count()
    return result
    
@register.simple_tag
def getWorkerNumOfAnswersCompareToOther(id_worker):
    num_workers = Worker.objects.exclude(id_id=id_worker).count()
    num_answers = Worker_Task.objects.filter(worker_id=id_worker).count()
    
    cursor = connection.cursor()
    cursor.execute('''SELECT COUNT(count_answers) FROM (
                        SELECT COUNT(argo_worker_task.id) AS count_answers FROM argo_worker
                        LEFT JOIN argo_worker_task ON argo_worker_task.worker_id = argo_worker.id_id
                        WHERE argo_worker.id_id <> ''' + str(id_worker) + ''' GROUP BY argo_worker.id_id
                      ) sub_query
                      WHERE sub_query.count_answers < ''' + str(num_answers))
    result_positive = cursor.fetchone()
    
    cursor = connection.cursor()
    cursor.execute('''SELECT COUNT(count_answers) FROM (
                        SELECT COUNT(argo_worker_task.id) AS count_answers FROM argo_worker
                        LEFT JOIN argo_worker_task ON argo_worker_task.worker_id = argo_worker.id_id
                        WHERE argo_worker.id_id <> ''' + str(id_worker) + ''' GROUP BY argo_worker.id_id
                      ) sub_query
                      WHERE sub_query.count_answers > ''' + str(num_answers))
    result_negative = cursor.fetchone()
    
    if num_workers==0:
        return (1, 0)
    elif result_negative!=None and result_negative[0]>0:
        return (-1, round(Decimal(result_negative[0]/(num_workers)*100), 2))
    else:
        return (1, round(Decimal(result_positive[0]/(num_workers)*100), 2))
from django import template
from django.db import connection
from argo.models import *
import datetime
import json

register = template.Library()

@register.simple_tag
def getCampaignUnderExecution(requester_id=0, num=5):
    if(requester_id==0) or (User.objects.filter(pk=requester_id, groups__name__iexact='administrator').exists()):
        result = Campaign.objects.filter(campaign_status_id__name__iexact='under execution').filter(start_date__lte=datetime.datetime.now().date()).filter(finish_date__gte=datetime.datetime.now().date()).order_by('start_date')[:num]
    else:
        result = Campaign.objects.filter(campaign_status_id__name__iexact='under execution').filter(requester_id=requester_id).filter(start_date__lte=datetime.datetime.now().date()).filter(finish_date__gte=datetime.datetime.now().date()).order_by('start_date')[:num]
    return result

@register.simple_tag
def getNumOfCampaignUnderExecution(requester_id=0):
    if(requester_id==0) or (User.objects.filter(pk=requester_id, groups__name__iexact='administrator').exists()):
        result = Campaign.objects.filter(campaign_status_id__name__iexact='under execution').filter(start_date__lte=datetime.datetime.now().date()).filter(finish_date__gte=datetime.datetime.now().date()).count()
    else:
        result = Campaign.objects.filter(campaign_status_id__name__iexact='under execution').filter(requester_id=requester_id).filter(start_date__lte=datetime.datetime.now().date()).filter(finish_date__gte=datetime.datetime.now().date()).count()
    return result

@register.simple_tag
def getCampaignFuture(requester_id, num=5):
    if(requester_id==0) or (User.objects.filter(pk=requester_id, groups__name__iexact='administrator').exists()):
        result = Campaign.objects.filter(campaign_status_id__name__iexact='draft').filter(start_date__gte=datetime.datetime.now().date()).order_by('start_date')[:num]
    else:
        result = Campaign.objects.filter(campaign_status_id__name__iexact='draft').filter(requester_id=requester_id).filter(start_date__gte=datetime.datetime.now().date()).order_by('start_date')[:num]
    return result

@register.simple_tag
def getNumOfCampaignFuture(requester_id=0):
    if(requester_id==0) or (User.objects.filter(pk=requester_id, groups__name__iexact='administrator').exists()):
        result = Campaign.objects.filter(campaign_status_id__name__iexact='draft').filter(start_date__gte=datetime.datetime.now().date()).count()
    else:
        result = Campaign.objects.filter(campaign_status_id__name__iexact='draft').filter(requester_id=requester_id).filter(start_date__gte=datetime.datetime.now().date()).count()
    return result

@register.simple_tag
def graph1(requester_id=0, num=10):
    if(requester_id==0) or (User.objects.filter(pk=requester_id, groups__name__iexact='administrator').exists()):
        cursor = connection.cursor()
        cursor.execute('''SELECT argo_campaign.name AS campaign_name, coalesce(sub_query1.num_worker, 0) AS num_worker, coalesce(sub_query2.num_answer, 0) AS num_answer
                          FROM argo_campaign
                          LEFT JOIN (
                            SELECT argo_campaign.id AS campaign_id, count(argo_worker_campaign.id) AS num_worker
                            FROM argo_worker_campaign
                            INNER JOIN argo_campaign ON argo_campaign.id = argo_worker_campaign.campaign_id AND argo_campaign.start_date <= CURRENT_DATE
                            WHERE argo_worker_campaign.worker_banned = FALSE
                            GROUP BY argo_campaign.id
                            ORDER BY argo_campaign.start_date DESC
                            LIMIT ''' + str(num) + '''
                          ) AS sub_query1 ON sub_query1.campaign_id = argo_campaign.id
                          LEFT JOIN (
                            SELECT argo_campaign.id AS campaign_id, count(argo_worker_task.id) AS num_answer
                            FROM argo_worker_task
                            INNER JOIN argo_task ON argo_task.id = argo_worker_task.task_id
                            INNER JOIN argo_campaign ON argo_campaign.id = argo_task.campaign_id AND argo_campaign.start_date <= CURRENT_DATE
                            GROUP BY argo_campaign.id
                            ORDER BY argo_campaign.start_date DESC
                            LIMIT ''' + str(num) + '''
                          ) AS sub_query2 ON sub_query2.campaign_id = argo_campaign.id
                          WHERE argo_campaign.start_date <= CURRENT_DATE''')
        result = cursor.fetchall()
    else:
        cursor = connection.cursor()
        cursor.execute('''SELECT argo_campaign.name AS campaign_name, coalesce(sub_query1.num_worker, 0) AS num_worker, coalesce(sub_query2.num_answer, 0) AS num_answer
                          FROM argo_campaign
                          LEFT JOIN (
                            SELECT argo_campaign.id AS campaign_id, count(argo_worker_campaign.id) AS num_worker
                            FROM argo_worker_campaign
                            INNER JOIN argo_campaign ON argo_campaign.id = argo_worker_campaign.campaign_id AND argo_campaign.requester_id = ''' + str(requester_id) + ''' AND argo_campaign.start_date <= CURRENT_DATE
                            WHERE argo_worker_campaign.worker_banned = FALSE
                            GROUP BY argo_campaign.id
                            ORDER BY argo_campaign.start_date DESC
                            LIMIT ''' + str(num) + '''
                          ) AS sub_query1 ON sub_query1.campaign_id = argo_campaign.id
                          LEFT JOIN (
                            SELECT argo_campaign.id AS campaign_id, count(argo_worker_task.id) AS num_answer
                            FROM argo_worker_task
                            INNER JOIN argo_task ON argo_task.id = argo_worker_task.task_id
                            INNER JOIN argo_campaign ON argo_campaign.id = argo_task.campaign_id AND argo_campaign.requester_id = ''' + str(requester_id) + ''' AND argo_campaign.start_date <= CURRENT_DATE
                            GROUP BY argo_campaign.id
                            ORDER BY argo_campaign.start_date DESC
                            LIMIT ''' + str(num) + '''
                          ) AS sub_query2 ON sub_query2.campaign_id = argo_campaign.id
                          WHERE argo_campaign.requester_id = ''' + str(requester_id) + ''' AND argo_campaign.start_date <= CURRENT_DATE''')
        result = cursor.fetchall()
    return json.dumps(result)

@register.simple_tag
def getSkillMoreUsed(num=5):
    cursor = connection.cursor()
    cursor.execute('''SELECT argo_skill.id, argo_skill.name, argo_skill.value, sub_query.task_count FROM (
                        SELECT argo_skill.id, count(argo_task_skill.task_id) AS task_count FROM argo_skill
                        INNER JOIN argo_task_skill ON argo_task_skill.skill_id = argo_skill.id
                        GROUP BY argo_skill.id
                      ) AS sub_query
                      INNER JOIN argo_skill ON argo_skill.id = sub_query.id
                      ORDER BY task_count DESC LIMIT ''' + str(num))
    result = cursor.fetchall()
    return result

@register.simple_tag
def getNumOfSkillMoreUsed():
    result = Skill.objects.count()
    return result
    
@register.simple_tag
def getLastWorker(requester_id=0, num=5):
    if(requester_id==0) or (User.objects.filter(pk=requester_id, groups__name__iexact='administrator').exists()):
        cursor = connection.cursor()
        cursor.execute('''SELECT argo_worker.id_id, auth_user.last_name, auth_user.first_name, to_char(auth_user.date_joined, 'YYYY-MM-DD') AS date_joined FROM argo_worker INNER JOIN auth_user ON auth_user.id = argo_worker.id_id ORDER BY auth_user.date_joined DESC LIMIT ''' + str(num))
        result = cursor.fetchall()
    else:
        cursor = connection.cursor()
        cursor.execute('''SELECT DISTINCT argo_worker.id_id, auth_user.last_name, auth_user.first_name, to_char(auth_user.date_joined, 'YYYY-MM-DD') AS date_joined FROM argo_worker INNER JOIN auth_user ON auth_user.id = argo_worker.id_id INNER JOIN argo_worker_campaign ON argo_worker_campaign.worker_id = argo_worker.id_id INNER JOIN argo_campaign ON argo_campaign.id = argo_worker_campaign.campaign_id AND argo_campaign.requester_id = ''' + str(requester_id) + ''' ORDER BY date_joined DESC LIMIT ''' + str(num))
        result = cursor.fetchall()
    return result

@register.simple_tag
def getNumOfLastWorker(requester_id=0):
    if(requester_id==0) or (User.objects.filter(pk=requester_id, groups__name__iexact='administrator').exists()):
        cursor = connection.cursor()
        cursor.execute('''SELECT count(id_id) AS num_worker FROM argo_worker''')
        result = cursor.fetchone()
    else:
        cursor = connection.cursor()
        cursor.execute('''SELECT count(DISTINCT argo_worker.id_id) AS num_worker FROM argo_worker INNER JOIN argo_worker_campaign ON argo_worker_campaign.worker_id = argo_worker.id_id INNER JOIN argo_campaign ON argo_campaign.id = argo_worker_campaign.campaign_id AND argo_campaign.requester_id = ''' + str(requester_id))
        result = cursor.fetchone()
    return result[0]

@register.simple_tag
def graph2():
    cursor = connection.cursor()
    cursor.execute('''SELECT genre, count(id_id) AS genre_count FROM argo_worker GROUP BY genre ORDER BY genre_count DESC''')
    result = cursor.fetchall()
    return json.dumps(result)

@register.simple_tag
def graph3():
    cursor = connection.cursor()
    cursor.execute(''' SELECT SUM(CASE WHEN (EXTRACT(YEAR FROM CURRENT_DATE) - EXTRACT(YEAR FROM birth_date)) < 18 THEN 1 ELSE 0 END) AS less_18,
                       SUM(CASE WHEN (EXTRACT(YEAR FROM CURRENT_DATE) - EXTRACT(YEAR FROM birth_date)) BETWEEN 18 AND 24 THEN 1 ELSE 0 END) AS between_18_24,
                       SUM(CASE WHEN (EXTRACT(YEAR FROM CURRENT_DATE) - EXTRACT(YEAR FROM birth_date)) BETWEEN 25 AND 34 THEN 1 ELSE 0 END) AS between_25_34,
                       SUM(CASE WHEN (EXTRACT(YEAR FROM CURRENT_DATE) - EXTRACT(YEAR FROM birth_date)) BETWEEN 35 AND 44 THEN 1 ELSE 0 END) AS between_35_44,
                       SUM(CASE WHEN (EXTRACT(YEAR FROM CURRENT_DATE) - EXTRACT(YEAR FROM birth_date)) >= 45 THEN 1 ELSE 0 END) AS greater_45
                       FROM argo_worker''')
    result = cursor.fetchone()

    i    = 0
    ret  = []
    case = ['<18', '18-24', '25-34', '35-44', '>45']
    for r in result:
        ret.append((case[i], r))
        i = i+1

    return json.dumps(ret)
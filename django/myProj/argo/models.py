from __future__ import unicode_literals
from django.db import models
from django.utils.encoding import python_2_unicode_compatible
from django.utils import timezone
from django.contrib.auth.models import User
from decimal import Decimal

class Worker(models.Model):
    id                   = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True)
    genre                = models.CharField(verbose_name=u"Genre", max_length=1)
    birth_date           = models.DateField(verbose_name=u"Birth Date")
    w_score              = models.DecimalField(verbose_name=u"Score", max_digits=5, decimal_places=2, default=Decimal('0.00'))
    w_trustworthiness    = models.DecimalField(verbose_name=u"Trustworthiness", max_digits=4, decimal_places=2, default=Decimal('1.00'))
    banned               = models.BooleanField(verbose_name=u"Banned", default=False)
        
    class Meta:
        verbose_name        = "Worker"
        verbose_name_plural = "Workers"
        default_permissions = ('add', 'change', 'delete', 'view')
        
    def __unicode__(self):
        name = self.id.first_name + " " +self.id.last_name + " - " + self.id.username 
        return name 
        
    def __str__(self):
        name = self.id.first_name + " " +self.id.last_name + " - " + self.id.username 
        return name 

class Campaign_Status(models.Model):
    name                 = models.TextField(verbose_name=u"Name")

    class Meta:
        verbose_name        = "Campaign Status"
        verbose_name_plural = "Campaign Status"
        default_permissions = ('add', 'change', 'delete', 'view')
        
    def __unicode__(self):
        return self.name

    def __str__(self):
        return self.name
        
class Campaign(models.Model):
    name                 = models.TextField(verbose_name=u"Name")
    requester            = models.ForeignKey(User, verbose_name=u"Requester", on_delete=models.CASCADE)
    campaign_status      = models.ForeignKey(Campaign_Status, verbose_name=u"Status", on_delete=models.CASCADE)
    creation_date        = models.DateTimeField(verbose_name=u"Creation Date", auto_now_add=True)
    start_date           = models.DateField(verbose_name=u"Start Date")
    finish_date          = models.DateField(verbose_name=u"Finish Date")
    start_signup         = models.DateField(verbose_name=u"Start Signup")
    finish_signup        = models.DateField(verbose_name=u"Finish Signup")
    init_trustworthiness = models.DecimalField(verbose_name=u"Trustworthiness", max_digits=4, decimal_places=2)
    score_increment      = models.DecimalField(verbose_name=u"Score Increment", max_digits=5, decimal_places=2)
    w_campaign           = models.ManyToManyField(Worker, verbose_name=u"Worker", through='Worker_Campaign')

    class Meta:
        verbose_name        = "Campaign"
        verbose_name_plural = "Campaigns"
        default_permissions = ('add', 'change', 'delete', 'view')
        
    def __unicode__(self):
        return self.name
        
    def __str__(self):
        return self.name

class Task_Status(models.Model):
    name                 = models.TextField(verbose_name=u"Name")

    class Meta:
        verbose_name        = "Task Status"
        verbose_name_plural = "Task Status"
        default_permissions = ('add', 'change', 'delete', 'view')
        
    def __unicode__(self):
        return self.name

    def __str__(self):
        return self.name

class Answer_Type(models.Model):
    name                 = models.TextField(verbose_name=u"Name")
        
    class Meta:
        verbose_name        = "Answer Type"
        verbose_name_plural = "Answer Types"
        default_permissions = ('add', 'change', 'delete', 'view')

    def __unicode__(self):
        return self.name
        
    def __str__(self):
        return self.name
      
class Task(models.Model):
    description          = models.TextField(verbose_name=u"Request")
    content              = models.TextField(verbose_name=u"Content", null=True)
    answer_type          = models.ForeignKey(Answer_Type, verbose_name=u"Answer Type", on_delete=models.CASCADE)
    insert_date          = models.DateTimeField(verbose_name=u"Insert Date", auto_now_add=True)
    priority             = models.IntegerField(verbose_name=u"Priority", default=0)
    id_origin            = models.TextField(verbose_name=u"ID Origin", null=True)
    gold_answer          = models.TextField(verbose_name=u"Gold Answer", null=True)
    final_answer         = models.TextField(verbose_name=u"Final Answer", null=True)
    campaign             = models.ForeignKey(Campaign, verbose_name=u"Campaign", on_delete=models.CASCADE)
    task_status          = models.ForeignKey(Task_Status, verbose_name=u"Status", on_delete=models.CASCADE)
    current_round        = models.IntegerField(verbose_name=u"Current Round", null=True)
    w_task               = models.ManyToManyField(Worker, verbose_name=u"Worker", through='Worker_Task')
        
    class Meta:
        verbose_name        = "Task"
        verbose_name_plural = "Tasks"
        default_permissions = ('add', 'change', 'delete', 'view')
        
    def __unicode__(self):
        return self.description
        
    def __str__(self):
        return self.description
   
class Task_Choice(models.Model):
    task                 = models.ForeignKey(Task, verbose_name=u"Task", on_delete=models.CASCADE)
    id_origin            = models.TextField(verbose_name=u"ID Origin")
    description          = models.TextField(verbose_name=u"Name")
        
    class Meta:
        verbose_name        = "Choice"
        verbose_name_plural = "Choice Set"
        default_permissions = ('add', 'change', 'delete', 'view')
    
class Task_Setup(models.Model):
    num_worker           = models.IntegerField(verbose_name=u"Num. Worker")
    min_trustworthiness  = models.DecimalField(verbose_name=u"Min. Trustworthiness", max_digits=4, decimal_places=2)
    consensus_threshold  = models.DecimalField(verbose_name=u"Consensus Threshold", max_digits=4, decimal_places=2)
    max_exec_time        = models.IntegerField(verbose_name=u"Max. Execution Time")
    t_task_setup         = models.ManyToManyField(Task, verbose_name=u"Task", through='Task_Task_Setup')
        
    class Meta:
        verbose_name        = "Setup"
        verbose_name_plural = "Setup"
        default_permissions = ('add', 'change', 'delete', 'view')
        
    def __unicode__(self):
        description = "{NumWorker: " + str(self.num_worker) + ", MinTrustworthiness: " + str(self.min_trustworthiness) + ", ConsensusThreshold: " + str(self.consensus_threshold) + ", MaxExecTime: " + str(self.max_exec_time) + "}"
        return description 
        
    def __str__(self):
        description = "{NumWorker: " + str(self.num_worker) + ", MinTrustworthiness: " + str(self.min_trustworthiness) + ", ConsensusThreshold: " + str(self.consensus_threshold) + ", MaxExecTime: " + str(self.max_exec_time) + "}"
        return description 
        
class Worker_Campaign(models.Model):
    worker               = models.ForeignKey(Worker, verbose_name=u"Worker", on_delete=models.CASCADE)
    campaign             = models.ForeignKey(Campaign, verbose_name=u"Campaign", on_delete=models.CASCADE)
    c_score              = models.DecimalField(verbose_name=u"Score", max_digits=5, decimal_places=2, default=Decimal('0.00'))
    c_trustworthiness    = models.DecimalField(verbose_name=u"Trustworthiness", max_digits=4, decimal_places=2, default=Decimal('1.00'))
    worker_banned        = models.BooleanField(verbose_name=u"Banned", default=False)
        
    class Meta:
        verbose_name        = "Worker Campaign"
        verbose_name_plural = "Workers Campaigns"
        default_permissions = ('add', 'change', 'delete', 'view')
        
    def __unicode__(self):
        name = self.campaign.name + " - " + self.worker.id.username
        return name 
        
    def __str__(self):
        name = self.campaign.name + " - " + self.worker.id.username
        return name 
        
class Worker_Task(models.Model):
    worker               = models.ForeignKey(Worker, verbose_name=u"Worker", on_delete=models.CASCADE)
    task                 = models.ForeignKey(Task, verbose_name=u"Task", on_delete=models.CASCADE)
    round                = models.IntegerField(verbose_name=u"Round")
    timeout              = models.BooleanField(verbose_name=u"Timestamp", default=False)
    timestamp_in         = models.DateTimeField(verbose_name=u"Timestamp In", auto_now_add=True)
    timestamp_out        = models.DateTimeField(verbose_name=u"Timestamp Out")
    answer               = models.TextField(verbose_name=u"Answer")
    current_w_trustworthiness = models.DecimalField(verbose_name=u"Current Trustworthiness", max_digits=4, decimal_places=2)
        
    class Meta:
        unique_together     = ('worker', 'task', 'round')
        verbose_name        = "Worker Task"
        verbose_name_plural = "Workers Tasks"
        default_permissions = ('add', 'change', 'delete', 'view')
        
    def __unicode__(self):
        name = self.task.description + " - " + self.worker.id.username
        return name 
        
    def __str__(self):
        name = self.task.description + " - " + self.worker.id.username
        return name 
        
class Task_Task_Setup(models.Model):
    task_setup           = models.ForeignKey(Task_Setup, verbose_name=u"Task Setup", on_delete=models.CASCADE)
    task                 = models.ForeignKey(Task, verbose_name=u"Task", on_delete=models.CASCADE)
    round                = models.IntegerField(verbose_name=u"Round")
        
    class Meta:
        verbose_name        = "Task Setup"
        verbose_name_plural = "Tasks Setup"
        default_permissions = ('add', 'change', 'delete', 'view')
        
class Pattern(models.Model):
    description          = models.TextField(verbose_name=u"Name")
    ts_pattern           = models.ManyToManyField(Task_Setup, verbose_name=u"Task Setup", through='Task_Setup_Pattern')
        
    class Meta:
        verbose_name        = "Pattern"
        verbose_name_plural = "Patterns"
        default_permissions = ('add', 'change', 'delete', 'view')
        
    def __unicode__(self):
        return self.description 
        
    def __str__(self):
        return self.description 
        
class Task_Setup_Pattern(models.Model):
    pattern              = models.ForeignKey(Pattern, verbose_name=u"Pattern", on_delete=models.CASCADE)
    task_setup           = models.ForeignKey(Task_Setup, verbose_name=u"Task Setup", on_delete=models.CASCADE)
    round                = models.IntegerField(verbose_name=u"Round")
        
    class Meta:
        verbose_name        = "Pattern Setup"
        verbose_name_plural = "Patterns Setup"
        default_permissions = ('add', 'change', 'delete', 'view')
        
class Skill(models.Model):
    name                 = models.TextField(verbose_name=u"Name")
    value                = models.FloatField(verbose_name=u"Value")
    ts_skill             = models.ManyToManyField(Task, verbose_name=u"Task", through='Task_Skill')
        
    class Meta:
        verbose_name        = "Skill"
        verbose_name_plural = "Skills"
        default_permissions = ('add', 'change', 'delete', 'view')
        
    def __unicode__(self):
        return self.name + ": " +  str(self.value)
        
    def __str__(self):
        return self.name + ": " +  str(self.value)

class Task_Skill(models.Model):
    task                 = models.ForeignKey(Task, verbose_name=u"Task", on_delete=models.CASCADE)
    skill                = models.ForeignKey(Skill, verbose_name=u"Skill", on_delete=models.CASCADE)
        
    class Meta:
        verbose_name        = "Task Skill"
        verbose_name_plural = "Tasks Skills"
        default_permissions = ('add', 'change', 'delete', 'view')
        
class Profile(models.Model):
    description          = models.TextField(verbose_name=u"Name")
    sk_profile           = models.ManyToManyField(Skill, verbose_name=u"Skill", through='Profile_Skill')
        
    class Meta:
        verbose_name        = "Profile"
        verbose_name_plural = "Profiles"
        default_permissions = ('add', 'change', 'delete', 'view')
        
    def __unicode__(self):
        return self.description 
        
    def __str__(self):
        return self.description 

class Profile_Skill(models.Model):
    profile              = models.ForeignKey(Profile, verbose_name=u"Profile", on_delete=models.CASCADE)
    skill                = models.ForeignKey(Skill, verbose_name=u"Skill", on_delete=models.CASCADE)
        
    class Meta:
        verbose_name        = "Profile Skill"
        verbose_name_plural = "Profiles Skills"
        default_permissions = ('add', 'change', 'delete', 'view')
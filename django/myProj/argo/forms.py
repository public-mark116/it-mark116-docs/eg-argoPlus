from django.contrib.auth.models import User

from django import forms
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth.models import User, Group, Permission
from .models import *

import json
from django.http import JsonResponse

class SignUpForm(forms.ModelForm):
    first_name             = forms.CharField(label='First Name', widget=forms.TextInput())
    last_name              = forms.CharField(label='Last Name', widget=forms.TextInput())
    email                  = forms.CharField(label='Email', widget=forms.EmailInput())
    username               = forms.CharField(label='Username', widget=forms.TextInput())
    password               = forms.CharField(label='Password', widget=forms.PasswordInput())
    
    class Meta: 
        model  = User
        fields = ('first_name', 'last_name', 'email', 'username', 'password')
    
class CampaignAdminForm(forms.ModelForm):
    name                 = forms.CharField(label='Name')
    requester            = forms.ModelChoiceField(label='Requester', help_text='Select user from the "Requester" group', queryset=User.objects.filter(groups__name__iexact='requester').order_by('username'))
    campaign_status      = forms.ModelChoiceField(label='Status', queryset=Campaign_Status.objects.order_by('name').all())
    start_signup         = forms.CharField(label='Start Signup', widget=forms.DateInput(attrs={'type': 'date'}))
    finish_signup        = forms.CharField(label='Finish Signup', widget=forms.DateInput(attrs={'type': 'date'}))
    start_date           = forms.CharField(label='Start Date', widget=forms.DateInput(attrs={'type': 'date'}))
    finish_date          = forms.CharField(label='Finish Date', widget=forms.DateInput(attrs={'type': 'date'}))
    init_trustworthiness = forms.DecimalField(label='Trustworthiness', help_text='Enter the required minimum value', widget=forms.NumberInput(), initial=1, min_value=1, decimal_places=2)
    score_increment      = forms.DecimalField(label='Score Increment', widget=forms.NumberInput(), initial=1, min_value=1, decimal_places=2)
    
    # taskjson #
    taskjson             = forms.CharField(label='JSON', help_text='View the help section for more information', widget=forms.Textarea, required=False)
    # taskjson #

    class Meta: 
        model  = Campaign
        fields = ('name', 'requester', 'campaign_status', 'start_signup', 'finish_signup', 'start_date', 'finish_date', 'init_trustworthiness', 'score_increment', 'taskjson')

    def __init__(self, *args, **kwargs): 
        super().__init__(*args, **kwargs)
        for field in self._meta.fields:
            if self.fields[field].required:
                attrs = {'class': 'form-control required'}
            else:
                attrs = {'class': 'form-control'}
            self.fields[field].widget.attrs.update(attrs)
            
    def clean_taskjson(self):
        returnJson = {'tasks': []}

        if self.cleaned_data['taskjson']:
            try:
                jsonMain = json.loads(self.cleaned_data.get('taskjson'))
                
                key_accepted  = ['request', 'content', 'priority', 'type', 'choices', 'status', 'id_origin', 'gold_answer', 'pattern', 'profile']
                key_mandatory = ['request', 'type']
                
                # check master key "tasks"
                if 'tasks' in jsonMain:
                    jsonTasks = jsonMain['tasks']
                elif 'TASKS' in jsonMain:
                    jsonTasks = jsonMain['TASKS']
                else:
                    raise forms.ValidationError('Key \'tasks\' not found in JSON')
                
                # covert keys to lower
                for index, jsonTask in enumerate(jsonTasks):
                    for key in list(jsonTask.keys()):
                        if key.lower() != key:
                            jsonTasks[index][key.lower()] = jsonTasks[index][key]
                            del jsonTasks[index][key]
                
                # check json
                for jsonTask in jsonTasks:
                    # check look for unexpected keys
                    for key in jsonTask.keys():
                        if (key not in key_accepted):
                            raise forms.ValidationError('Unexpected key \'' + key + '\' in JSON')

                    # check if all mandatory keys are used                
                    for key in key_mandatory:
                        if (key not in jsonTask.keys()):
                            raise forms.ValidationError('Key \'' + key + '\' not found in JSON (key \'' + key + '\' is mandatory)')
                
                # check value of setup (pattern), of skill (profile)
                for jsonTask in jsonTasks:
                    if ('type' in jsonTask.keys()) and (not Answer_Type.objects.filter(name__iexact=jsonTask['type'].lower()).exists()):
                        raise forms.ValidationError('Unexpected value \'' + jsonTask['type'].lower() + '\' for \'type\' key')
                
                    if ('status' in jsonTask.keys()) and (not Task_Status.objects.filter(name__iexact=jsonTask['status'].lower()).exists()):
                        raise forms.ValidationError('Unexpected value \'' + jsonTask['status'].lower() + '\' for \'status\' key')
                        
                    if ('pattern' in jsonTask.keys()) and (not Task_Setup_Pattern.objects.filter(pattern__description__iexact=jsonTask['pattern'].lower()).exists()):
                        raise forms.ValidationError('Unexpected value \'' + jsonTask['pattern'].lower() + '\' for \'pattern\' key')
                
                    if ('profile' in jsonTask.keys()) and (not Profile_Skill.objects.filter(profile__description__iexact=jsonTask['profile'].lower()).exists()):
                        raise forms.ValidationError('Unexpected value \'' + jsonTask['profile'].lower() + '\' for \'profile\' key')
                        
                returnJson['tasks'] = jsonTasks
            
            except ValueError as error:
                raise forms.ValidationError(error)
            
        return json.dumps(returnJson)
        
    def clean(self):
        self.cleaned_data = super(CampaignAdminForm, self).clean()
        
        if self.cleaned_data and ('finish_signup' in self.cleaned_data and 'start_signup' in self.cleaned_data and 'finish_date' in self.cleaned_data and 'start_date' in self.cleaned_data):
            # check finish_signup and start_signup
            if self.cleaned_data['finish_signup'] < self.cleaned_data['start_signup']:
                raise forms.ValidationError({'start_signup': ['"Finish Signup" must follow than the "Start Signup"'], 'finish_signup': ['"Finish Signup" must follow than the "Start Signup"']})
                
            # check finish_date and start_date
            if self.cleaned_data['finish_date'] < self.cleaned_data['start_date']:
                raise forms.ValidationError({'start_date': ['"Finish Date" must follow than the "Start Date"'], 'finish_date': ['"Finish Date" must follow than the "Start Date"']})

            # check start_date and start_date
            if self.cleaned_data['start_date'] < self.cleaned_data['start_signup']:
                raise forms.ValidationError({'start_signup': ['"Start Date" must follow than the "Start Date"'], 'start_date': ['"Start Date" must follow than the "Start Date"']})
                
            # if status of campaign is "under execution" check if at least one task is "ready"
            campStatus = self.cleaned_data.get('campaign_status')
            if campStatus == (Campaign_Status.objects.get(name__iexact='under execution')):
                count = 0
                
                # check in json
                jsonMain   = json.loads(self.cleaned_data.get('taskjson'))
                for jsonTask in jsonMain['tasks']:
                    if ('status' in jsonTask) and jsonTask['status'].lower() == 'ready to execute':
                        count += 1
                    else:
                        count += 1

                # check in previously task
                campTaskCount      = int(self.data.get('task_set-TOTAL_FORMS', 0))
                taskStatusReadyObj = Task_Status.objects.get(name__iexact='ready to execute')
                for i in range(0, campTaskCount):
                    if self.data.get('task_set-' + str(i) + '-task_status'):
                        if int(self.data.get('task_set-' + str(i) + '-task_status')) == int(taskStatusReadyObj.id):
                            count += 1
                        
                if count == 0:
                  raise forms.ValidationError({'campaign_status': ['This campaign does not have any tasks "Ready to Execute"']})

            return self.cleaned_data

class CampaignRequesterAdminForm(forms.ModelForm):
    name                 = forms.CharField(label='Campaign Name')
    requester            = forms.HiddenInput()
    campaign_status      = forms.ModelChoiceField(label='Status', queryset=Campaign_Status.objects.order_by('name').all())
    start_signup         = forms.CharField(label='Start Signup', widget=forms.DateInput(attrs={'type': 'date'}))
    finish_signup        = forms.CharField(label='Finish Signup', widget=forms.DateInput(attrs={'type': 'date'}))
    start_date           = forms.CharField(label='Start Date', widget=forms.DateInput(attrs={'type': 'date'}))
    finish_date          = forms.CharField(label='Finish Date', widget=forms.DateInput(attrs={'type': 'date'}))
    init_trustworthiness = forms.DecimalField(label='Trustworthiness', help_text='Enter the required minimum value', widget=forms.NumberInput(), initial=1, min_value=1, decimal_places=2)

    # taskjson #
    taskjson             = forms.CharField(label='JSON', help_text='View the help section for more information', widget=forms.Textarea, required=False)
    # taskjson #
    
    class Meta: 
        model  = Campaign
        fields = ('name', 'campaign_status', 'start_signup', 'finish_signup', 'start_date', 'finish_date', 'init_trustworthiness', 'taskjson')

    def __init__(self, *args, **kwargs): 
        super().__init__(*args, **kwargs)
        for field in self._meta.fields:
            if self.fields[field].required:
                attrs = {'class': 'form-control required'}
            else:
                attrs = {'class': 'form-control'}
            self.fields[field].widget.attrs.update(attrs)

    def clean_taskjson(self):
        returnJson = {'tasks': []}

        if self.cleaned_data['taskjson']:
            try:
                jsonMain = json.loads(self.cleaned_data.get('taskjson'))
                
                key_accepted  = ['request', 'content', 'priority', 'type', 'choices', 'status', 'id_origin', 'gold_answer', 'pattern', 'profile']
                key_mandatory = ['request', 'type']
                
                # check master key "tasks"
                if 'tasks' in jsonMain:
                    jsonTasks = jsonMain['tasks']
                elif 'TASKS' in jsonMain:
                    jsonTasks = jsonMain['TASKS']
                else:
                    raise forms.ValidationError('Key \'tasks\' not found in JSON')
                
                # covert keys to lower
                for index, jsonTask in enumerate(jsonTasks):
                    for key in list(jsonTask.keys()):
                        if key.lower() != key:
                            jsonTasks[index][key.lower()] = jsonTasks[index][key]
                            del jsonTasks[index][key]
                
                # check json
                for jsonTask in jsonTasks:
                    # check look for unexpected keys
                    for key in jsonTask.keys():
                        if (key not in key_accepted):
                            raise forms.ValidationError('Unexpected key \'' + key + '\' in JSON')

                    # check if all mandatory keys are used                
                    for key in key_mandatory:
                        if (key not in jsonTask.keys()):
                            raise forms.ValidationError('Key \'' + key + '\' not found in JSON (key \'' + key + '\' is mandatory)')
                
                # check value of setup (pattern), of skill (profile)
                for jsonTask in jsonTasks:
                    if ('type' in jsonTask.keys()) and (not Answer_Type.objects.filter(name__iexact=jsonTask['type'].lower()).exists()):
                        raise forms.ValidationError('Unexpected value \'' + jsonTask['type'].lower() + '\' for \'type\' key')
                
                    if ('status' in jsonTask.keys()) and (not Task_Status.objects.filter(name__iexact=jsonTask['status'].lower()).exists()):
                        raise forms.ValidationError('Unexpected value \'' + jsonTask['status'].lower() + '\' for \'status\' key')
                        
                    if ('pattern' in jsonTask.keys()) and (not Task_Setup_Pattern.objects.filter(pattern__description__iexact=jsonTask['pattern'].lower()).exists()):
                        raise forms.ValidationError('Unexpected value \'' + jsonTask['pattern'].lower() + '\' for \'pattern\' key')
                
                    if ('profile' in jsonTask.keys()) and (not Profile_Skill.objects.filter(profile__description__iexact=jsonTask['profile'].lower()).exists()):
                        raise forms.ValidationError('Unexpected value \'' + jsonTask['profile'].lower() + '\' for \'profile\' key')

                returnJson['tasks'] = jsonTasks
            
            except ValueError as error:
                raise forms.ValidationError(error)
            
        return json.dumps(returnJson)
        
    def clean(self):
        self.cleaned_data = super(CampaignRequesterAdminForm, self).clean()

        if self.cleaned_data and ('finish_signup' in self.cleaned_data and 'start_signup' in self.cleaned_data and 'finish_date' in self.cleaned_data and 'start_date' in self.cleaned_data):
            # check finish_signup and start_signup
            if self.cleaned_data['finish_signup'] < self.cleaned_data['start_signup']:
                raise forms.ValidationError({'start_signup': ['"Finish Signup" must follow than the "Start Signup"'], 'finish_signup': ['"Finish Signup" must follow than the "Start Signup"']})
                
            # check finish_date and start_date
            if self.cleaned_data['finish_date'] < self.cleaned_data['start_date']:
                raise forms.ValidationError({'start_date': ['"Finish Date" must follow than the "Start Date"'], 'finish_date': ['"Finish Date" must follow than the "Start Date"']})

            # check start_date and start_date
            if self.cleaned_data['start_date'] < self.cleaned_data['start_signup']:
                raise forms.ValidationError({'start_signup': ['"Start Date" must follow than the "Start Date"'], 'start_date': ['"Start Date" must follow than the "Start Date"']})
                
            # if status of campaign is "under execution" check if at least one task is "ready"
            campStatus = self.cleaned_data.get('campaign_status')
            if campStatus == (Campaign_Status.objects.get(name__iexact='under execution')):
                count = 0
                
                # check in json
                jsonMain   = json.loads(self.cleaned_data.get('taskjson'))
                for jsonTask in jsonMain['tasks']:
                    if ('status' in jsonTask) and jsonTask['status'].lower() == 'ready to execute':
                        count += 1
                    else:
                        count += 1

                # check in previously task
                campTaskCount      = int(self.data.get('task_set-TOTAL_FORMS', 0))
                taskStatusReadyObj = Task_Status.objects.get(name__iexact='ready to execute')
                for i in range(0, campTaskCount):
                    if self.data.get('task_set-' + str(i) + '-task_status'):
                        if int(self.data.get('task_set-' + str(i) + '-task_status')) == int(taskStatusReadyObj.id):
                            count += 1
                        
                if count == 0:
                  raise forms.ValidationError({'campaign_status': ['This campaign does not have any tasks "Ready to Execute"']})

        return self.cleaned_data
     
class TaskInlineAdminForm(forms.ModelForm):
    description   = forms.CharField(label='Request', widget=forms.Textarea)
    content       = forms.CharField(label='Content', widget=forms.Textarea, required=False)
    task_status   = forms.ModelChoiceField(label='Status', queryset=Task_Status.objects.order_by('name').all())
    answer_type   = forms.ModelChoiceField(label='Answer Type', help_text='Select "Choice" to insert the list of possible answers (Choice Set)', queryset=Answer_Type.objects.order_by('name').all())
    priority      = forms.DecimalField(label='Priority', widget=forms.NumberInput(), initial=1, min_value=1)
   
    class Meta: 
        model  = Task
        fields = ('campaign', 'description', 'content', 'task_status', 'answer_type', 'priority')

    def __init__(self, *args, **kwargs): 
        super().__init__(*args, **kwargs)
        for field in self._meta.fields:
            if self.fields[field].required:
                attrs = {'class': 'form-control required'}
            else:
                attrs = {'class': 'form-control'}
            self.fields[field].widget.attrs.update(attrs)

class TaskAdminForm(forms.ModelForm):
    campaign      = forms.ModelChoiceField(label='Campaign', queryset=Campaign.objects.order_by('name').all())
    description   = forms.CharField(label='Request', widget=forms.Textarea)
    content       = forms.CharField(label='Content', widget=forms.Textarea, required=False)
    task_status   = forms.ModelChoiceField(label='Status', queryset=Task_Status.objects.order_by('name').all())
    answer_type   = forms.ModelChoiceField(label='Answer Type', help_text='Select "Choice" to insert the list of possible answers (Choice Set)', queryset=Answer_Type.objects.order_by('name').all())
    priority      = forms.DecimalField(label='Priority', widget=forms.NumberInput(), initial=1, min_value=1)
    id_origin     = forms.CharField(label='ID Origin', help_text='Enter the original ID of the task (if it exists)', required=False)
    current_round = forms.DecimalField(label='Current Round', widget=forms.NumberInput(), initial=1, min_value=1)
    gold_answer   = forms.CharField(label='Gold Answer', required=False)
    final_answer  = forms.CharField(label='Final Answer', widget=forms.TextInput(attrs={'readonly':'readonly'}), required=False)
    
    class Meta: 
        model  = Task
        fields = ('campaign', 'description', 'content', 'task_status', 'answer_type', 'priority', 'id_origin', 'current_round', 'gold_answer', 'final_answer')
        
    def __init__(self, *args, **kwargs): 
        super().__init__(*args, **kwargs)
        
        # filter ModelChoiceField if user is not Administrator (if is Requester)
        if not self.requestIsAdmin:
            self.fields['campaign'].queryset = Campaign.objects.filter(requester_id=self.requestUser)
        
        for field in self._meta.fields:
            if self.fields[field].required:
                attrs = {'class': 'form-control required'}
            else:
                attrs = {'class': 'form-control'}
            self.fields[field].widget.attrs.update(attrs)
           
class Task_ChoiceAdminForm(forms.ModelForm):
    task        = forms.ModelChoiceField(label='Task', help_text='Select a task with "Choice" as answer type', queryset=Task.objects.filter(answer_type=(Answer_Type.objects.filter(name='Choice'))).order_by('description'))
    id_origin   = forms.CharField(label='ID Origin', help_text='Enter the original ID of the answer (if it exists)', required=False)
    description = forms.CharField(label='Name', help_text='Text of choice')
    
    class Meta: 
        model  = Task_Choice
        fields = ('task', 'id_origin', 'description')

    def __init__(self, *args, **kwargs): 
        super().__init__(*args, **kwargs)
        
        # filter ModelChoiceField if user is not Administrator (if is Requester)
        if not self.requestIsAdmin:
            self.fields['task'].queryset = Task.objects.filter(campaign_id__requester_id=self.requestUser)
            
        for field in self._meta.fields:
            if self.fields[field].required:
                attrs = {'class': 'form-control required'}
            else:
                attrs = {'class': 'form-control'}
            self.fields[field].widget.attrs.update(attrs)
        
class Task_Task_SetupAdminForm(forms.ModelForm):
    task       = forms.ModelChoiceField(label='Task', help_text='Select a task to apply this setup', queryset=Task.objects.order_by('description').all())
    round      = forms.IntegerField(label='Round', help_text='Select the round to apply the setup', widget=forms.NumberInput(), initial=1)
    task_setup = forms.ModelChoiceField(label='Task Setup', queryset=Task_Setup.objects.order_by('num_worker').all())
   
    class Meta: 
        model  = Task_Task_Setup
        fields = ('task', 'round', 'task_setup')

    def __init__(self, *args, **kwargs): 
        super().__init__(*args, **kwargs)
        
        # filter ModelChoiceField if user is not Administrator (if is Requester)
        if not self.requestIsAdmin:
            self.fields['task'].queryset = Task.objects.filter(campaign_id__requester_id=self.requestUser)
        
        if 'instance' in kwargs and kwargs['instance'] is not None:
            self.id = kwargs['instance'].id
        else:
            self.id = None
            
        for field in self._meta.fields:
            if self.fields[field].required:
                attrs = {'class': 'form-control required'}
            else:
                attrs = {'class': 'form-control'}
            self.fields[field].widget.attrs.update(attrs)
            
    def clean_round(self):
        if self.cleaned_data['round'] < 1:
            raise forms.ValidationError('Value must be greater than 0')
            
        return self.cleaned_data['round']

    def clean(self):
        self.cleaned_data = super(Task_Task_SetupAdminForm, self).clean()
        
        if self.cleaned_data and ('task' in self.cleaned_data and 'round' in self.cleaned_data):
            ck = Task_Task_Setup.objects.filter(task=self.cleaned_data['task']).filter(round=self.cleaned_data['round'])
            if self.id:
                ck = ck.exclude(pk=self.id)
            if ck.count() > 0:
                raise forms.ValidationError({'task': ['Pattern and round pair must be unique (system is case insensitive)'], 'round': ['Pattern and round pair must be unique (system is case insensitive)']})
        
        return self.cleaned_data

class Worker_CampaignForm(forms.ModelForm):
    campaign          = forms.ModelChoiceField(label='Campaign', queryset=Campaign.objects.order_by('name').all())
    worker            = forms.ModelChoiceField(label='Worker', help_text='Select user from the "Workers" group', queryset=Worker.objects.all())
    worker_banned     = forms.BooleanField(label='Banned', help_text='User will no longer be able to access to this campaign', required=False)
    c_score           = forms.DecimalField(label='Score', help_text='Change the value of the score for this user in this campaign', widget=forms.NumberInput(), initial=0, min_value=0, decimal_places=2)
    c_trustworthiness = forms.DecimalField(label='Trustworthiness', help_text='Change the value of the trustworthiness for this user in this campaign', widget=forms.NumberInput(), initial=1, min_value=1, decimal_places=2)
    
    class Meta: 
        model  = Worker_Campaign
        fields = ('campaign', 'worker', 'worker_banned', 'c_score', 'c_trustworthiness')
        
    def __init__(self, *args, **kwargs): 
        super().__init__(*args, **kwargs)
        
        # filter ModelChoiceField if user is not Administrator (if is Requester)
        if not self.requestIsAdmin:
            self.fields['task'].queryset = Task.objects.filter(campaign_id__requester_id=self.requestUser)
        
        if 'instance' in kwargs and kwargs['instance'] is not None:
            self.id = kwargs['instance'].id
        else:
            self.id = None

        for field in self._meta.fields:
            if self.fields[field].required:
                attrs = {'class': 'form-control required'}
            else:
                attrs = {'class': 'form-control'}
            self.fields[field].widget.attrs.update(attrs)

    def clean(self):
        self.cleaned_data = super(Worker_CampaignForm, self).clean()

        if self.cleaned_data and ('campaign' in self.cleaned_data and 'worker' in self.cleaned_data):
            ck = Worker_Campaign.objects.filter(campaign=self.cleaned_data['campaign']).filter(worker=self.cleaned_data['worker'])
            if self.id:
                ck = ck.exclude(pk=self.id)
            if ck.count() > 0:
                raise forms.ValidationError({'campaign': ['Worker is already subscribed to this campaign'], 'worker': ['Worker is already subscribed to this campaign']})
        
        return self.cleaned_data
        
class Worker_TaskForm(forms.ModelForm):
    worker            = forms.ModelChoiceField(label='Worker', queryset=Worker.objects.all())
    task              = forms.ModelChoiceField(label='Task', queryset=Task.objects.order_by('description').all())
    round             = forms.IntegerField(label='Round', widget=forms.NumberInput(), initial=1)
    current_w_trustworthiness = forms.DecimalField(label='Trustworthiness', widget=forms.NumberInput(), initial=1, min_value=1, decimal_places=2)
    timeout           = forms.BooleanField(label='Timeout', required=False)
    answer            = forms.CharField(label='Answer', widget=forms.TextInput(attrs={'readonly':'readonly'}), required=False)
    timestamp_out     = forms.CharField(label='Start Date', widget=forms.DateInput(attrs={'type': 'date'}))
    
    class Meta: 
        model  = Worker_Task
        fields = ('worker', 'task', 'round', 'current_w_trustworthiness', 'timeout', 'answer', 'timestamp_out')

    def __init__(self, *args, **kwargs): 
        super().__init__(*args, **kwargs)
        
        # filter ModelChoiceField if user is not Administrator (if is Requester)
        if not self.requestIsAdmin:
            self.fields['task'].queryset = Task.objects.filter(campaign_id__requester_id=self.requestUser)
            
        for field in self._meta.fields:
            if self.fields[field].required:
                attrs = {'class': 'form-control required'}
            else:
                attrs = {'class': 'form-control'}
            self.fields[field].widget.attrs.update(attrs)
            
class WorkerAdminCreateForm(forms.ModelForm):
    id                = forms.ModelChoiceField(label='User', help_text='Select user from the "Workers" group', queryset=User.objects.filter(groups__name='Workers').exclude(id__in=(Worker.objects.values('id'))).order_by('username'))
    birth_date        = forms.CharField(label='Birth Date', widget=forms.DateInput(attrs={'type': 'date'}))
    genre             = forms.CharField(label='Genre', widget=forms.Select(choices=(('M', 'Male'), ('F', 'Famale'))))
    banned            = forms.BooleanField(label='Banned', help_text='User will no longer be able to access to the application', required=False)
    w_score           = forms.DecimalField(label='Score', help_text='Change the value of the score for this user', widget=forms.NumberInput(), initial=0, min_value=0, decimal_places=2)
    w_trustworthiness = forms.DecimalField(label='Trustworthiness', help_text='Change the value of the trustworthiness for this user', widget=forms.NumberInput(), initial=1, min_value=1, decimal_places=2)
    
    class Meta: 
        model  = Worker
        fields = ('id', 'birth_date', 'genre', 'banned', 'w_score', 'w_trustworthiness')

    def __init__(self, *args, **kwargs): 
        super().__init__(*args, **kwargs)
        for field in self._meta.fields:
            if self.fields[field].required:
                attrs = {'class': 'form-control required'}
            else:
                attrs = {'class': 'form-control'}
            self.fields[field].widget.attrs.update(attrs)
            
class WorkerAdminUpdateForm(forms.ModelForm):
    id                = forms.ModelChoiceField(label='User', help_text='Select user from the "Workers" group', queryset=User.objects.filter(groups__name='Workers'))
    birth_date        = forms.CharField(label='Birth Date', widget=forms.DateInput(attrs={'type': 'date'}))
    genre             = forms.CharField(label='Genre', widget=forms.Select(choices=(('M', 'Male'), ('F', 'Famale'))))
    banned            = forms.BooleanField(label='Banned', help_text='User will no longer be able to access to the application', required=False)
    w_score           = forms.DecimalField(label='Score', help_text='Change the value of score for this user', widget=forms.NumberInput(), initial=0, min_value=0, decimal_places=2)
    w_trustworthiness = forms.DecimalField(label='Trustworthiness', help_text='Change the value of trustworthiness for this user', widget=forms.NumberInput(), initial=1, min_value=1, decimal_places=2)
    
    class Meta: 
        model  = Worker
        fields = ('id', 'birth_date', 'genre', 'banned', 'w_score', 'w_trustworthiness')

    def __init__(self, *args, **kwargs): 
        super().__init__(*args, **kwargs)
        for field in self._meta.fields:
            if self.fields[field].required:
                attrs = {'class': 'form-control required'}
            else:
                attrs = {'class': 'form-control'}
            self.fields[field].widget.attrs.update(attrs)
            
class Campaign_StatusAdminForm(forms.ModelForm):
    name = forms.CharField(label='Name')
    
    class Meta: 
        model  = Campaign_Status
        fields = ('name', )

    def __init__(self, *args, **kwargs): 
        super().__init__(*args, **kwargs)
        
        if 'instance' in kwargs and kwargs['instance'] is not None:
            self.id = kwargs['instance'].id
        else:
            self.id = None
            
        for field in self._meta.fields:
            if self.fields[field].required:
                attrs = {'class': 'form-control required'}
            else:
                attrs = {'class': 'form-control'}
            self.fields[field].widget.attrs.update(attrs)
            
    def clean(self):
        self.cleaned_data = super(Campaign_StatusAdminForm, self).clean()
        
        if self.cleaned_data and 'name' in self.cleaned_data:
            ck = Campaign_Status.objects.filter(name__iexact=self.cleaned_data['name'])
            if self.id:
                ck = ck.exclude(pk=self.id)
            if ck.count() > 0:
                raise forms.ValidationError({'name': ['Value must be unique (system is case insensitive)']})
        
        return self.cleaned_data
        
class Task_StatusAdminForm(forms.ModelForm):
    name = forms.CharField(label='Name')
    
    class Meta: 
        model  = Task_Status
        fields = ('name', )

    def __init__(self, *args, **kwargs): 
        super().__init__(*args, **kwargs)
        
        if 'instance' in kwargs and kwargs['instance'] is not None:
            self.id = kwargs['instance'].id
        else:
            self.id = None
            
        for field in self._meta.fields:
            if self.fields[field].required:
                attrs = {'class': 'form-control required'}
            else:
                attrs = {'class': 'form-control'}
            self.fields[field].widget.attrs.update(attrs)
            
    def clean(self):
        self.cleaned_data = super(Task_StatusAdminForm, self).clean()
        
        if self.cleaned_data and 'name' in self.cleaned_data:
            ck = Task_Status.objects.filter(name__iexact=self.cleaned_data['name'])
            if self.id:
                ck = ck.exclude(pk=self.id)
            if ck.count() > 0:
                raise forms.ValidationError({'name': ['Value must be unique (system is case insensitive)']})
        
        return self.cleaned_data
        
class Task_SetupAdminForm(forms.ModelForm):
    num_worker          = forms.IntegerField(label='Number of Worker', widget=forms.NumberInput(), initial=5, min_value=1)
    min_trustworthiness = forms.DecimalField(label='Trustworthiness', help_text='Enter the required minimum value', widget=forms.NumberInput(), initial=1, min_value=1, decimal_places=2)
    consensus_threshold = forms.DecimalField(label='Consensus Threshold', help_text='Used to calculate the finale answer of the task', initial=0.5, min_value=0, max_value=1, decimal_places=1, widget=forms.NumberInput())
    max_exec_time       = forms.IntegerField(label='Maximum Execution Time', help_text='Minutes available to the worker to answer the question', initial=60, min_value=1, widget=forms.NumberInput())
    
    class Meta: 
        model  = Task_Setup
        fields = ('num_worker', 'min_trustworthiness', 'consensus_threshold', 'max_exec_time')

    def __init__(self, *args, **kwargs): 
        super().__init__(*args, **kwargs)
        for field in self._meta.fields:
            if self.fields[field].required:
                attrs = {'class': 'form-control required'}
            else:
                attrs = {'class': 'form-control'}
            self.fields[field].widget.attrs.update(attrs)
            
    def clean_num_worker(self):
        if self.cleaned_data['num_worker'] < 1:
            raise forms.ValidationError('Value must be greater than 0')
            
        return self.cleaned_data['num_worker']
            
    def clean_min_trustworthiness(self):
        if self.cleaned_data['min_trustworthiness'] < 0:
            raise forms.ValidationError('Value must be greater or equal than 0')
            
        return self.cleaned_data['min_trustworthiness']
            
    def clean_consensus_threshold(self):
        if self.cleaned_data['consensus_threshold'] < 0:
            raise forms.ValidationError('Value must be greater or equal than 0')
            
        return self.cleaned_data['consensus_threshold']
            
    def clean_max_exec_time(self):
        if self.cleaned_data['max_exec_time'] < 1:
            raise forms.ValidationError('Value must be greater than 0')
            
        return self.cleaned_data['max_exec_time']
            
class PatternAdminForm(forms.ModelForm):
    description = forms.CharField(label='Name')
    
    class Meta: 
        model  = Pattern
        fields = ('description',)

    def __init__(self, *args, **kwargs): 
        super().__init__(*args, **kwargs)
        
        if 'instance' in kwargs and kwargs['instance'] is not None:
            self.id = kwargs['instance'].id
        else:
            self.id = None
            
        for field in self._meta.fields:
            if self.fields[field].required:
                attrs = {'class': 'form-control required'}
            else:
                attrs = {'class': 'form-control'}
            self.fields[field].widget.attrs.update(attrs)
            
    def clean(self):
        self.cleaned_data = super(PatternAdminForm, self).clean()
        
        if self.cleaned_data and 'description' in self.cleaned_data:
            ck = Pattern.objects.filter(description__iexact=self.cleaned_data['description'])
            if self.id:
                ck = ck.exclude(pk=self.id)
            if ck.count() > 0:
                raise forms.ValidationError({'description': ['Value must be unique (system is case insensitive)']})
        
        return self.cleaned_data

class Task_Setup_PatternAdminForm(forms.ModelForm):
    pattern    = forms.ModelChoiceField(label='Pattern', help_text='Select a pattern to represent this setup', queryset=Pattern.objects.order_by('description').all())
    round      = forms.IntegerField(label='Round', help_text='Select the round to apply this setup', widget=forms.NumberInput(), initial=1)
    task_setup = forms.ModelChoiceField(label='Task Setup', queryset=Task_Setup.objects.order_by('num_worker').all())
    
    class Meta: 
        model  = Task_Setup_Pattern
        fields = ('pattern', 'round', 'task_setup')

    def __init__(self, *args, **kwargs): 
        super().__init__(*args, **kwargs)
        
        if 'instance' in kwargs and kwargs['instance'] is not None:
            self.id = kwargs['instance'].id
        else:
            self.id = None
            
        for field in self._meta.fields:
            if self.fields[field].required:
                attrs = {'class': 'form-control required'}
            else:
                attrs = {'class': 'form-control'}
            self.fields[field].widget.attrs.update(attrs)
            
    def clean_round(self):
        if self.cleaned_data['round'] < 1:
            raise forms.ValidationError('Value must be greater than 0')
            
        return self.cleaned_data['round']

    def clean(self):
        self.cleaned_data = super(Task_Setup_PatternAdminForm, self).clean()
        
        if self.cleaned_data and ('pattern' in self.cleaned_data and 'round' in self.cleaned_data):
            ck = Task_Setup_Pattern.objects.filter(pattern=self.cleaned_data['pattern']).filter(round=self.cleaned_data['round'])
            if self.id:
                ck = ck.exclude(pk=self.id)
            if ck.count() > 0:
                raise forms.ValidationError({'pattern': ['Pattern and round pair must be unique (system is case insensitive)'], 'round': ['Pattern and round pair must be unique (system is case insensitive)']})
        
        return self.cleaned_data

class Answer_TypeAdminForm(forms.ModelForm):
    name = forms.CharField(label='Name')
    
    class Meta: 
        model  = Answer_Type
        fields = ('name', )

    def __init__(self, *args, **kwargs): 
        super().__init__(*args, **kwargs)

        if 'instance' in kwargs and kwargs['instance'] is not None:
            self.id = kwargs['instance'].id
        else:
            self.id = None

        for field in self._meta.fields:
            if self.fields[field].required:
                attrs = {'class': 'form-control required'}
            else:
                attrs = {'class': 'form-control'}
            self.fields[field].widget.attrs.update(attrs)

    def clean(self):
        self.cleaned_data = super(Answer_TypeAdminForm, self).clean()
        
        if self.cleaned_data and 'name' in self.cleaned_data:
            ck = Answer_Type.objects.filter(name__iexact=self.cleaned_data['name'])
            if self.id:
                ck = ck.exclude(pk=self.id)
            if ck.count() > 0:
                raise forms.ValidationError({'name': ['Value must be unique (system is case insensitive)']})
        
        return self.cleaned_data
            
class SkillAdminForm(forms.ModelForm):
    name  = forms.CharField(label='Name')
    value = forms.FloatField(label='Value')
    
    class Meta: 
        model  = Skill
        fields = ('name', 'value')

    def __init__(self, *args, **kwargs): 
        super().__init__(*args, **kwargs)
        
        if 'instance' in kwargs and kwargs['instance'] is not None:
            self.id = kwargs['instance'].id
        else:
            self.id = None
            
        for field in self._meta.fields:
            if self.fields[field].required:
                attrs = {'class': 'form-control required'}
            else:
                attrs = {'class': 'form-control'}
            self.fields[field].widget.attrs.update(attrs)
    
    def clean(self):
        self.cleaned_data = super(SkillAdminForm, self).clean()
        
        if self.cleaned_data and ('name' in self.cleaned_data and 'value' in self.cleaned_data):
            ck = Skill.objects.filter(name__iexact=self.cleaned_data['name']).filter(value=self.cleaned_data['value'])
            if self.id:
                ck = ck.exclude(pk=self.id)
            if ck.count() > 0:
                raise forms.ValidationError({'name': ['Skill and value pair must be unique (system is case insensitive)'], 'value': ['Skill and value pair must be unique (system is case insensitive)']})
        
        return self.cleaned_data
            
class ProfileAdminForm(forms.ModelForm):
    description = forms.CharField(label='Name')
    
    class Meta: 
        model  = Profile
        fields = ('description',)

    def __init__(self, *args, **kwargs): 
        super().__init__(*args, **kwargs)
        
        if 'instance' in kwargs and kwargs['instance'] is not None:
            self.id = kwargs['instance'].id
        else:
            self.id = None
            
        for field in self._meta.fields:
            if self.fields[field].required:
                attrs = {'class': 'form-control required'}
            else:
                attrs = {'class': 'form-control'}
            self.fields[field].widget.attrs.update(attrs)
    
    def clean(self):
        self.cleaned_data = super(ProfileAdminForm, self).clean()
        
        if self.cleaned_data and 'description' in self.cleaned_data:
            ck = Profile.objects.filter(description__iexact=self.cleaned_data['description'])
            if self.id:
                ck = ck.exclude(pk=self.id)
            if ck.count() > 0:
                raise forms.ValidationError({'description': ['Value must be unique (system is case insensitive)']})
        
        return self.cleaned_data
            
class Profile_SkillAdminForm(forms.ModelForm):
    profile = forms.ModelChoiceField(label='Profile', help_text='Select a profile to represent this skill', queryset=Profile.objects.order_by('description').all())
    skill   = forms.ModelChoiceField(label='Skill', queryset=Skill.objects.order_by('name', 'value').all())
    
    class Meta: 
        model  = Profile_Skill
        fields = ('profile', 'skill')

    def __init__(self, *args, **kwargs): 
        super().__init__(*args, **kwargs)
        
        if 'instance' in kwargs and kwargs['instance'] is not None:
            self.id = kwargs['instance'].id
        else:
            self.id = None
            
        for field in self._meta.fields:
            if self.fields[field].required:
                attrs = {'class': 'form-control required'}
            else:
                attrs = {'class': 'form-control'}
            self.fields[field].widget.attrs.update(attrs)
    
    def clean(self):
        self.cleaned_data = super(Profile_SkillAdminForm, self).clean()
        
        if self.cleaned_data and ('profile' in self.cleaned_data and 'skill' in self.cleaned_data):
            ck = Profile_Skill.objects.filter(profile=self.cleaned_data['profile']).filter(skill=self.cleaned_data['skill'])
            if self.id:
                ck = ck.exclude(pk=self.id)
            if ck.count() > 0:
                raise forms.ValidationError({'profile': ['Profile and skill pair must be unique (system is case insensitive)'], 'skill': ['Profile and skill pair must be unique (system is case insensitive)']})
        
        return self.cleaned_data
            
class Task_SkillAdminForm(forms.ModelForm):
    task  = forms.ModelChoiceField(label='Task', queryset=Task.objects.order_by('description').all())
    skill = forms.ModelChoiceField(label='Skill', queryset=Skill.objects.order_by('name', 'value').all())
    
    class Meta: 
        model  = Task_Skill
        fields = ('task', 'skill')

    def __init__(self, *args, **kwargs): 
        super().__init__(*args, **kwargs)
        
        # filter ModelChoiceField if user is not Administrator (if is Requester)
        if not self.requestIsAdmin:
            self.fields['task'].queryset = Task.objects.filter(campaign_id__requester_id=self.requestUser)
        
        if 'instance' in kwargs and kwargs['instance'] is not None:
            self.id = kwargs['instance'].id
        else:
            self.id = None
            
        for field in self._meta.fields:
            if self.fields[field].required:
                attrs = {'class': 'form-control required'}
            else:
                attrs = {'class': 'form-control'}
            self.fields[field].widget.attrs.update(attrs)
    
    def clean(self):
        self.cleaned_data = super(Task_SkillAdminForm, self).clean()
        
        if self.cleaned_data and ('task' in self.cleaned_data and 'skill' in self.cleaned_data):
            ck = Task_Skill.objects.filter(task=self.cleaned_data['task']).filter(skill=self.cleaned_data['skill'])
            if self.id:
                ck = ck.exclude(pk=self.id)
            if ck.count() > 0:
                raise forms.ValidationError({'task': ['Task and skill pair must be unique (system is case insensitive)'], 'skill': ['Task and skill pair must be unique (system is case insensitive)']})
        
        return self.cleaned_data
        
# AdminForm (Django_auth)
class UserForm(forms.ModelForm):
    first_name       = forms.CharField(label='First Name')
    last_name        = forms.CharField(label='Last Name')
    username         = forms.CharField(label='Username', help_text='Required. 150 characters or fewer. Letters, digits and @/./+/-/_ only.')
    email            = forms.CharField(label='Email', widget=forms.EmailInput())
    is_active        = forms.BooleanField(label='Active', help_text='Designates whether this user should be treated as active. Unselect this instead of deleting accounts.', required=False)
    is_staff         = forms.BooleanField(label='Staff', help_text='Designates whether the user can log into this admin site.', required=False)
    is_superuser     = forms.BooleanField(label='Superuser', help_text='Designates that this user has all permissions without explicitly assigning them.', required=False)
    groups           = forms.ModelMultipleChoiceField(queryset=Group.objects.order_by('name').all(), label='Group')
    password         = forms.CharField(label='Password', help_text='Leave blank to generate new password.', widget=forms.PasswordInput())
    
    class Meta: 
        model  = User
        fields = ('first_name', 'last_name', 'username', 'email', 'is_active', 'is_staff', 'is_superuser', 'password', 'groups')

    def __init__(self, *args, **kwargs): 
        super().__init__(*args, **kwargs)
        for field in self._meta.fields:
            if self.fields[field].required:
                attrs = {'class': 'form-control required'}
            else:
                attrs = {'class': 'form-control'}
            self.fields[field].widget.attrs.update(attrs)
            
    def clean_email(self):
        if User.objects.filter(email=self.cleaned_data['email']).exists():
            raise forms.ValidationError('This email is already associated with an account')
            
        return self.cleaned_data['email']
            
class UserUpdateForm(forms.ModelForm):
    first_name       = forms.CharField(label='First Name')
    last_name        = forms.CharField(label='Last Name')
    username         = forms.CharField(label='Username', help_text='Required. 150 characters or fewer. Letters, digits and @/./+/-/_ only.')
    email            = forms.CharField(label='Email', widget=forms.EmailInput())
    is_active        = forms.BooleanField(label='Active', help_text='Designates whether this user should be treated as active. Unselect this instead of deleting accounts.', required=False)
    is_staff         = forms.BooleanField(label='Staff', help_text='Designates whether the user can log into this admin site.', required=False)
    is_superuser     = forms.BooleanField(label='Superuser', help_text='Designates that this user has all permissions without explicitly assigning them.', required=False)
    groups           = forms.ModelMultipleChoiceField(queryset=Group.objects.all(), label='Group')
    
    class Meta: 
        model  = User
        fields = ('first_name', 'last_name', 'username', 'email', 'is_active', 'is_staff', 'is_superuser', 'groups')

    def __init__(self, *args, **kwargs): 
        super().__init__(*args, **kwargs)
        for field in self._meta.fields:
            if self.fields[field].required:
                attrs = {'class': 'form-control required'}
            else:
                attrs = {'class': 'form-control'}
            self.fields[field].widget.attrs.update(attrs)
            
class GroupForm(forms.ModelForm):
    name        = forms.CharField(label='Name')
    permissions = forms.ModelMultipleChoiceField(queryset=Permission.objects.all(), label='Permissions', help_text='Hold down "Control", or "Command" on a Mac, to select more than one.', widget=forms.SelectMultiple(attrs={'style': 'height:400px;'}))
    
    class Meta: 
        model  = Group
        fields = ('name', 'permissions')

    def __init__(self, *args, **kwargs): 
        super().__init__(*args, **kwargs)
        for field in self._meta.fields:
            if self.fields[field].required:
                attrs = {'class': 'form-control required'}
            else:
                attrs = {'class': 'form-control'}
            self.fields[field].widget.attrs.update(attrs)
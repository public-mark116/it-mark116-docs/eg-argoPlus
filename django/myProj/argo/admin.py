# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from django.core import serializers
from django.conf import settings
from django.core.mail import send_mail
from django.contrib.admin.utils import (
    NestedObjects, construct_change_message, flatten_fieldsets,
    get_deleted_objects, lookup_needs_distinct, model_format_dict, quote,
    unquote,
)
from django import forms
from django.core.exceptions import ValidationError
from django.contrib.auth.models import User, Group, Permission
from .models import *
from .forms import *
from django.forms.models import BaseInlineFormSet

import json

admin.site.site_header = settings.SITE_NAME.upper()
admin.site.site_title  = settings.SITE_NAME
admin.site.index_template     = 'admin/index.html'
admin.site.app_index_template = 'admin/index.html'
admin.autodiscover()

class TaskInlineFormSet(BaseInlineFormSet):
    limit_display = 100  # limit of inline-element to display
        
    # inline-element to display
    def get_queryset(self) :
        qs = super(TaskInlineFormSet, self).get_queryset()
        return qs.order_by('priority')[:self.limit_display]
    
class TaskInline(admin.StackedInline):
    model   = Task
    extra   = 0                     # initial inline-element count
    form    = TaskInlineAdminForm
    formset = TaskInlineFormSet     # inline-element to display
    
    # limit of inline-element to display
    limit_display = formset.limit_display
    
    # list of field to display
    classes   = ['collapse']
    fieldsets = [
        (None, {'fields': ['campaign', 'description', 'content', 'task_status', 'answer_type', 'priority']}),
    ]
    
class CampaignAdmin(admin.ModelAdmin):
    model_name    = 'campaign'
    
    list_per_page = 15
    list_display  = ('action_column', 'name', 'requester', 'campaign_status', 'start_date', 'finish_date', 'start_signup', 'finish_signup')
    list_display_links = None
    list_filter   = ['campaign_status'] 
    ordering      = ['start_date', 'name']
    search_fields = ['name']
        
    # overwrite has_change_permission to grant view permission
    def has_change_permission(self, request, obj=None):
        # user can change?
        if request.user.has_perm('argo.change_campaign'):
            return True

        # user can view changelist_view?
        if (not obj) and (request.user.has_perm('argo.view_campaign')):
            return True

        return False
        
    # remove action "delete" to requester
    def get_actions(self, request):
        actions = super(CampaignAdmin, self).get_actions(request)
        if (not request.user.has_perm('argo.delete_campaign')) and ('delete_selected' in actions):
            del actions['delete_selected']
        return actions
        
    # define edit link for changeform_view
    def action_column(self, instance):
        change_link = u"<a href='/admin/%s/%s/%s/change' class='action_column_change' title='Change'><i class='fa fa-pencil' aria-hidden='true'></i></a>" % (instance._meta.app_label, self.model_name, instance.id)
        delete_link = u"<a href='/admin/%s/%s/%s/delete' class='action_column_delete' title='Delete'><i class='fa fa-trash' aria-hidden='true'></i></a>" % (instance._meta.app_label, self.model_name, instance.id)
        return change_link + ' ' + delete_link
    action_column.short_description = ''
    action_column.allow_tags = True
        
    # set query of changelist_view (to show different elements to administrator or to requester)
    def get_queryset(self, request):
        qs = super(CampaignAdmin, self).get_queryset(request)
        if request.user.groups.filter(name__iexact='requester').exists():
            return qs.filter(requester=request.user)
        else:
            return qs
            
    # set page title/menu active of changelist (manage model page)
    def changelist_view(self, request, extra_context=None):
        extra_context = extra_context or {}
        extra_context['page_title']   = Campaign._meta.verbose_name_plural + ' Manage'
        extra_context['xPanel_title'] = Campaign._meta.verbose_name_plural
        extra_context['model_name']   = self.model_name
        extra_context['permission']   = {
                                         'view':   int(request.user.has_perm('argo.view_campaign')),
                                         'change': int(request.user.has_perm('argo.change_campaign')),
                                         'delete': int(request.user.has_perm('argo.delete_campaign'))
                                        }
        extra_context['menu_active']  = 'campaign'
        return super(CampaignAdmin, self).changelist_view(request, extra_context=extra_context)
        
    # set form
    def get_form(self, request, obj=None, **kwargs):
        if request.user.groups.filter(name__iexact='requester').exists():
            return CampaignRequesterAdminForm
        else:
            return CampaignAdminForm
    
    # set fields of form    
    def get_fieldsets(self, request, obj=None):
        if request.user.groups.filter(name__iexact='requester').exists():
            return [
                      (None,               {'fields': ['name', 'campaign_status']}),
                      ('Advance Settings', {'fields': ['start_signup', 'finish_signup', 'start_date', 'finish_date', 'init_trustworthiness', 'score_increment']}),
                      ('Tasks JSON',       {'fields': ['taskjson']}),
                   ]
        else:
            return [
                      (None,               {'fields': ['name', 'requester', 'campaign_status']}),
                      ('Advance Settings', {'fields': ['start_signup', 'finish_signup', 'start_date', 'finish_date', 'init_trustworthiness', 'score_increment']}),
                      ('Tasks JSON',       {'fields': ['taskjson']}),
                   ]
                   
    # hide inline fields in create
    def get_formsets_with_inlines(self, request, obj=None):
        if obj:
            for inline in self.get_inline_instances(request, obj):
                yield inline.get_formset(request, obj), inline

    # set inline part of form
    inlines = [TaskInline,]
    
    # overwrite save method
    def save_model(self, request, obj, form, change):
        # add requester instance if user is a Requester and is create
        if (not change) and (request.user.groups.filter(name__iexact='requester').exists()): 
            obj.requester = request.user
            
        super(CampaignAdmin, self).save_model(request, obj, form, change)
        
        # convert json to task if json field is not emtpy
        if form.cleaned_data['taskjson']:
            key      = [('description', 'request'), ('priority', 'priority'), ('content', 'content'), ('id_origin', 'id_origin'), ('gold_answer', 'gold_answer')]
            taskjson = json.loads(form.cleaned_data['taskjson'])
            
            for task in taskjson['tasks']:
                # insert task
                t = Task(campaign=obj, current_round=1)
                
                if 'type' in task:
                    setattr(t, 'answer_type', Answer_Type.objects.get(name__iexact=task['type'].lower()))
                    
                if 'status' in task:
                    setattr(t, 'task_status', Task_Status.objects.get(name__iexact=task['status'].lower()))
                else:  # if status is empty set it to "ready to execute"
                    setattr(t, 'task_status', Task_Status.objects.get(name__iexact='ready to execute'))
                    
                for k in key:
                    if k[1] in task:
                        setattr(t, k[0], task[k[1]])
                    elif k[1]=="priority":  # if priority is empty set it to 1000
                        setattr(t, k[0], 1000)
                t.save()
                # insert task
                
                # insert task_choice
                if task['type'].lower()=="choice":
                    for i in task['choices']:
                        c = Task_Choice(task=t, id_origin=i, description=task['choices'][i])
                        c.save()
                # insert task_choice
                
                # insert task_setup
                if 'pattern' in task:
                    pattern = Task_Setup_Pattern.objects.filter(pattern__description__iexact=task['pattern'].lower())
                    for setup in pattern:
                        ts = Task_Task_Setup(task=t, round=setup.round, task_setup=setup.task_setup)
                        ts.save()
                # insert task_setup
                
                # insert task_skill
                if 'profile' in task:
                    profile = Profile_Skill.objects.filter(profile__description__iexact=task['profile'].lower())
                    for skill in profile:
                        ts = Task_Skill(task=t, skill=skill.skill)
                        ts.save()
                # insert task_skill
                
    # set page title/menu active of changeform_view (create/update model page)
    def changeform_view(self, request, object_id=None, form_url='', extra_context=None):
        extra_context = extra_context or {}
        extra_context['xPanel_title'] = Campaign._meta.verbose_name
        extra_context['model_name']   = self.model_name
        if object_id:
            extra_context['page_title']            = Campaign._meta.verbose_name + ' Edit'
            extra_context['menu_active']           = 'campaign'
            extra_context['readonly_field']        = 'requester'
            extra_context['readonly_inline_field'] = json.dumps([{'inline': 'task', 'fields': 'answer_type'},])
        else:
            extra_context['page_title']            = Campaign._meta.verbose_name + ' Add'
            extra_context['menu_active']           = 'campaign_add'
        return super(CampaignAdmin, self).changeform_view(request, object_id=object_id, form_url=form_url, extra_context=extra_context)
        
    # set page title/menu active of history_view (history model page)
    def history_view(self, request, object_id, extra_context=None):
        extra_context = extra_context or {}
        extra_context['page_title']   = Campaign._meta.verbose_name + ' History'
        extra_context['xPanel_title'] = Campaign._meta.verbose_name
        extra_context['model_name']   = self.model_name
        extra_context['menu_active']  = 'campaign'
        return super(CampaignAdmin, self).history_view(request, object_id=object_id, extra_context=extra_context)

    # set page title/menu active of delete_view (delete model page)
    def delete_view(self, request, object_id, extra_context=None):
        extra_context = extra_context or {}
        extra_context['page_title']   = Campaign._meta.verbose_name + ' Delete'
        extra_context['xPanel_title'] = Campaign._meta.verbose_name
        extra_context['model_name']   = self.model_name
        extra_context['menu_active']  = 'campaign'
        return super(CampaignAdmin, self).delete_view(request, object_id=object_id, extra_context=extra_context)
        
admin.site.register(Campaign, CampaignAdmin)
        
class Task_Task_SetupInlineFormSet(BaseInlineFormSet):
    limit_display = 100  # limit of inline-element to display
        
    # inline-element to display
    def get_queryset(self) :
        qs = super(Task_Task_SetupInlineFormSet, self).get_queryset()
        return qs.order_by('round')[:self.limit_display]
    
class Task_Task_SetupInline(admin.StackedInline):
    model   = Task_Task_Setup
    extra   = 0                                # initial inline-element count
    form    = Task_Task_SetupAdminForm
    formset = Task_Task_SetupInlineFormSet     # inline-element to display
    
    # limit of inline-element to display
    limit_display = formset.limit_display
    
    # list of field to display
    classes   = ['collapse']
    fieldsets = [
        (None, {'fields': ['round', 'task_setup']}),
    ]
    
class Task_SkillFormSet(BaseInlineFormSet):
    limit_display = 100  # limit of inline-element to display
    
    # inline-element to display
    def get_queryset(self) :
        qs = super(Task_SkillFormSet, self).get_queryset()
        return qs.order_by('skill__name', 'skill__value')[:self.limit_display]
        
class Task_SkillInline(admin.StackedInline):
    model   = Task_Skill
    extra   = 0                     # initial inline-element count
    form    = Task_SkillAdminForm 
    formset = Task_SkillFormSet     # inline-element to display
    
    # limit of inline-element to display
    limit_display = formset.limit_display
    
    # list of field to display
    classes   = ['collapse']
    fieldsets = [
        (None, {'fields': ['task', 'skill']}),
    ]
   
class Task_ChoiceFormSet(BaseInlineFormSet):
    limit_display = 100  # limit of inline-element to display
    
    # inline-element to display
    def get_queryset(self) :
        qs = super(Task_ChoiceFormSet, self).get_queryset()
        return qs.order_by('description')[:self.limit_display]
        
class Task_ChoiceInline(admin.StackedInline):
    model   = Task_Choice
    extra   = 0                     # initial inline-element count
    form    = Task_ChoiceAdminForm 
    formset = Task_ChoiceFormSet    # inline-element to display
    
    # limit of inline-element to display
    limit_display = formset.limit_display
    
    # list of field to display
    fieldsets = [
        (None, {'fields': ['task', 'description', 'id_origin']}),
    ]
    
class TaskAdmin(admin.ModelAdmin):
    model_name    = 'task'

    list_per_page = 15
    list_display  = ('action_column', 'campaign', 'description', 'task_status', 'answer_type', 'insert_date', 'id_origin', 'current_round')
    list_display_links = None
    list_filter   = [('campaign', admin.RelatedOnlyFieldListFilter), 'task_status', 'answer_type']
    ordering      = ['campaign', 'description']
    search_fields = ['description', 'content', 'id_origin', 'campaign__name']

    # overwrite has_change_permission to grant view permission
    def has_change_permission(self, request, obj=None):
        # user can change?
        if request.user.has_perm('argo.change_task'):
            return True

        # user can view changelist_view?
        if (not obj) and (request.user.has_perm('argo.view_task')):
            return True

        return False
        
    # remove action "delete" to requester
    def get_actions(self, request):
        actions = super(TaskAdmin, self).get_actions(request)
        if (not request.user.has_perm('argo.delete_task')) and ('delete_selected' in actions):
            del actions['delete_selected']
        return actions
        
    # define edit link for changeform_view
    def action_column(self, instance):
        change_link = u"<a href='/admin/%s/%s/%s/change' class='action_column_change' title='Change'><i class='fa fa-pencil' aria-hidden='true'></i></a>" % (instance._meta.app_label, self.model_name, instance.id)
        delete_link = u"<a href='/admin/%s/%s/%s/delete' class='action_column_delete' title='Delete'><i class='fa fa-trash' aria-hidden='true'></i></a>" % (instance._meta.app_label, self.model_name, instance.id)
        return change_link + ' ' + delete_link
    action_column.short_description = ''
    action_column.allow_tags = True
    
    # set query of changelist_view (to show different elements to administrator or to requester)
    def get_queryset(self, request):
        qs = super(TaskAdmin, self).get_queryset(request)
        if request.user.groups.filter(name__iexact='requester').exists():
            return qs.filter(campaign_id__requester=request.user)
        else:
            return qs
            
    # set page title/menu active of changelist (manage model page)
    def changelist_view(self, request, extra_context=None):
        extra_context = extra_context or {}
        extra_context['page_title']   = Task._meta.verbose_name_plural + ' Manage'
        extra_context['xPanel_title'] = Task._meta.verbose_name_plural
        extra_context['model_name']   = self.model_name
        extra_context['permission']   = {
                                         'view':   int(request.user.has_perm('argo.view_task')),
                                         'change': int(request.user.has_perm('argo.change_task')),
                                         'delete': int(request.user.has_perm('argo.delete_task'))
                                        }
        extra_context['menu_active']  = 'task'
        return super(TaskAdmin, self).changelist_view(request, extra_context=extra_context)
        
    # set form (and filter ModelChoiceField)
    def get_form(self, request, obj=None, **kwargs):
        form = TaskAdminForm
        form.requestUser    = request.user
        form.requestIsAdmin = request.user.groups.filter(name__iexact='administrator').exists()

        # init inlines form
        subform1 = Task_Task_SetupAdminForm
        subform1.requestUser    = request.user
        subform1.requestIsAdmin = request.user.groups.filter(name__iexact='administrator').exists()
        
        subform2 = Task_SkillAdminForm
        subform2.requestUser    = request.user
        subform2.requestIsAdmin = request.user.groups.filter(name__iexact='administrator').exists()
        
        subform3 = Task_ChoiceAdminForm
        subform3.requestUser    = request.user
        subform3.requestIsAdmin = request.user.groups.filter(name__iexact='administrator').exists()

        return form
    
    # set fields of form    
    def get_fieldsets(self, request, obj=None):
        return [
                  (None,               {'fields': ['campaign', 'description', 'content', 'task_status']}),
                  ('Advance Settings', {'fields': ['answer_type', 'priority', 'id_origin']}),
                  ('Results',          {'fields': ['current_round', 'gold_answer', 'final_answer'], 'classes': ['collapse']}),
               ]
    
    # set inline part of form
    inlines = [Task_Task_SetupInline, Task_SkillInline, Task_ChoiceInline]
    
    # set page title/menu active of changeform_view (create/update model page)
    def changeform_view(self, request, object_id=None, form_url='', extra_context=None):
        extra_context = extra_context or {}
        extra_context['xPanel_title'] = Task._meta.verbose_name
        extra_context['model_name']   = self.model_name
        extra_context['menu_active']  = 'task'
        if object_id:
            extra_context['page_title']     = Task._meta.verbose_name + ' Edit'
            extra_context['readonly_field'] = 'campaign,answer_type'
        else:
            extra_context['page_title']     = Task._meta.verbose_name + ' Add'
        return super(TaskAdmin, self).changeform_view(request, object_id=object_id, form_url=form_url, extra_context=extra_context)
                
    # set page title/menu active of history_view (history model page)
    def history_view(self, request, object_id, extra_context=None):
        extra_context = extra_context or {}
        extra_context['page_title']   = Task._meta.verbose_name + ' History'
        extra_context['xPanel_title'] = Task._meta.verbose_name
        extra_context['model_name']   = self.model_name
        extra_context['menu_active']  = 'task'
        return super(TaskAdmin, self).history_view(request, object_id=object_id, extra_context=extra_context)

    # set page title/menu active of delete_view (delete model page)
    def delete_view(self, request, object_id, extra_context=None):
        extra_context = extra_context or {}
        extra_context['page_title']   = Task._meta.verbose_name + ' Delete'
        extra_context['xPanel_title'] = Task._meta.verbose_name
        extra_context['model_name']   = self.model_name
        extra_context['menu_active']  = 'task'
        return super(TaskAdmin, self).delete_view(request, object_id=object_id, extra_context=extra_context)
        
admin.site.register(Task, TaskAdmin)

class Task_ChoiceAdmin(admin.ModelAdmin):
    model_name    = 'task_choice'

    list_per_page = 15
    list_display  = ('action_column', 'get_campaign_name', 'task', 'description', 'id_origin')
    list_display_links = None
    ordering      = ['task', 'description']
    list_filter   = [('task__campaign', admin.RelatedOnlyFieldListFilter), 'task__task_status']
    search_fields = ['task__description', 'task__content', 'description', 'id_origin']
        
    # overwrite has_change_permission to grant view permission
    def has_change_permission(self, request, obj=None):
        # user can change?
        if request.user.has_perm('argo.change_task_choice'):
            return True

        # user can view changelist_view?
        if (not obj) and (request.user.has_perm('argo.view_task_choice')):
            return True

        return False
    
    # remove action "delete" to requester
    def get_actions(self, request):
        actions = super(Task_ChoiceAdmin, self).get_actions(request)
        if (not request.user.has_perm('argo.delete_task_choice')) and ('delete_selected' in actions):
            del actions['delete_selected']
        return actions
        
    # get external field
    def get_campaign_name(self, instance):
        return instance.task.campaign
    get_campaign_name.short_description = 'Campaign'
    get_campaign_name.admin_order_field = 'task__campaign__name'
        
    # define edit link for changeform_view
    def action_column(self, instance):
        change_link = u"<a href='/admin/%s/%s/%s/change' class='action_column_change' title='Change'><i class='fa fa-pencil' aria-hidden='true'></i></a>" % (instance._meta.app_label, self.model_name, instance.id)
        delete_link = u"<a href='/admin/%s/%s/%s/delete' class='action_column_delete' title='Delete'><i class='fa fa-trash' aria-hidden='true'></i></a>" % (instance._meta.app_label, self.model_name, instance.id)
        return change_link + ' ' + delete_link
    action_column.short_description = ''
    action_column.allow_tags = True
    
    # set query of changelist_view (to show different elements to administrator or to requester)
    def get_queryset(self, request):
        qs = super(Task_ChoiceAdmin, self).get_queryset(request)
        if request.user.groups.filter(name__iexact='requester').exists():
            return qs.filter(task_id__campaign_id__requester=request.user)
        else:
            return qs
            
    # set page title/menu active of changelist (manage model page)
    def changelist_view(self, request, extra_context=None):
        extra_context = extra_context or {}
        extra_context['page_title']   = Task_Choice._meta.verbose_name_plural + ' Manage'
        extra_context['xPanel_title'] = Task_Choice._meta.verbose_name_plural
        extra_context['model_name']   = self.model_name
        extra_context['permission']   = {
                                         'view':   int(request.user.has_perm('argo.view_task_choice')),
                                         'change': int(request.user.has_perm('argo.change_task_choice')),
                                         'delete': int(request.user.has_perm('argo.delete_task_choice'))
                                        }
        extra_context['menu_active']  = 'task_choice'
        return super(Task_ChoiceAdmin, self).changelist_view(request, extra_context=extra_context)
    
    # set form (and filter ModelChoiceField)
    def get_form(self, request, obj=None, **kwargs):
        form = Task_ChoiceAdminForm
        form.requestUser    = request.user
        form.requestIsAdmin = request.user.groups.filter(name__iexact='administrator').exists()
        return form
        
    # set page title/menu active of changeform_view (create/update model page)
    def changeform_view(self, request, object_id=None, form_url='', extra_context=None):
        extra_context = extra_context or {}
        extra_context['xPanel_title'] = Task_Choice._meta.verbose_name
        extra_context['model_name']   = self.model_name
        extra_context['menu_active']  = 'task_choice'
        if object_id:
            extra_context['page_title']     = Task_Choice._meta.verbose_name + ' Edit'
            extra_context['readonly_field'] = 'task'
        else:
            extra_context['page_title']     = Task_Choice._meta.verbose_name + ' Add'
        return super(Task_ChoiceAdmin, self).changeform_view(request, object_id=object_id, form_url=form_url, extra_context=extra_context)
        
    # set page title/menu active of history_view (history model page)
    def history_view(self, request, object_id, extra_context=None):
        extra_context = extra_context or {}
        extra_context['page_title']   = Task_Choice._meta.verbose_name + ' History'
        extra_context['xPanel_title'] = Task_Choice._meta.verbose_name
        extra_context['model_name']   = self.model_name
        extra_context['menu_active']  = 'task_choice'
        return super(Task_ChoiceAdmin, self).history_view(request, object_id=object_id, extra_context=extra_context)

    # set page title/menu active of delete_view (delete model page)
    def delete_view(self, request, object_id, extra_context=None):
        extra_context = extra_context or {}
        extra_context['page_title']   = Task_Choice._meta.verbose_name + ' Delete'
        extra_context['xPanel_title'] = Task_Choice._meta.verbose_name
        extra_context['model_name']   = self.model_name
        extra_context['menu_active']  = 'task_choice'
        return super(Task_ChoiceAdmin, self).delete_view(request, object_id=object_id, extra_context=extra_context)
        
admin.site.register(Task_Choice, Task_ChoiceAdmin)

class Task_Task_SetupAdmin(admin.ModelAdmin):
    model_name    = 'task_task_setup'

    list_per_page = 15
    list_display  = ('action_column', 'get_campaign_name', 'task', 'round', 'task_setup')
    list_display_links = None
    list_filter   = [('task__campaign', admin.RelatedOnlyFieldListFilter), 'task__task_status']
    ordering      = ['task', 'round']
    search_fields = ['task__description', 'task__content']
        
    # overwrite has_change_permission to grant view permission
    def has_change_permission(self, request, obj=None):
        # user can change?
        if request.user.has_perm('argo.change_task_task_setup'):
            return True

        # user can view changelist_view?
        if (not obj) and (request.user.has_perm('argo.view_task_task_setup')):
            return True

        return False
        
    # get external field
    def get_campaign_name(self, instance):
        return instance.task.campaign
    get_campaign_name.short_description = 'Campaign'
    get_campaign_name.admin_order_field = 'task__campaign__name'
    
    # remove action "delete" to requester
    def get_actions(self, request):
        actions = super(Task_Task_SetupAdmin, self).get_actions(request)
        if (not request.user.has_perm('argo.delete_task_task_setup')) and ('delete_selected' in actions):
            del actions['delete_selected']
        return actions
        
    # get external field
    def get_campaign_name(self, instance):
        return instance.task.campaign
    get_campaign_name.short_description = 'Campaign'
    get_campaign_name.admin_order_field = 'task__campaign__name'
    
    # define edit link for changeform_view
    def action_column(self, instance):
        change_link = u"<a href='/admin/%s/%s/%s/change' class='action_column_change' title='Change'><i class='fa fa-pencil' aria-hidden='true'></i></a>" % (instance._meta.app_label, self.model_name, instance.id)
        delete_link = u"<a href='/admin/%s/%s/%s/delete' class='action_column_delete' title='Delete'><i class='fa fa-trash' aria-hidden='true'></i></a>" % (instance._meta.app_label, self.model_name, instance.id)
        return change_link + ' ' + delete_link
    action_column.short_description = ''
    action_column.allow_tags = True
    
    # set query of changelist_view (to show different elements to administrator or to requester)
    def get_queryset(self, request):
        qs = super(Task_Task_SetupAdmin, self).get_queryset(request)
        if request.user.groups.filter(name__iexact='requester').exists():
            return qs.filter(task_id__campaign_id__requester=request.user)
        else:
            return qs
            
    # set page title/menu active of changelist (manage model page)
    def changelist_view(self, request, extra_context=None):
        extra_context = extra_context or {}
        extra_context['page_title']   = Task_Task_Setup._meta.verbose_name_plural + ' Manage'
        extra_context['xPanel_title'] = Task_Task_Setup._meta.verbose_name_plural
        extra_context['model_name']   = self.model_name
        extra_context['permission']   = {
                                         'view':   int(request.user.has_perm('argo.view_task_task_setup')),
                                         'change': int(request.user.has_perm('argo.change_task_task_setup')),
                                         'delete': int(request.user.has_perm('argo.delete_task_task_setup'))
                                        }
        extra_context['menu_active']  = 'task_setup'        
        return super(Task_Task_SetupAdmin, self).changelist_view(request, extra_context=extra_context)
    
    # set form (and filter ModelChoiceField)
    def get_form(self, request, obj=None, **kwargs):
        form = Task_Task_SetupAdminForm
        form.requestUser    = request.user
        form.requestIsAdmin = request.user.groups.filter(name__iexact='administrator').exists()
        return form
    
    # set fields of form    
    def get_fieldsets(self, request, obj=None):
        return [
                  (None, {'fields': ['task', 'round', 'task_setup']}),
               ]

    # set page title/menu active of changeform_view (create/update model page)
    def changeform_view(self, request, object_id=None, form_url='', extra_context=None):
        extra_context = extra_context or {}
        extra_context['xPanel_title'] = Task_Task_Setup._meta.verbose_name
        extra_context['model_name']   = self.model_name
        extra_context['menu_active']  = 'task_setup'
        if object_id:
            extra_context['page_title']     = Task_Task_Setup._meta.verbose_name + ' Edit'
            extra_context['readonly_field'] = 'task,round,task_setup'
        else:
            extra_context['page_title']     = Task_Task_Setup._meta.verbose_name + ' Add'
        return super(Task_Task_SetupAdmin, self).changeform_view(request, object_id=object_id, form_url=form_url, extra_context=extra_context)
        
    # set page title/menu active of history_view (history model page)
    def history_view(self, request, object_id, extra_context=None):
        extra_context = extra_context or {}
        extra_context['page_title']   = Task_Task_Setup._meta.verbose_name + ' History'
        extra_context['xPanel_title'] = Task_Task_Setup._meta.verbose_name
        extra_context['model_name']   = self.model_name
        extra_context['menu_active']  = 'task_setup'
        return super(Task_Task_SetupAdmin, self).history_view(request, object_id=object_id, extra_context=extra_context)

    # set page title/menu active of delete_view (delete model page)
    def delete_view(self, request, object_id, extra_context=None):
        extra_context = extra_context or {}
        extra_context['page_title']   = Task_Task_Setup._meta.verbose_name + ' Delete'
        extra_context['xPanel_title'] = Task_Task_Setup._meta.verbose_name
        extra_context['model_name']   = self.model_name
        extra_context['menu_active']  = 'task_setup'
        return super(Task_Task_SetupAdmin, self).delete_view(request, object_id=object_id, extra_context=extra_context)
        
admin.site.register(Task_Task_Setup, Task_Task_SetupAdmin)

class Task_SkillAdmin(admin.ModelAdmin):
    model_name    = 'task_skill'

    list_per_page = 15
    list_display  = ('action_column', 'get_campaign_name', 'task', 'skill')
    list_display_links = None
    list_filter   = [('task__campaign', admin.RelatedOnlyFieldListFilter), 'task__task_status']
    ordering      = ['task']
    search_fields = ['task__description', 'skill__name']

    # overwrite has_change_permission to grant view permission
    def has_change_permission(self, request, obj=None):
        # user can change?
        if request.user.has_perm('argo.change_task_skill'):
            return True

        # user can view changelist_view?
        if (not obj) and (request.user.has_perm('argo.view_task_skill')):
            return True

        return False

    # get external field
    def get_campaign_name(self, instance):
        return instance.task.campaign
    get_campaign_name.short_description = 'Campaign'
    get_campaign_name.admin_order_field = 'task__campaign__name'
    
    # remove action "delete" to requester
    def get_actions(self, request):
        actions = super(Task_SkillAdmin, self).get_actions(request)
        if (not request.user.has_perm('argo.delete_task_skill')) and ('delete_selected' in actions):
            del actions['delete_selected']
        return actions
    
    # define edit link for changeform_view
    def action_column(self, instance):
        change_link = u"<a href='/admin/%s/%s/%s/change' class='action_column_change' title='Change'><i class='fa fa-pencil' aria-hidden='true'></i></a>" % (instance._meta.app_label, self.model_name, instance.id)
        delete_link = u"<a href='/admin/%s/%s/%s/delete' class='action_column_delete' title='Delete'><i class='fa fa-trash' aria-hidden='true'></i></a>" % (instance._meta.app_label, self.model_name, instance.id)
        return change_link + ' ' + delete_link
    action_column.short_description = ''
    action_column.allow_tags = True
    
    # set query of changelist_view (to show different elements to administrator or to requester)
    def get_queryset(self, request):
        qs = super(Task_SkillAdmin, self).get_queryset(request)
        if request.user.groups.filter(name__iexact='requester').exists():
            return qs.filter(task_id__campaign_id__requester=request.user)
        else:
            return qs
            
    # set page title/menu active of changelist (manage model page) 
    def changelist_view(self, request, extra_context=None):
        extra_context = extra_context or {}
        extra_context['page_title']   = Task_Skill._meta.verbose_name_plural + ' Manage'
        extra_context['xPanel_title'] = Task_Skill._meta.verbose_name_plural
        extra_context['model_name']   = self.model_name
        extra_context['permission']   = {
                                         'view':   int(request.user.has_perm('argo.view_task_skill')),
                                         'change': int(request.user.has_perm('argo.change_task_skill')),
                                         'delete': int(request.user.has_perm('argo.delete_task_skill'))
                                        }
        extra_context['menu_active']  = 'task_skill'
        return super(Task_SkillAdmin, self).changelist_view(request, extra_context=extra_context)
    
    # set form (and filter ModelChoiceField)
    def get_form(self, request, obj=None, **kwargs):
        form = Task_SkillAdminForm
        form.requestUser    = request.user
        form.requestIsAdmin = request.user.groups.filter(name__iexact='administrator').exists()
        return form
        
    # set page title/menu active of changeform_view (create/update model page)
    def changeform_view(self, request, object_id=None, form_url='', extra_context=None):
        extra_context = extra_context or {}
        extra_context['xPanel_title'] = Task_Skill._meta.verbose_name
        extra_context['model_name']   = self.model_name
        extra_context['menu_active']  = 'task_skill'
        if object_id:
            extra_context['page_title'] = Task_Skill._meta.verbose_name + ' Edit'
        else:
            extra_context['page_title'] = Task_Skill._meta.verbose_name + ' Add'
        return super(Task_SkillAdmin, self).changeform_view(request, object_id=object_id, form_url=form_url, extra_context=extra_context)
        
    # set page title/menu active of history_view (history model page)
    def history_view(self, request, object_id, extra_context=None):
        extra_context = extra_context or {}
        extra_context['page_title']   = Task_Skill._meta.verbose_name + ' History'
        extra_context['xPanel_title'] = Task_Skill._meta.verbose_name
        extra_context['model_name']   = self.model_name
        extra_context['menu_active']  = 'task_skill'
        return super(Task_SkillAdmin, self).history_view(request, object_id=object_id, extra_context=extra_context)

    # set page title/menu active of delete_view (delete model page)
    def delete_view(self, request, object_id, extra_context=None):
        extra_context = extra_context or {}
        extra_context['page_title']   = Task_Skill._meta.verbose_name + ' Delete'
        extra_context['xPanel_title'] = Task_Skill._meta.verbose_name
        extra_context['model_name']   = self.model_name
        extra_context['menu_active']  = 'task_skill'
        return super(Task_SkillAdmin, self).delete_view(request, object_id=object_id, extra_context=extra_context)
        
admin.site.register(Task_Skill, Task_SkillAdmin)

class Worker_CampaignAdmin(admin.ModelAdmin):
    model_name    = 'worker_campaign'

    list_per_page = 15
    list_display  = ('action_column', 'campaign', 'worker', 'c_score', 'c_trustworthiness', 'worker_banned')
    list_display_links = None
    list_filter   = [('campaign', admin.RelatedOnlyFieldListFilter), 'campaign__campaign_status', 'worker_banned'] 
    search_fields = ['worker__id__username', 'worker__id__first_name', 'worker__id__last_name', 'campaign__name']
        
    # overwrite has_change_permission to grant view permission
    def has_change_permission(self, request, obj=None):
        # user can change?
        if request.user.has_perm('argo.change_worker_campaign'):
            return True

        # user can view changelist_view?
        if (not obj) and (request.user.has_perm('argo.view_worker_campaign')):
            return True

        return False
        
    # remove action "delete" to requester
    def get_actions(self, request):
        actions = super(Worker_CampaignAdmin, self).get_actions(request)
        if (not request.user.has_perm('argo.delete_worker_campaign')) and ('delete_selected' in actions):
            del actions['delete_selected']
        return actions
    
    # define edit link for changeform_view
    def action_column(self, instance):
        change_link = u"<a href='/admin/%s/%s/%s/change' class='action_column_change' title='Change'><i class='fa fa-pencil' aria-hidden='true'></i></a>" % (instance._meta.app_label, self.model_name, instance.id)
        delete_link = u"<a href='/admin/%s/%s/%s/delete' class='action_column_delete' title='Delete'><i class='fa fa-trash' aria-hidden='true'></i></a>" % (instance._meta.app_label, self.model_name, instance.id)
        return change_link + ' ' + delete_link
    action_column.short_description = ''
    action_column.allow_tags = True
    
    # set query of changelist_view (to show different elements to administrator or to requester)
    def get_queryset(self, request):
        qs = super(Worker_CampaignAdmin, self).get_queryset(request)
        if request.user.groups.filter(name__iexact='requester').exists():
            return qs.filter(campaign_id__requester=request.user)
        else:
            return qs
            
    # set page title/menu active of changelist (manage model page)
    def changelist_view(self, request, extra_context=None):
        extra_context = extra_context or {}
        extra_context['page_title']   = Worker_Campaign._meta.verbose_name_plural + ' Manage'
        extra_context['xPanel_title'] = Worker_Campaign._meta.verbose_name_plural
        extra_context['model_name']   = self.model_name
        extra_context['permission']   = {
                                         'view':   int(request.user.has_perm('argo.view_worker_campaign')),
                                         'change': int(request.user.has_perm('argo.change_worker_campaign')),
                                         'delete': int(request.user.has_perm('argo.delete_worker_campaign'))
                                        }
        extra_context['menu_active']  = 'worker_campaign'
        return super(Worker_CampaignAdmin, self).changelist_view(request, extra_context=extra_context)
        
    # set form (and filter ModelChoiceField)
    def get_form(self, request, obj=None, **kwargs):
        form = Worker_CampaignForm
        form.requestUser    = request.user
        form.requestIsAdmin = request.user.groups.filter(name__iexact='administrator').exists()
        return form
            
    # set fields of form    
    def get_fieldsets(self, request, obj=None):
        return [
                  (None,           {'fields': ['worker', 'campaign', 'worker_banned']}),
                  ('Informations', {'fields': ['c_score', 'c_trustworthiness']}),
               ]
               
    # set page title/menu active of changeform_view (create/update model page)
    def changeform_view(self, request, object_id=None, form_url='', extra_context=None):
        extra_context = extra_context or {}
        extra_context['xPanel_title'] = Worker_Campaign._meta.verbose_name
        extra_context['model_name']   = self.model_name
        extra_context['menu_active']  = 'worker_campaign'
        if object_id:
            extra_context['page_title']     = Worker_Campaign._meta.verbose_name + ' Edit'
            extra_context['readonly_field'] = 'worker,campaign'
        else:
            extra_context['page_title']     = Worker_Campaign._meta.verbose_name + ' Add'
        return super(Worker_CampaignAdmin, self).changeform_view(request, object_id=object_id, form_url=form_url, extra_context=extra_context)
        
    # set page title/menu active of history_view (history model page)
    def history_view(self, request, object_id, extra_context=None):
        extra_context = extra_context or {}
        extra_context['page_title']   = Worker_Campaign._meta.verbose_name + ' History'
        extra_context['xPanel_title'] = Worker_Campaign._meta.verbose_name
        extra_context['model_name']   = self.model_name
        extra_context['menu_active']  = 'worker_campaign'
        return super(Worker_CampaignAdmin, self).history_view(request, object_id=object_id, extra_context=extra_context)

    # set page title/menu active of delete_view (delete model page)
    def delete_view(self, request, object_id, extra_context=None):
        extra_context = extra_context or {}
        extra_context['page_title']   = Worker_Campaign._meta.verbose_name + ' Delete'
        extra_context['xPanel_title'] = Worker_Campaign._meta.verbose_name
        extra_context['model_name']   = self.model_name
        extra_context['menu_active']  = 'worker_campaign'
        return super(Worker_CampaignAdmin, self).delete_view(request, object_id=object_id, extra_context=extra_context)
        
admin.site.register(Worker_Campaign, Worker_CampaignAdmin)

class Worker_TaskAdmin(admin.ModelAdmin):
    model_name    = 'worker_task'

    list_per_page = 15
    list_display  = ('action_column', 'get_campaign_name', 'task', 'worker', 'round', 'current_w_trustworthiness')
    list_display_links = None
    list_filter   = [('task__campaign', admin.RelatedOnlyFieldListFilter), 'task__task_status']
    ordering      = ['task__campaign__name', 'task', 'worker']
    search_fields = ['worker__id__username', 'worker__id__first_name', 'worker__id__last_name', 'task__description', 'task__content']

    # overwrite has_add_permission to prevent insert
    def has_add_permission(self, request):
        return False

    # overwrite has_change_permission to grant view permission
    def has_change_permission(self, request, obj=None):
        # user can change?
        if request.user.has_perm('argo.change_worker_task'):
            return True

        # user can view changelist_view?
        if (not obj) and (request.user.has_perm('argo.view_worker_task')):
            return True

        return False
        
    # overwrite get_model_perms to prevent insert
    def get_model_perms(self, request): 
        return {'view': True}
    
    # remove action "delete" to requester
    def get_actions(self, request):
        actions = super(Worker_TaskAdmin, self).get_actions(request)
        if (not request.user.has_perm('argo.delete_worker_task')) and ('delete_selected' in actions):
            del actions['delete_selected']
        return actions

    # get external field
    def get_campaign_name(self, instance):
        return instance.task.campaign
    get_campaign_name.short_description = 'Campaign'
    get_campaign_name.admin_order_field = 'task__campaign__name'
    
    # define edit link for changeform_view
    def action_column(self, instance):
        change_link = u"<a href='/admin/%s/%s/%s/change' class='action_column_change' title='Change'><i class='fa fa-pencil' aria-hidden='true'></i></a>" % (instance._meta.app_label, self.model_name, instance.id)
        delete_link = u"<a href='/admin/%s/%s/%s/delete' class='action_column_delete' title='Delete'><i class='fa fa-trash' aria-hidden='true'></i></a>" % (instance._meta.app_label, self.model_name, instance.id)
        return change_link + ' ' + delete_link
    action_column.short_description = ''
    action_column.allow_tags = True
    
    # set query of changelist_view (to show different elements to administrator or to requester)
    def get_queryset(self, request):
        qs = super(Worker_TaskAdmin, self).get_queryset(request)
        if request.user.groups.filter(name__iexact='requester').exists():
            return qs.filter(task_id__campaign_id__requester=request.user)
        else:
            return qs
            
    # set page title/menu active of changelist (manage model page)
    def changelist_view(self, request, extra_context=None):
        extra_context = extra_context or {}
        extra_context['page_title']   = Worker_Task._meta.verbose_name_plural + ' Manage'
        extra_context['xPanel_title'] = Worker_Task._meta.verbose_name_plural
        extra_context['model_name']   = self.model_name
        extra_context['permission']   = {
                                         'view':   int(request.user.has_perm('argo.view_worker_task')),
                                         'change': int(request.user.has_perm('argo.change_worker_task')),
                                         'delete': int(request.user.has_perm('argo.delete_worker_task'))
                                        }
        extra_context['menu_active']  = 'worker_task'
        return super(Worker_TaskAdmin, self).changelist_view(request, extra_context=extra_context)
    
    # set form (and filter ModelChoiceField)
    def get_form(self, request, obj=None, **kwargs):
        form = Worker_TaskForm
        form.requestUser    = request.user
        form.requestIsAdmin = request.user.groups.filter(name__iexact='administrator').exists()
        return form

    # set fields of form    
    def get_fieldsets(self, request, obj=None):
        return [
                  (None,           {'fields': ['worker', 'task', 'round', 'answer']}),
                  ('Informations', {'fields': ['current_w_trustworthiness', 'timeout']}),
               ]

    # set page title/menu active of changeform_view (create/update model page)
    def changeform_view(self, request, object_id=None, form_url='', extra_context=None):
        extra_context = extra_context or {}
        extra_context['xPanel_title'] = Worker_Task._meta.verbose_name
        extra_context['model_name']   = self.model_name
        extra_context['menu_active']  = 'worker_task'
        if object_id:
            extra_context['page_title']     = Worker_Task._meta.verbose_name + ' Edit'
            extra_context['readonly_field'] = 'worker,task,round,answer,current_w_trustworthiness,timestamp_out,timeout'
            extra_context['show_goback_link']       = True
            extra_context['show_save']              = False
            extra_context['show_save_and_continue'] = False
        else:
            extra_context['page_title']     = Worker_Task._meta.verbose_name + ' Add'
        return super(Worker_TaskAdmin, self).changeform_view(request, object_id=object_id, form_url=form_url, extra_context=extra_context)
        
    # set page title/menu active of history_view (history model page)
    def history_view(self, request, object_id, extra_context=None):
        extra_context = extra_context or {}
        extra_context['page_title']   = Worker_Task._meta.verbose_name + ' History'
        extra_context['xPanel_title'] = Worker_Task._meta.verbose_name
        extra_context['model_name']   = self.model_name
        extra_context['menu_active']  = 'worker_task'
        return super(Worker_TaskAdmin, self).history_view(request, object_id=object_id, extra_context=extra_context)

    # set page title/menu active of delete_view (delete model page)
    def delete_view(self, request, object_id, extra_context=None):
        extra_context = extra_context or {}
        extra_context['page_title']   = Worker_Task._meta.verbose_name + ' Delete'
        extra_context['xPanel_title'] = Worker_Task._meta.verbose_name
        extra_context['model_name']   = self.model_name
        extra_context['menu_active']  = 'worker_task'        
        return super(Worker_TaskAdmin, self).delete_view(request, object_id=object_id, extra_context=extra_context)
        
admin.site.register(Worker_Task, Worker_TaskAdmin)

class WorkerAdmin(admin.ModelAdmin):
    model_name    = 'worker'

    list_per_page = 15
    list_display  = ('action_column', '__unicode__', 'w_trustworthiness', 'w_score', 'banned')
    list_display_links = None
    list_filter   = ['banned']
    search_fields = ['id__username', 'id__first_name', 'id__last_name']

    # overwrite has_change_permission to grant view permission
    def has_change_permission(self, request, obj=None):
        # user can change?
        if request.user.has_perm('argo.change_worker'):
            return True

        # user can view changelist_view?
        if (not obj) and (request.user.has_perm('argo.view_worker')):
            return True

        return False
        
    # remove action "delete" to requester
    def get_actions(self, request):
        actions = super(WorkerAdmin, self).get_actions(request)
        if (not request.user.has_perm('argo.delete_worker')) and ('delete_selected' in actions):
            del actions['delete_selected']
        return actions
    
    # define edit link for changeform_view
    def action_column(self, instance):
        change_link = u"<a href='/admin/%s/%s/%s/change' class='action_column_change' title='Change'><i class='fa fa-pencil' aria-hidden='true'></i></a>" % (instance._meta.app_label, self.model_name, instance.id_id)
        delete_link = u"<a href='/admin/%s/%s/%s/delete' class='action_column_delete' title='Delete'><i class='fa fa-trash' aria-hidden='true'></i></a>" % (instance._meta.app_label, self.model_name, instance.id_id)
        return change_link + ' ' + delete_link
    action_column.short_description = ''
    action_column.allow_tags = True
    
    # set page title/menu active of changelist (manage model page)
    def changelist_view(self, request, extra_context=None):
        extra_context = extra_context or {}
        extra_context['page_title']   = Worker._meta.verbose_name_plural + ' Manage'
        extra_context['xPanel_title'] = Worker._meta.verbose_name_plural
        extra_context['model_name']   = self.model_name
        extra_context['permission']   = {
                                         'view':   int(request.user.has_perm('argo.view_worker')),
                                         'change': int(request.user.has_perm('argo.change_worker')),
                                         'delete': int(request.user.has_perm('argo.delete_worker'))
                                        }
        extra_context['menu_active']  = 'worker'
        return super(WorkerAdmin, self).changelist_view(request, extra_context=extra_context)
    
    # set form
    def get_form(self, request, obj=None, **kwargs):
        if obj:
            return WorkerAdminUpdateForm
        else:
            return WorkerAdminCreateForm
            
    # set fields of form    
    def get_fieldsets(self, request, obj=None):
        return [
                  (None,           {'fields': ['id', 'birth_date', 'genre', 'banned']}),
                  ('Informations', {'fields': ['w_score', 'w_trustworthiness']}),
               ]
               
    # set page title/menu active of changeform_view (create/update model page)
    def changeform_view(self, request, object_id=None, form_url='', extra_context=None):
        extra_context = extra_context or {}
        extra_context['xPanel_title'] = Worker._meta.verbose_name
        extra_context['model_name']   = self.model_name
        if object_id:
            extra_context['page_title']     = Worker._meta.verbose_name + ' Edit'
            extra_context['menu_active']    = 'worker'
            extra_context['readonly_field'] = 'id'
        else:
            extra_context['page_title']     = Worker._meta.verbose_name + ' Add'
            extra_context['menu_active']    = 'worker_add'
        return super(WorkerAdmin, self).changeform_view(request, object_id=object_id, form_url=form_url, extra_context=extra_context)
        
    # set page title/menu active of history_view (history model page)
    def history_view(self, request, object_id, extra_context=None):
        extra_context = extra_context or {}
        extra_context['page_title']   = Worker._meta.verbose_name + ' History'
        extra_context['xPanel_title'] = Worker._meta.verbose_name
        extra_context['model_name']   = self.model_name
        extra_context['menu_active']  = 'worker'
        return super(WorkerAdmin, self).history_view(request, object_id=object_id, extra_context=extra_context)

    # set page title/menu active of delete_view (delete model page)      
    def delete_view(self, request, object_id, extra_context=None):
        extra_context = extra_context or {}
        extra_context['page_title']   = Worker._meta.verbose_name + ' Delete'
        extra_context['xPanel_title'] = Worker._meta.verbose_name
        extra_context['model_name']   = self.model_name
        extra_context['menu_active']  = 'worker'
        return super(WorkerAdmin, self).delete_view(request, object_id=object_id, extra_context=extra_context)
        
admin.site.register(Worker, WorkerAdmin)

class Campaign_StatusAdmin(admin.ModelAdmin):
    model_name    = 'campaign_status'

    list_per_page = 15
    list_display  = ('action_column', 'name',)
    list_display_links = None
    list_filter   = []
    ordering      = ['name']
    search_fields = ['name']

    # overwrite has_change_permission to grant view permission
    def has_change_permission(self, request, obj=None):
        # user can change?
        if request.user.has_perm('argo.change_campaign_status'):
            return True

        # user can view changelist_view?
        if (not obj) and (request.user.has_perm('argo.view_campaign_status')):
            return True

        return False

    # non-modifiable or removable elements
    def get_protected_elements(self):
        protected_elements = ['draft', 'terminated', 'under execution']
        ret = []
        for elm in protected_elements:
            obj = Campaign_Status.objects.filter(name__iexact=elm)[0]
            ret.append(obj.id)
        return ret

    # overwrite has_delete_permission to protect non-removable elements
    def has_delete_permission(self, request, obj=None):
        protected_elements = self.get_protected_elements()
        
        # delete in changeform_view
        if obj!=None:
            for elm in protected_elements:
                if elm == getattr(obj, 'id'):
                    return False

        # delete in changelist_view 
        if request.POST and request.POST.get('action') == 'delete_selected':
            for elm in protected_elements:
                if str(elm) in request.POST.getlist('_selected_action'):
                    return False

        # user can delete?
        if request.user.has_perm('argo.delete_campaign_status'):
            return True
            
        return False
        
    # remove action "delete" to requester
    def get_actions(self, request):
        actions = super(Campaign_StatusAdmin, self).get_actions(request)
        if (not request.user.has_perm('argo.delete_campaign_status')) and ('delete_selected' in actions):
            del actions['delete_selected']
        return actions
    
    # define edit link for changeform_view
    def action_column(self, instance):
        change_link = u"<a href='/admin/%s/%s/%s/change' class='action_column_change' title='Change'><i class='fa fa-pencil' aria-hidden='true'></i></a>" % (instance._meta.app_label, self.model_name, instance.id)
        delete_link = u"<a href='/admin/%s/%s/%s/delete' class='action_column_delete' title='Delete'><i class='fa fa-trash' aria-hidden='true'></i></a>" % (instance._meta.app_label, self.model_name, instance.id)
        return change_link + ' ' + delete_link
    action_column.short_description = ''
    action_column.allow_tags = True
    
    # set page title/menu active of changelist (manage model page)
    def changelist_view(self, request, extra_context=None):
        extra_context = extra_context or {}
        extra_context['page_title']   = Campaign_Status._meta.verbose_name_plural + ' Manage'
        extra_context['xPanel_title'] = Campaign_Status._meta.verbose_name_plural
        extra_context['model_name']   = self.model_name
        extra_context['permission']   = {
                                         'view':   int(request.user.has_perm('argo.view_campaign_status')),
                                         'change': int(request.user.has_perm('argo.change_campaign_status')),
                                         'delete': int(request.user.has_perm('argo.delete_campaign_status'))
                                        }
        extra_context['menu_active']  = 'campaign_status'
        return super(Campaign_StatusAdmin, self).changelist_view(request, extra_context=extra_context)
        
    # set form
    def get_form(self, request, obj=None, **kwargs):
        return Campaign_StatusAdminForm
        
    # set page title/menu active of changeform_view (create/update model page)
    def changeform_view(self, request, object_id=None, form_url='', extra_context=None):
        extra_context = extra_context or {}
        extra_context['xPanel_title'] = Campaign_Status._meta.verbose_name
        extra_context['model_name']   = self.model_name
        extra_context['menu_active']  = 'campaign_status'
        if object_id:
            extra_context['page_title'] = Campaign_Status._meta.verbose_name + ' Edit'
        else:
            extra_context['page_title'] = Campaign_Status._meta.verbose_name + ' Add'
            
        # check for a non-modifiable elements
        protected_elements = self.get_protected_elements()
        if (object_id) and (int(object_id) in protected_elements):
            extra_context['protected_element'] = True
            extra_context['readonly_field']    = 'name'
            
            extra_context['show_goback_link']       = True
            extra_context['show_save']              = False
            extra_context['show_save_and_continue'] = False
        return super(Campaign_StatusAdmin, self).changeform_view(request, object_id=object_id, form_url=form_url, extra_context=extra_context)
        
    # set page title/menu active of history_view (history model page)
    def history_view(self, request, object_id, extra_context=None):
        extra_context = extra_context or {}
        extra_context['page_title']   = Campaign_Status._meta.verbose_name + ' History'        
        extra_context['xPanel_title'] = Campaign_Status._meta.verbose_name
        extra_context['model_name']   = self.model_name
        extra_context['menu_active']  = 'campaign_status'
        return super(Campaign_StatusAdmin, self).history_view(request, object_id=object_id, extra_context=extra_context)

    # set page title/menu active of delete_view (delete model page)
    def delete_view(self, request, object_id, extra_context=None):
        extra_context = extra_context or {}
        extra_context['page_title']   = Campaign_Status._meta.verbose_name + ' Delete'
        extra_context['xPanel_title'] = Campaign_Status._meta.verbose_name
        extra_context['model_name']   = self.model_name
        extra_context['menu_active']  = 'campaign_status'
        return super(Campaign_StatusAdmin, self).delete_view(request, object_id=object_id, extra_context=extra_context)
        
admin.site.register(Campaign_Status, Campaign_StatusAdmin)

class Task_StatusAdmin(admin.ModelAdmin):
    model_name    = 'task_status'

    list_per_page = 15
    list_display  = ('action_column', 'name',)
    list_display_links = None
    list_filter   = []
    ordering      = ['name']
    search_fields = ['name']

    # overwrite has_change_permission to grant view permission
    def has_change_permission(self, request, obj=None):
        # user can change?
        if request.user.has_perm('argo.change_task_status'):
            return True

        # user can view changelist_view?
        if (not obj) and (request.user.has_perm('argo.view_task_status')):
            return True

        return False

    # non-modifiable or removable elements
    def get_protected_elements(self):
        protected_elements = ['ready to execute', 'terminated successfully', 'terminated unsuccessfully', 'under execution']
        ret = []
        for elm in protected_elements:
            obj = Task_Status.objects.filter(name__iexact=elm)[0]
            ret.append(obj.id)
        return ret

    # overwrite has_delete_permission to protect non-removable elements
    def has_delete_permission(self, request, obj=None):
        protected_elements = self.get_protected_elements()
        
        # delete in changeform_view
        if obj!=None:
            for elm in protected_elements:
                if elm == getattr(obj, 'id'):
                    return False

        # delete in changelist_view 
        if request.POST and request.POST.get('action') == 'delete_selected':
            for elm in protected_elements:
                if str(elm) in request.POST.getlist('_selected_action'):
                    return False

        # user can delete?
        if request.user.has_perm('argo.delete_task_status'):
            return True
            
        return False
        
    # remove action "delete" to requester
    def get_actions(self, request):
        actions = super(Task_StatusAdmin, self).get_actions(request)
        if (not request.user.has_perm('argo.delete_task_status')) and ('delete_selected' in actions):
            del actions['delete_selected']
        return actions
    
    # define edit link for changeform_view
    def action_column(self, instance):
        change_link = u"<a href='/admin/%s/%s/%s/change' class='action_column_change' title='Change'><i class='fa fa-pencil' aria-hidden='true'></i></a>" % (instance._meta.app_label, self.model_name, instance.id)
        delete_link = u"<a href='/admin/%s/%s/%s/delete' class='action_column_delete' title='Delete'><i class='fa fa-trash' aria-hidden='true'></i></a>" % (instance._meta.app_label, self.model_name, instance.id)
        return change_link + ' ' + delete_link
    action_column.short_description = ''
    action_column.allow_tags = True
    
    # set page title/menu active of changelist (manage model page)
    def changelist_view(self, request, extra_context=None):
        extra_context = extra_context or {}
        extra_context['page_title']   = Task_Status._meta.verbose_name_plural + ' Manage'
        extra_context['xPanel_title'] = Task_Status._meta.verbose_name_plural
        extra_context['model_name']   = self.model_name
        extra_context['permission']   = {
                                         'view':   int(request.user.has_perm('argo.view_task_status')),
                                         'change': int(request.user.has_perm('argo.change_task_status')),
                                         'delete': int(request.user.has_perm('argo.delete_task_status'))
                                        }
        extra_context['menu_active']  = 'task_status'
        return super(Task_StatusAdmin, self).changelist_view(request, extra_context=extra_context)
        
    # set form
    def get_form(self, request, obj=None, **kwargs):
        return Task_StatusAdminForm
        
    # set page title/menu active of changeform_view (create/update model page)
    def changeform_view(self, request, object_id=None, form_url='', extra_context=None):
        extra_context = extra_context or {}
        extra_context['xPanel_title'] = Task_Status._meta.verbose_name
        extra_context['model_name']   = self.model_name
        extra_context['menu_active']  = 'task_status'
        if object_id:
            extra_context['page_title'] = Task_Status._meta.verbose_name + ' Edit'
        else:
            extra_context['page_title'] = Task_Status._meta.verbose_name + ' Add'
            
        # check for a non-modifiable elements
        protected_elements = self.get_protected_elements()
        if (object_id) and (int(object_id) in protected_elements):
            extra_context['protected_element'] = True
            extra_context['readonly_field']    = 'name'
            
            extra_context['show_goback_link']       = True
            extra_context['show_save']              = False
            extra_context['show_save_and_continue'] = False
        return super(Task_StatusAdmin, self).changeform_view(request, object_id=object_id, form_url=form_url, extra_context=extra_context)
        
    # set page title/menu active of history_view (history model page)
    def history_view(self, request, object_id, extra_context=None):
        extra_context = extra_context or {}
        extra_context['page_title']   = Task_Status._meta.verbose_name + ' History'
        extra_context['xPanel_title'] = Task_Status._meta.verbose_name
        extra_context['model_name']   = self.model_name
        extra_context['menu_active']  = 'task_status'
        return super(Task_StatusAdmin, self).history_view(request, object_id=object_id, extra_context=extra_context)

    # set page title/menu active of delete_view (delete model page)
    def delete_view(self, request, object_id, extra_context=None):
        extra_context = extra_context or {}
        extra_context['page_title']   = Task_Status._meta.verbose_name + ' Delete'
        extra_context['xPanel_title'] = Task_Status._meta.verbose_name
        extra_context['model_name']   = self.model_name
        extra_context['menu_active']  = 'task_status'
        return super(Task_StatusAdmin, self).delete_view(request, object_id=object_id, extra_context=extra_context)
        
admin.site.register(Task_Status, Task_StatusAdmin)

class Task_SetupAdmin(admin.ModelAdmin):
    model_name    = 'task_setup'

    list_per_page = 15
    list_display  = ('action_column', 'num_worker', 'min_trustworthiness', 'consensus_threshold', 'max_exec_time')
    list_display_links = None
    list_filter   = [] 
    search_fields = []

    # overwrite has_change_permission to grant view permission
    def has_change_permission(self, request, obj=None):
        # user can change?
        if request.user.has_perm('argo.change_task_setup'):
            return True

        # user can view changelist_view?
        if (not obj) and (request.user.has_perm('argo.view_task_setup')):
            return True

        return False

    # remove action "delete" to requester
    def get_actions(self, request):
        actions = super(Task_SetupAdmin, self).get_actions(request)
        if (not request.user.has_perm('argo.delete_task_setup')) and ('delete_selected' in actions):
            del actions['delete_selected']
        return actions
    
    # define edit link for changeform_view
    def action_column(self, instance):
        change_link = u"<a href='/admin/%s/%s/%s/change' class='action_column_change' title='Change'><i class='fa fa-pencil' aria-hidden='true'></i></a>" % (instance._meta.app_label, self.model_name, instance.id)
        delete_link = u"<a href='/admin/%s/%s/%s/delete' class='action_column_delete' title='Delete'><i class='fa fa-trash' aria-hidden='true'></i></a>" % (instance._meta.app_label, self.model_name, instance.id)
        return change_link + ' ' + delete_link
    action_column.short_description = ''
    action_column.allow_tags = True
    
   # set page title/menu active of changelist (manage model page)
    def changelist_view(self, request, extra_context=None):
        extra_context = extra_context or {}
        extra_context['page_title']   = Task_Setup._meta.verbose_name_plural + ' Manage'
        extra_context['xPanel_title'] = Task_Setup._meta.verbose_name_plural
        extra_context['model_name']   = self.model_name
        extra_context['permission']   = {
                                         'view':   int(request.user.has_perm('argo.view_task_setup')),
                                         'change': int(request.user.has_perm('argo.change_task_setup')),
                                         'delete': int(request.user.has_perm('argo.delete_task_setup'))
                                        }
        extra_context['menu_active']  = 'setup'
        return super(Task_SetupAdmin, self).changelist_view(request, extra_context=extra_context)
        
    # set form
    def get_form(self, request, obj=None, **kwargs):
        return Task_SetupAdminForm
        
    # set page title/menu active of changeform_view (create/update model page)
    def changeform_view(self, request, object_id=None, form_url='', extra_context=None):
        extra_context = extra_context or {}
        extra_context['xPanel_title'] = Task_Setup._meta.verbose_name
        extra_context['model_name']   = self.model_name
        extra_context['menu_active']  = 'setup'
        if object_id:
            extra_context['page_title'] = Task_Setup._meta.verbose_name + ' Edit'
        else:
            extra_context['page_title'] = Task_Setup._meta.verbose_name + ' Add'
        return super(Task_SetupAdmin, self).changeform_view(request, object_id=object_id, form_url=form_url, extra_context=extra_context)
        
    # set page title/menu active of history_view (history model page)
    def history_view(self, request, object_id, extra_context=None):
        extra_context = extra_context or {}
        extra_context['page_title']   = Task_Setup._meta.verbose_name + ' History'
        extra_context['xPanel_title'] = Task_Setup._meta.verbose_name
        extra_context['model_name']   = self.model_name
        extra_context['menu_active']  = 'setup'
        return super(Task_SetupAdmin, self).history_view(request, object_id=object_id, extra_context=extra_context)

    # set page title/menu active of delete_view (delete model page)
    def delete_view(self, request, object_id, extra_context=None):
        extra_context = extra_context or {}
        extra_context['page_title']   = Task_Setup._meta.verbose_name + ' Delete'
        extra_context['xPanel_title'] = Task_Setup._meta.verbose_name
        extra_context['model_name']   = self.model_name
        extra_context['menu_active']  = 'setup'
        return super(Task_SetupAdmin, self).delete_view(request, object_id=object_id, extra_context=extra_context)
        
admin.site.register(Task_Setup, Task_SetupAdmin)

class PatternAdmin(admin.ModelAdmin):
    model_name    = 'pattern'

    list_per_page = 15
    list_display  = ('action_column', 'description',)
    list_display_links = None
    list_filter   = [] 
    ordering      = ['description']
    search_fields = ['description']

    # overwrite has_change_permission to grant view permission
    def has_change_permission(self, request, obj=None):
        # user can change?
        if request.user.has_perm('argo.change_pattern'):
            return True

        # user can view changelist_view?
        if (not obj) and (request.user.has_perm('argo.view_pattern')):
            return True

        return False

    # remove action "delete" to requester
    def get_actions(self, request):
        actions = super(PatternAdmin, self).get_actions(request)
        if (not request.user.has_perm('argo.delete_pattern')) and ('delete_selected' in actions):
            del actions['delete_selected']
        return actions
    
    # define edit link for changeform_view
    def action_column(self, instance):
        change_link = u"<a href='/admin/%s/%s/%s/change' class='action_column_change' title='Change'><i class='fa fa-pencil' aria-hidden='true'></i></a>" % (instance._meta.app_label, self.model_name, instance.id)
        delete_link = u"<a href='/admin/%s/%s/%s/delete' class='action_column_delete' title='Delete'><i class='fa fa-trash' aria-hidden='true'></i></a>" % (instance._meta.app_label, self.model_name, instance.id)
        return change_link + ' ' + delete_link
    action_column.short_description = ''
    action_column.allow_tags = True
    
   # set page title/menu active of changelist (manage model page)
    def changelist_view(self, request, extra_context=None):
        extra_context = extra_context or {}
        extra_context['page_title']   = Pattern._meta.verbose_name_plural + ' Manage'
        extra_context['xPanel_title'] = Pattern._meta.verbose_name_plural
        extra_context['model_name']   = self.model_name
        extra_context['permission']   = {
                                         'view':   int(request.user.has_perm('argo.view_pattern')),
                                         'change': int(request.user.has_perm('argo.change_pattern')),
                                         'delete': int(request.user.has_perm('argo.delete_pattern'))
                                        }
        extra_context['menu_active']  = 'pattern'
        return super(PatternAdmin, self).changelist_view(request, extra_context=extra_context)
        
    # set form
    def get_form(self, request, obj=None, **kwargs):
        return PatternAdminForm
        
    # set page title/menu active of changeform_view (create/update model page)
    def changeform_view(self, request, object_id=None, form_url='', extra_context=None):
        extra_context = extra_context or {}
        extra_context['xPanel_title'] = Pattern._meta.verbose_name
        extra_context['model_name']   = self.model_name
        extra_context['menu_active']  = 'pattern'
        if object_id:
            extra_context['page_title'] = Pattern._meta.verbose_name + ' Edit'
        else:
            extra_context['page_title'] = Pattern._meta.verbose_name + ' Add'
        return super(PatternAdmin, self).changeform_view(request, object_id=object_id, form_url=form_url, extra_context=extra_context)
        
    # set page title/menu active of history_view (history model page)
    def history_view(self, request, object_id, extra_context=None):
        extra_context = extra_context or {}
        extra_context['page_title']   = Pattern._meta.verbose_name + ' History'
        extra_context['xPanel_title'] = Pattern._meta.verbose_name
        extra_context['model_name']   = self.model_name
        extra_context['menu_active']  = 'pattern'
        return super(PatternAdmin, self).history_view(request, object_id=object_id, extra_context=extra_context)

    # set page title/menu active of delete_view (delete model page)
    def delete_view(self, request, object_id, extra_context=None):
        extra_context = extra_context or {}
        extra_context['page_title']   = Pattern._meta.verbose_name + ' Delete'
        extra_context['xPanel_title'] = Pattern._meta.verbose_name
        extra_context['model_name']   = self.model_name
        extra_context['menu_active']  = 'pattern'
        return super(PatternAdmin, self).delete_view(request, object_id=object_id, extra_context=extra_context)
        
admin.site.register(Pattern, PatternAdmin)

class TaskSetupPatternAdmin(admin.ModelAdmin):
    model_name    = 'task_setup_pattern'

    list_per_page = 15
    list_display  = ('action_column', 'pattern', 'round', 'task_setup')
    list_display_links = None
    list_filter   = []
    ordering      = ['pattern', 'round']
    search_fields = ['pattern__description']

    # overwrite has_change_permission to grant view permission
    def has_change_permission(self, request, obj=None):
        # user can change?
        if request.user.has_perm('argo.change_task_setup_pattern'):
            return True

        # user can view changelist_view?
        if (not obj) and (request.user.has_perm('argo.view_task_setup_pattern')):
            return True

        return False

    # remove action "delete" to requester
    def get_actions(self, request):
        actions = super(TaskSetupPatternAdmin, self).get_actions(request)
        if (not request.user.has_perm('argo.delete_task_setup_pattern')) and ('delete_selected' in actions):
            del actions['delete_selected']
        return actions
    
    # define edit link for changeform_view
    def action_column(self, instance):
        change_link = u"<a href='/admin/%s/%s/%s/change' class='action_column_change' title='Change'><i class='fa fa-pencil' aria-hidden='true'></i></a>" % (instance._meta.app_label, self.model_name, instance.id)
        delete_link = u"<a href='/admin/%s/%s/%s/delete' class='action_column_delete' title='Delete'><i class='fa fa-trash' aria-hidden='true'></i></a>" % (instance._meta.app_label, self.model_name, instance.id)
        return change_link + ' ' + delete_link
    action_column.short_description = ''
    action_column.allow_tags = True
    
    # set page title/menu active of changelist (manage model page)
    def changelist_view(self, request, extra_context=None):
        extra_context = extra_context or {}
        extra_context['page_title']   = Task_Setup_Pattern._meta.verbose_name_plural + ' Manage'
        extra_context['xPanel_title'] = Task_Setup_Pattern._meta.verbose_name_plural
        extra_context['model_name']   = self.model_name
        extra_context['permission']   = {
                                         'view':   int(request.user.has_perm('argo.view_task_setup_pattern')),
                                         'change': int(request.user.has_perm('argo.change_task_setup_pattern')),
                                         'delete': int(request.user.has_perm('argo.delete_task_setup_pattern'))
                                        }
        extra_context['menu_active']  = 'pattern_round_setup'
        return super(TaskSetupPatternAdmin, self).changelist_view(request, extra_context=extra_context)
        
    # set form
    def get_form(self, request, obj=None, **kwargs):
        return Task_Setup_PatternAdminForm
        
    # set page title/menu active of changeform_view (create/update model page)
    def changeform_view(self, request, object_id=None, form_url='', extra_context=None):
        extra_context = extra_context or {}
        extra_context['xPanel_title'] = Task_Setup_Pattern._meta.verbose_name
        extra_context['model_name']   = self.model_name
        extra_context['menu_active']  = 'pattern_round_setup'
        if object_id:
            extra_context['page_title'] = Task_Setup_Pattern._meta.verbose_name + ' Edit'
        else:
            extra_context['page_title'] = Task_Setup_Pattern._meta.verbose_name + ' Add'
        return super(TaskSetupPatternAdmin, self).changeform_view(request, object_id=object_id, form_url=form_url, extra_context=extra_context)
        
    # set page title/menu active of history_view (history model page)
    def history_view(self, request, object_id, extra_context=None):
        extra_context = extra_context or {}
        extra_context['page_title']   = Task_Setup_Pattern._meta.verbose_name + ' History'
        extra_context['xPanel_title'] = Task_Setup_Pattern._meta.verbose_name
        extra_context['model_name']   = self.model_name
        extra_context['menu_active']  = 'pattern_round_setup'
        return super(TaskSetupPatternAdmin, self).history_view(request, object_id=object_id, extra_context=extra_context)

    # set page title/menu active of delete_view (delete model page)
    def delete_view(self, request, object_id, extra_context=None):
        extra_context = extra_context or {}
        extra_context['page_title']   = Task_Setup_Pattern._meta.verbose_name + ' Delete'
        extra_context['xPanel_title'] = Task_Setup_Pattern._meta.verbose_name
        extra_context['model_name']   = self.model_name
        extra_context['menu_active']  = 'pattern_round_setup'
        return super(TaskSetupPatternAdmin, self).delete_view(request, object_id=object_id, extra_context=extra_context)
        
admin.site.register(Task_Setup_Pattern, TaskSetupPatternAdmin)

class Answer_TypeAdmin(admin.ModelAdmin):
    model_name    = 'answer_type'
    
    list_per_page = 15
    list_display  = ('action_column', 'name',)
    list_display_links = None
    list_filter   = [] 
    ordering      = ['name']
    search_fields = ['name']

    # overwrite has_change_permission to grant view permission
    def has_change_permission(self, request, obj=None):
        # user can change?
        if request.user.has_perm('argo.change_answer_type'):
            return True

        # user can view changelist_view?
        if (not obj) and (request.user.has_perm('argo.view_answer_type')):
            return True

        return False

    # non-modifiable or removable elements
    def get_protected_elements(self):
        protected_elements = ['choice', 'text']
        ret = []
        for elm in protected_elements:
            obj = Answer_Type.objects.filter(name__iexact=elm)[0]
            ret.append(obj.id)
        return ret

    # overwrite has_delete_permission to protect non-removable elements
    def has_delete_permission(self, request, obj=None):
        protected_elements = self.get_protected_elements()
        
        # delete in changeform_view
        if obj!=None:
            for elm in protected_elements:
                if elm == getattr(obj, 'id'):
                    return False

        # delete in changelist_view 
        if request.POST and request.POST.get('action') == 'delete_selected':
            for elm in protected_elements:
                if str(elm) in request.POST.getlist('_selected_action'):
                    return False

        # user can delete?
        if request.user.has_perm('argo.delete_answer_type'):
            return True
            
        return False

    # remove action "delete" to requester
    def get_actions(self, request):
        actions = super(Answer_TypeAdmin, self).get_actions(request)
        if (not request.user.has_perm('argo.delete_answer_type')) and ('delete_selected' in actions):
            del actions['delete_selected']
        return actions
    
    # define edit link for changeform_view
    def action_column(self, instance):
        change_link = u"<a href='/admin/%s/%s/%s/change' class='action_column_change' title='Change'><i class='fa fa-pencil' aria-hidden='true'></i></a>" % (instance._meta.app_label, self.model_name, instance.id)
        delete_link = u"<a href='/admin/%s/%s/%s/delete' class='action_column_delete' title='Delete'><i class='fa fa-trash' aria-hidden='true'></i></a>" % (instance._meta.app_label, self.model_name, instance.id)
        return change_link + ' ' + delete_link
    action_column.short_description = ''
    action_column.allow_tags = True
    
    # set page title/menu active of changelist (manage model page) 
    def changelist_view(self, request, extra_context=None):
        extra_context = extra_context or {}
        extra_context['page_title']   = Answer_Type._meta.verbose_name_plural + ' Manage'
        extra_context['xPanel_title'] = Answer_Type._meta.verbose_name_plural
        extra_context['model_name']   = self.model_name
        extra_context['permission']   = {
                                         'view':   int(request.user.has_perm('argo.view_answer_type')),
                                         'change': int(request.user.has_perm('argo.change_answer_type')),
                                         'delete': int(request.user.has_perm('argo.delete_answer_type'))
                                        }
        extra_context['menu_active']  = 'answer_types'
        return super(Answer_TypeAdmin, self).changelist_view(request, extra_context=extra_context)

    # set form
    def get_form(self, request, obj=None, **kwargs):
        return Answer_TypeAdminForm
        
    # set page title/menu active of changeform_view (create/update model page)
    def changeform_view(self, request, object_id=None, form_url='', extra_context=None):
        extra_context = extra_context or {}
        extra_context['xPanel_title'] = Answer_Type._meta.verbose_name
        extra_context['model_name']   = self.model_name
        extra_context['menu_active']  = 'answer_types'
        if object_id:
            extra_context['page_title'] = Answer_Type._meta.verbose_name + ' Edit'
        else:
            extra_context['page_title'] = Answer_Type._meta.verbose_name + ' Add'
            
        # check for a non-modifiable elements
        protected_elements = self.get_protected_elements()
        if (object_id) and (int(object_id) in protected_elements):
            extra_context['protected_element'] = True
            extra_context['readonly_field']    = 'name'
            
            extra_context['show_goback_link']       = True
            extra_context['show_save']              = False
            extra_context['show_save_and_continue'] = False
        return super(Answer_TypeAdmin, self).changeform_view(request, object_id=object_id, form_url=form_url, extra_context=extra_context)

    # set page title/menu active of history_view (history model page)
    def history_view(self, request, object_id, extra_context=None):
        extra_context = extra_context or {}
        extra_context['page_title']   = Answer_Type._meta.verbose_name + ' History'
        extra_context['xPanel_title'] = Answer_Type._meta.verbose_name
        extra_context['model_name']   = self.model_name
        extra_context['menu_active']  = 'answer_types'
        return super(Answer_TypeAdmin, self).history_view(request, object_id=object_id, extra_context=extra_context)

    # set page title/menu active of delete_view (delete model page)
    def delete_view(self, request, object_id, extra_context=None):
        extra_context = extra_context or {}
        extra_context['page_title']   = Answer_Type._meta.verbose_name + ' Delete'
        extra_context['xPanel_title'] = Answer_Type._meta.verbose_name
        extra_context['model_name']   = self.model_name
        extra_context['menu_active']  = 'answer_types'
        return super(Answer_TypeAdmin, self).delete_view(request, object_id=object_id, extra_context=extra_context)

admin.site.register(Answer_Type, Answer_TypeAdmin)

class SkillAdmin(admin.ModelAdmin):
    model_name    = 'skill'

    list_per_page = 15
    list_display  = ('action_column', 'name', 'value')
    list_display_links = None
    list_filter   = ['name'] 
    ordering      = ['name', 'value']
    search_fields = ['name']

    # overwrite has_change_permission to grant view permission
    def has_change_permission(self, request, obj=None):
        # user can change?
        if request.user.has_perm('argo.change_skill'):
            return True

        # user can view changelist_view?
        if (not obj) and (request.user.has_perm('argo.view_skill')):
            return True

        return False

    # remove action "delete" to requester
    def get_actions(self, request):
        actions = super(SkillAdmin, self).get_actions(request)
        if (not request.user.has_perm('argo.delete_skill')) and ('delete_selected' in actions):
            del actions['delete_selected']
        return actions
    
    # define edit link for changeform_view
    def action_column(self, instance):
        change_link = u"<a href='/admin/%s/%s/%s/change' class='action_column_change' title='Change'><i class='fa fa-pencil' aria-hidden='true'></i></a>" % (instance._meta.app_label, self.model_name, instance.id)
        delete_link = u"<a href='/admin/%s/%s/%s/delete' class='action_column_delete' title='Delete'><i class='fa fa-trash' aria-hidden='true'></i></a>" % (instance._meta.app_label, self.model_name, instance.id)
        return change_link + ' ' + delete_link
    action_column.short_description = ''
    action_column.allow_tags = True
    
    # set page title/menu active of changelist (manage model page) 
    def changelist_view(self, request, extra_context=None):
        extra_context = extra_context or {}
        extra_context['page_title']   = Skill._meta.verbose_name_plural + ' Manage'
        extra_context['xPanel_title'] = Skill._meta.verbose_name_plural
        extra_context['model_name']   = self.model_name
        extra_context['permission']   = {
                                         'view':   int(request.user.has_perm('argo.view_skill')),
                                         'change': int(request.user.has_perm('argo.change_skill')),
                                         'delete': int(request.user.has_perm('argo.delete_skill'))
                                        }
        extra_context['menu_active']  = 'skill'
        return super(SkillAdmin, self).changelist_view(request, extra_context=extra_context)
        
    # set form
    def get_form(self, request, obj=None, **kwargs):
        return SkillAdminForm
        
    # set page title/menu active of changeform_view (create/update model page)
    def changeform_view(self, request, object_id=None, form_url='', extra_context=None):
        extra_context = extra_context or {}
        extra_context['xPanel_title'] = Skill._meta.verbose_name
        extra_context['model_name']   = self.model_name
        extra_context['menu_active']  = 'skill'
        if object_id:
            extra_context['page_title'] = Skill._meta.verbose_name + ' Edit'
        else:
            extra_context['page_title'] = Skill._meta.verbose_name + ' Add'
        return super(SkillAdmin, self).changeform_view(request, object_id=object_id, form_url=form_url, extra_context=extra_context)
        
    # set page title/menu active of history_view (history model page)
    def history_view(self, request, object_id, extra_context=None):
        extra_context = extra_context or {}
        extra_context['page_title']   = Skill._meta.verbose_name + ' History'
        extra_context['xPanel_title'] = Skill._meta.verbose_name
        extra_context['model_name']   = self.model_name
        extra_context['menu_active']  = 'skill'
        return super(SkillAdmin, self).history_view(request, object_id=object_id, extra_context=extra_context)

    # set page title/menu active of delete_view (delete model page)
    def delete_view(self, request, object_id, extra_context=None):
        extra_context = extra_context or {}
        extra_context['page_title']   = Skill._meta.verbose_name + ' Delete'
        extra_context['xPanel_title'] = Skill._meta.verbose_name
        extra_context['model_name']   = self.model_name
        extra_context['menu_active']  = 'skill'
        return super(SkillAdmin, self).delete_view(request, object_id=object_id, extra_context=extra_context)
        
admin.site.register(Skill, SkillAdmin)

class ProfileAdmin(admin.ModelAdmin):
    model_name    = 'profile'

    list_per_page = 15
    list_display  = ('action_column', 'description',)
    list_display_links = None
    list_filter   = [] 
    ordering      = ['description']
    search_fields = ['description']

    # overwrite has_change_permission to grant view permission
    def has_change_permission(self, request, obj=None):
        # user can change?
        if request.user.has_perm('argo.change_profile'):
            return True

        # user can view changelist_view?
        if (not obj) and (request.user.has_perm('argo.view_profile')):
            return True

        return False

    # remove action "delete" to requester
    def get_actions(self, request):
        actions = super(ProfileAdmin, self).get_actions(request)
        if (not request.user.has_perm('argo.delete_profile')) and ('delete_selected' in actions):
            del actions['delete_selected']
        return actions
    
    # define edit link for changeform_view
    def action_column(self, instance):
        change_link = u"<a href='/admin/%s/%s/%s/change' class='action_column_change' title='Change'><i class='fa fa-pencil' aria-hidden='true'></i></a>" % (instance._meta.app_label, self.model_name, instance.id)
        delete_link = u"<a href='/admin/%s/%s/%s/delete' class='action_column_delete' title='Delete'><i class='fa fa-trash' aria-hidden='true'></i></a>" % (instance._meta.app_label, self.model_name, instance.id)
        return change_link + ' ' + delete_link
    action_column.short_description = ''
    action_column.allow_tags = True
    
    # set page title/menu active of changelist (manage model page) 
    def changelist_view(self, request, extra_context=None):
        extra_context = extra_context or {}
        extra_context['page_title']   = Profile._meta.verbose_name_plural + ' Manage'
        extra_context['xPanel_title'] = Profile._meta.verbose_name_plural
        extra_context['model_name']   = self.model_name
        extra_context['permission']   = {
                                         'view':   int(request.user.has_perm('argo.view_profile')),
                                         'change': int(request.user.has_perm('argo.change_profile')),
                                         'delete': int(request.user.has_perm('argo.delete_profile'))
                                        }
        extra_context['menu_active']  = 'profile'
        return super(ProfileAdmin, self).changelist_view(request, extra_context=extra_context)
        
    # set form
    def get_form(self, request, obj=None, **kwargs):
        return ProfileAdminForm
        
    # set page title/menu active of changeform_view (create/update model page)
    def changeform_view(self, request, object_id=None, form_url='', extra_context=None):
        extra_context = extra_context or {}
        extra_context['xPanel_title'] = Profile._meta.verbose_name
        extra_context['model_name']   = self.model_name
        extra_context['menu_active']  = 'profile'
        if object_id:
            extra_context['page_title'] = Profile._meta.verbose_name + ' Edit'
        else:
            extra_context['page_title'] = Profile._meta.verbose_name + ' Add'
        return super(ProfileAdmin, self).changeform_view(request, object_id=object_id, form_url=form_url, extra_context=extra_context)
        
    # set page title/menu active of history_view (history model page)
    def history_view(self, request, object_id, extra_context=None):
        extra_context = extra_context or {}
        extra_context['page_title']   = Profile._meta.verbose_name + ' History'
        extra_context['xPanel_title'] = Profile._meta.verbose_name
        extra_context['model_name']   = self.model_name
        extra_context['menu_active']  = 'profile'
        return super(ProfileAdmin, self).history_view(request, object_id=object_id, extra_context=extra_context)

    # set page title/menu active of delete_view (delete model page)
    def delete_view(self, request, object_id, extra_context=None):
        extra_context = extra_context or {}
        extra_context['page_title']   = Profile._meta.verbose_name + ' Delete'
        extra_context['xPanel_title'] = Profile._meta.verbose_name
        extra_context['model_name']   = self.model_name
        extra_context['menu_active']  = 'profile'
        return super(ProfileAdmin, self).delete_view(request, object_id=object_id, extra_context=extra_context)
        
admin.site.register(Profile, ProfileAdmin)

class Profile_SkillAdmin(admin.ModelAdmin):
    model_name    = 'profile_skill'

    list_per_page = 15
    list_display  = ('action_column', 'profile', 'skill')
    list_display_links = None
    list_filter   = ['profile'] 
    ordering      = ['profile', 'skill']
    search_fields = ['profile__description', 'skill__name']

    # overwrite has_change_permission to grant view permission
    def has_change_permission(self, request, obj=None):
        # user can change?
        if request.user.has_perm('argo.change_profile_skill'):
            return True

        # user can view changelist_view?
        if (not obj) and (request.user.has_perm('argo.view_profile_skill')):
            return True

        return False

    # remove action "delete" to requester
    def get_actions(self, request):
        actions = super(Profile_SkillAdmin, self).get_actions(request)
        if (not request.user.has_perm('argo.delete_profile_skill')) and ('delete_selected' in actions):
            del actions['delete_selected']
        return actions
    
    # define edit link for changeform_view
    def action_column(self, instance):
        change_link = u"<a href='/admin/%s/%s/%s/change' class='action_column_change' title='Change'><i class='fa fa-pencil' aria-hidden='true'></i></a>" % (instance._meta.app_label, self.model_name, instance.id)
        delete_link = u"<a href='/admin/%s/%s/%s/delete' class='action_column_delete' title='Delete'><i class='fa fa-trash' aria-hidden='true'></i></a>" % (instance._meta.app_label, self.model_name, instance.id)
        return change_link + ' ' + delete_link
    action_column.short_description = ''
    action_column.allow_tags = True
    
    # set page title/menu active of changelist (manage model page) 
    def changelist_view(self, request, extra_context=None):
        extra_context = extra_context or {}
        extra_context['page_title']   = Profile_Skill._meta.verbose_name_plural + ' Manage'
        extra_context['xPanel_title'] = Profile_Skill._meta.verbose_name_plural
        extra_context['model_name']   = self.model_name
        extra_context['permission']   = {
                                         'view':   int(request.user.has_perm('argo.view_profile_skill')),
                                         'change': int(request.user.has_perm('argo.change_profile_skill')),
                                         'delete': int(request.user.has_perm('argo.delete_profile_skill'))
                                        }
        extra_context['menu_active']  = 'profile_skill'
        return super(Profile_SkillAdmin, self).changelist_view(request, extra_context=extra_context)
        
    # set form
    def get_form(self, request, obj=None, **kwargs):
        return Profile_SkillAdminForm
        
    # set page title/menu active of changeform_view (create/update model page)
    def changeform_view(self, request, object_id=None, form_url='', extra_context=None):
        extra_context = extra_context or {}
        extra_context['xPanel_title'] = Profile_Skill._meta.verbose_name
        extra_context['model_name']   = self.model_name
        extra_context['menu_active']  = 'profile_skill'
        if object_id:
            extra_context['page_title'] = Profile_Skill._meta.verbose_name + ' Edit'
        else:
            extra_context['page_title'] = Profile_Skill._meta.verbose_name + ' Add'
        return super(Profile_SkillAdmin, self).changeform_view(request, object_id=object_id, form_url=form_url, extra_context=extra_context)
        
    # set page title/menu active of history_view (history model page)
    def history_view(self, request, object_id, extra_context=None):
        extra_context = extra_context or {}
        extra_context['page_title']   = Profile_Skill._meta.verbose_name + ' History'
        extra_context['xPanel_title'] = Profile_Skill._meta.verbose_name
        extra_context['model_name']   = self.model_name
        extra_context['menu_active']  = 'profile_skill'
        return super(Profile_SkillAdmin, self).history_view(request, object_id=object_id, extra_context=extra_context)

    # set page title/menu active of delete_view (delete model page)
    def delete_view(self, request, object_id, extra_context=None):
        extra_context = extra_context or {}
        extra_context['page_title']   = Profile_Skill._meta.verbose_name + ' Delete'
        extra_context['xPanel_title'] = Profile_Skill._meta.verbose_name
        extra_context['model_name']   = self.model_name
        extra_context['menu_active']  = 'profile_skill'
        return super(Profile_SkillAdmin, self).delete_view(request, object_id=object_id, extra_context=extra_context)
        
admin.site.register(Profile_Skill, Profile_SkillAdmin)

# Django_auth
class UserAdmin(admin.ModelAdmin):
    model_name    = 'user'

    list_per_page = 15
    list_display  = ('action_column', 'username', 'email', 'first_name', 'last_name', 'is_active', 'get_group', 'is_staff', 'is_superuser')
    list_display_links = None
    list_filter   = ['groups', 'is_active', 'is_staff', 'is_superuser'] 
    search_fields = ['username', 'first_name', 'last_name']
    
    # remove action "delete" to requester
    def get_actions(self, request):
        actions = super(UserAdmin, self).get_actions(request)
        if (not request.user.has_perm('auth.delete_user')) and ('delete_selected' in actions):
            del actions['delete_selected']
        return actions

    # group list in view list
    def get_group(self, user):
        groups = []
        for group in user.groups.all():
            groups.append(group.name)
        return ' '.join(groups)
    get_group.short_description = 'Groups'
    
    # define edit link for changeform_view
    def action_column(self, instance):
        change_link = u"<a href='/admin/%s/%s/%s/change' class='action_column_change' title='Change'><i class='fa fa-pencil' aria-hidden='true'></i></a>" % (instance._meta.app_label, self.model_name, instance.id)
        delete_link = u"<a href='/admin/%s/%s/%s/delete' class='action_column_delete' title='Delete'><i class='fa fa-trash' aria-hidden='true'></i></a>" % (instance._meta.app_label, self.model_name, instance.id)
        return change_link + ' ' + delete_link
    action_column.short_description = ''
    action_column.allow_tags = True
    
    # set page title/menu active of changelist (manage model page)
    def changelist_view(self, request, extra_context=None):
        extra_context = extra_context or {}
        extra_context['page_title']   = 'Users Manage'
        extra_context['xPanel_title'] = 'Users'
        extra_context['model_name']   = self.model_name
        extra_context['permission']   = {
                                         'view':   int(request.user.has_perm('auth.change_user')),
                                         'change': int(request.user.has_perm('auth.change_user')),
                                         'delete': int(request.user.has_perm('auth.delete_user'))
                                        }
        extra_context['menu_active']  = 'user'
        return super(UserAdmin, self).changelist_view(request, extra_context=extra_context)
        
    # set form
    def get_form(self, request, obj=None, **kwargs):
        if obj:
            return UserUpdateForm
        else:
            return UserForm
           
    # set fields of form               
    def get_fieldsets(self, request, obj=None):
        if obj:
            return [
                      (None,     {'fields': ['first_name', 'last_name', 'username', 'email', 'is_active', 'is_staff', 'is_superuser']}),
                      ('Groups', {'fields': ['groups']}),
                   ]
        else:
            return [
                      (None,                {'fields': ['first_name', 'last_name', 'username', 'email', 'is_active', 'is_staff', 'is_superuser']}),
                      ('Password & Groups', {'fields': ['password', 'groups']}),
                   ]
                   
    # overwrite save method
    def save_model(self, request, obj, form, change):
        if (not change):
            obj.set_password(form.cleaned_data['password'])
            if (form.cleaned_data['is_active']):
                send_mail('ARGO - Start Now!', 'Hi ' + form.cleaned_data['first_name'] + ' ' + form.cleaned_data['last_name'] + ',\nYour profile has been activated, now you can start entering campaigns.\n\nBest regards\nARGO TEAM', settings.EMAIL_FROM, [form.cleaned_data['email']], fail_silently=False)
            
        elif ((obj.groups.filter(name__iexact='requester').exists()) and ('is_active' in form.changed_data) and (form.cleaned_data['is_active'])):
            send_mail('ARGO - Start Now!', 'Hi ' + form.cleaned_data['first_name'] + ' ' + form.cleaned_data['last_name'] + ',\nYour profile has been activated, now you can start entering campaigns.\n\nBest regards\nARGO TEAM', settings.EMAIL_FROM, [form.cleaned_data['email']], fail_silently=False)
            
        super(UserAdmin, self).save_model(request, obj, form, change) 
        
    # set page title/menu active of changeform_view (create/update model page)
    def changeform_view(self, request, object_id=None, form_url='', extra_context=None):
        extra_context = extra_context or {}
        extra_context['xPanel_title'] = 'User'
        extra_context['model_name']   = self.model_name
        extra_context['menu_active']  = 'user'
        if object_id:
            extra_context['page_title']     = 'User Edit'
            extra_context['readonly_field'] = 'username,groups'
        else:
            extra_context['page_title']     = 'User Add'
        return super(UserAdmin, self).changeform_view(request, object_id=object_id, form_url=form_url, extra_context=extra_context)
        
    # set page title/menu active of history_view (history model page)
    def history_view(self, request, object_id, extra_context=None):
        extra_context = extra_context or {}
        extra_context['page_title']   = 'User History'
        extra_context['xPanel_title'] = 'User'
        extra_context['model_name']   = self.model_name
        extra_context['menu_active']  = 'user'
        return super(UserAdmin, self).history_view(request, object_id=object_id, extra_context=extra_context)

    # set page title/menu active of delete_view (delete model page)
    def delete_view(self, request, object_id, extra_context=None):
        extra_context = extra_context or {}
        extra_context['page_title']   = 'User Delete'
        extra_context['xPanel_title'] = 'User'
        extra_context['model_name']   = self.model_name
        extra_context['menu_active']  = 'user'
        return super(UserAdmin, self).delete_view(request, object_id=object_id, extra_context=extra_context)
        
admin.site.unregister(User)
admin.site.register(User, UserAdmin)

class GroupAdmin(admin.ModelAdmin):
    model_name    = 'group'

    list_per_page = 15
    list_display  = ('action_column', 'name',)
    list_display_links = None
    list_filter   = []
    search_fields = ['name',]
    
    # non-modifiable or removable elements
    def get_protected_elements(self):
        protected_elements = ['workers', 'requester', 'administrator']
        ret = []
        for elm in protected_elements:
            obj = Group.objects.filter(name__iexact=elm)[0]
            ret.append(obj.id)
        return ret

    # overwrite has_delete_permission to protect non-removable elements
    def has_delete_permission(self, request, obj=None):
        protected_elements = self.get_protected_elements()
        
        # delete in changeform_view
        if obj!=None:
            for elm in protected_elements:
                if elm == getattr(obj, 'id'):
                    return False

        # delete in changelist_view 
        if request.POST and request.POST.get('action') == 'delete_selected':
            for elm in protected_elements:
                if str(elm) in request.POST.getlist('_selected_action'):
                    return False

        # user can delete?
        if request.user.has_perm('auth.delete_group'):
            return True
            
        return False
        
    # remove action "delete" to requester
    def get_actions(self, request):
        actions = super(GroupAdmin, self).get_actions(request)
        if (not request.user.has_perm('auth.delete_group')) and ('delete_selected' in actions):
            del actions['delete_selected']
        return actions
          
    # define edit link for changeform_view
    def action_column(self, instance):
        change_link = u"<a href='/admin/%s/%s/%s/change' class='action_column_change' title='Change'><i class='fa fa-pencil' aria-hidden='true'></i></a>" % (instance._meta.app_label, self.model_name, instance.id)
        delete_link = u"<a href='/admin/%s/%s/%s/delete' class='action_column_delete' title='Delete'><i class='fa fa-trash' aria-hidden='true'></i></a>" % (instance._meta.app_label, self.model_name, instance.id)
        return change_link + ' ' + delete_link
    action_column.short_description = ''
    action_column.allow_tags = True
    
    # set page title/menu active of changelist (manage model page)
    def changelist_view(self, request, extra_context=None):
        extra_context = extra_context or {}
        extra_context['page_title']   = 'Groups Manage'
        extra_context['xPanel_title'] = 'Groups'
        extra_context['menu_active']  = 'group'
        extra_context['permission']   = {
                                         'view':   int(request.user.has_perm('auth.change_user')),
                                         'change': int(request.user.has_perm('auth.change_user')),
                                         'delete': int(request.user.has_perm('auth.delete_user'))
                                        }
        extra_context['model_name']   = self.model_name
        return super(GroupAdmin, self).changelist_view(request, extra_context=extra_context)
        
    # set form
    def get_form(self, request, obj=None, **kwargs):
        return GroupForm
        
    # set page title/menu active of changeform_view (create/update model page)
    def changeform_view(self, request, object_id=None, form_url='', extra_context=None):
        extra_context = extra_context or {}
        extra_context['xPanel_title'] = 'Group'
        extra_context['model_name']   = self.model_name
        if object_id:
            extra_context['page_title']  = 'Group Edit'
            extra_context['menu_active'] = 'group'
        else:
            extra_context['page_title']  = 'Group Add'
            extra_context['menu_active'] = 'group_add'
            
        # check for a non-modifiable elements
        protected_elements = self.get_protected_elements()
        if (object_id) and (int(object_id) in protected_elements):
            extra_context['protected_element'] = True
            extra_context['readonly_field']    = 'name,permissions'
            
            extra_context['show_goback_link']       = True
            extra_context['show_save']              = False
            extra_context['show_save_and_continue'] = False
        return super(GroupAdmin, self).changeform_view(request, object_id=object_id, form_url=form_url, extra_context=extra_context)
        
    # set page title/menu active of history_view (history model page)
    def history_view(self, request, object_id, extra_context=None):
        extra_context = extra_context or {}
        extra_context['page_title']   = 'Group History'
        extra_context['xPanel_title'] = 'Group'
        extra_context['model_name']   = self.model_name
        extra_context['menu_active']  = 'group'
        return super(GroupAdmin, self).history_view(request, object_id=object_id, extra_context=extra_context)

    # set page title/menu active of delete_view (delete model page)
    def delete_view(self, request, object_id, extra_context=None):
        extra_context = extra_context or {}
        extra_context['page_title']   = 'Group Delete'
        extra_context['xPanel_title'] = 'Group'
        extra_context['model_name']   = self.model_name
        extra_context['menu_active']  = 'group'
        return super(GroupAdmin, self).delete_view(request, object_id=object_id, extra_context=extra_context)
        
admin.site.unregister(Group)
admin.site.register(Group, GroupAdmin)
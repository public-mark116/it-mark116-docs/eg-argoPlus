// show correct button in action column
function checkActionColumn(changePerm, deletePerm) {
  if(changePerm==1) {
    $(".action_column_change").show();
  }
  if(deletePerm==1) {
    $(".action_column_delete").show();
  }
}

// apply filter to change list
function filter(filterName) {
  currentUrl     = location.href;
  currentUrlPart = currentUrl.split("?");
  filterValue    = $("form#" + filterName + " select[name=filter_value]").val();
  
  location.href  = currentUrlPart[0] + filterValue;
}

// reset all filter in change list
function filterReset() {
  currentUrl     = location.href;
  currentUrlPart = currentUrl.split("?");
  
  location.href  = currentUrlPart[0];
}

(function($) {
  $(document).ready(function() {
    $(".changelist-filter-item select").each(function() {
      // convert select to select2
      $("#" + this.id).select2();
      
      // show "Reset all filter" link
      if(this.value!="?") {
        $(".changelist-filter-reset").show();
      }
    });
  });
})(django.jQuery);
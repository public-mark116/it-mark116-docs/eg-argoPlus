/**
 * Resize function without multiple trigger
 *
 * Usage:
 * $(window).smartresize(function(){
 *     // code here
 * });
 */
(function($, sr) {
  // debouncing function from John Hann
  // http://unscriptable.com/index.php/2009/03/20/debouncing-javascript-methods/
  var debounce = function(func, threshold, execAsap) {
    var timeout;

    return function debounced() {
      var obj  = this;
      var args = arguments;

      function delayed() {
        if (!execAsap)
          func.apply(obj, args);
        timeout = null;
      }

      if (timeout)
        clearTimeout(timeout);
      else if (execAsap)
        func.apply(obj, args);

      timeout = setTimeout(delayed, threshold || 100);
    };
  };

  // smartresize
  jQuery.fn[sr] = function(fn) {
    return fn ? this.bind('resize', debounce(fn)) : this.trigger(sr);
  };
})(jQuery, 'smartresize');

/**
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var CURRENT_URL   = window.location.href.split('#')[0].split('?')[0],
    $BODY         = $('body'),
    $MENU_TOGGLE  = $('#menu_toggle'),
    $RIGHT_COL    = $('.right_col'),
    $SIDEBAR_MENU = $('#sidebar-menu'),
    $LEFT_COL     = $('.left_col'),
    $CONTAINER    = $('#container'),
    $HEADER       = $('header'),
    $FOOTER       = $('footer');


// SIDEBAR
function init_sidebar() {
  /* FOOTER DOWN */
  var setContentHeight = function() {
    $RIGHT_COL.css('min-height', $(window).height());

    var bodyHeight      = $BODY.outerHeight();
    var containerMargin = parseInt($CONTAINER.css('margin-bottom'));
    var footerHeight    = $FOOTER.outerHeight();
  
    var contentHeight = bodyHeight - containerMargin - footerHeight;
    $CONTAINER.css('min-height', contentHeight);
  };
  /* FOOTER DOWN */

  
  /* SIDEBAR MENU & SUB-MENU */
  $SIDEBAR_MENU.find('a').on('click', function(ev) {
    var $li = $(this).parent();

    if ($li.is('.open')) {
      // close menu
      $li.removeClass('open');
      $li.find('span.fa-plus-minus').removeClass('fa-minus').addClass('fa-plus');
      $('ul:first', $li).slideUp(function() {setContentHeight();});
    }
    else {
      // open menu
      if (!$li.parent().is('.child_menu')) {
        // close other
        $SIDEBAR_MENU.find('li').removeClass('open');
        $SIDEBAR_MENU.find('span.fa-plus-minus').removeClass('fa-minus').addClass('fa-plus');
        $SIDEBAR_MENU.find('li ul').slideUp();
      }
      
      if (!$li.parent().is('.child_menu') || !$BODY.is('.nav-sm')) {
        // open if is main menu or if is not mobile
        $li.addClass('open');
        $li.children('a').children('span.fa-plus-minus').removeClass('fa-plus').addClass('fa-minus');
        $('ul:first', $li).slideDown(function() {setContentHeight();});
      }
    }
  });
  /* SIDEBAR MENU & SUB-MENU */

  
  /* TOGGLE MENU */
  $MENU_TOGGLE.on('click', function() {
    if ($BODY.hasClass('nav-md')) {
      // close sidebar
      $SIDEBAR_MENU.find('li.open ul').hide();
      $SIDEBAR_MENU.find('li.open').removeClass('open');
      $SIDEBAR_MENU.find('span.fa-plus-minus').removeClass('fa-minus').addClass('fa-plus');
    }
    else {
      // open sidebar
      $SIDEBAR_MENU.find('li.open-sm ul').show();
    }

    // edit class and recalculate content height
    $BODY.toggleClass('nav-md nav-sm');
    setContentHeight();
  });
  /* TOGGLE MENU */
  
  
  /* SET ACTIVE MENU */
  // search and active menu
  if ($SIDEBAR_MENU.find('.menu_in_use').length) {
    var $SIDEBAR_MENU_CURRENT = $SIDEBAR_MENU.find('.menu_in_use a');
  }
  else {
    var $SIDEBAR_MENU_CURRENT = $SIDEBAR_MENU.find('a').filter(function() {return this.href == CURRENT_URL;});
  }

  $SIDEBAR_MENU_CURRENT.parent('li').addClass('current-page');
  $SIDEBAR_MENU_CURRENT.parents('ul').slideDown(function() {
    $SIDEBAR_MENU_CURRENT.parents('li.open').find('ul').not($SIDEBAR_MENU_CURRENT.parents('ul')).hide();
    setContentHeight();
  });
  $SIDEBAR_MENU_CURRENT.parent('li').parents('li').addClass('open active');
  $SIDEBAR_MENU_CURRENT.parent('li').parents('li').children('a').children('span.fa-plus-minus').removeClass('fa-plus').addClass('fa-minus');
  /* SET ACTIVE MENU */
  
  
  /* FIX HEIGHT WHEN START AND WHEN RESIZE */
  $(window).smartresize(function() {
    setContentHeight();
  });
  
  setContentHeight();
  /* FIX HEIGHT WHEN START AND WHEN RESIZE */
  
  
  /* FIXED SIDEBAR */
  if ($.fn.mCustomScrollbar) {
    $('.menu_fixed').mCustomScrollbar({
      theme:'minimal',
      mouseWheel:{preventDefault: true},
      scrollInertia:50,
      autoHideScrollbar:true
    });
  }
  /* FIXED SIDEBAR */
};
// SIDEBAR


// X PANEL
$(document).ready(function() {
  $('.collapse-link').on('click', function() {
    var $BOX_PANEL   = $(this).closest('.x_panel');
    var $ICON        = $(this).find('i');
    var $BOX_CONTENT = $BOX_PANEL.find('.x_content');
    
    // fix for some div with hardcoded fix class
    if ($BOX_PANEL.attr('style')) {
      $BOX_CONTENT.slideToggle(200, function() {$BOX_PANEL.removeAttr('style');});
    } else {
      $BOX_CONTENT.slideToggle(200);
      $BOX_PANEL.css('height', 'auto');
    }

    $ICON.toggleClass('fa-chevron-up fa-chevron-down');
  });
});
// X PANEL


// TABLE
/* FIX ACTION SELECT */
$('#changelist-form .actions select').addClass('form-control');
$('#changelist-form .actions select option[value=delete_selected]').html('Delete Selected');
/* FIX ACTION SELECT */

/* MANAGE CHECKBOX */
$('#result_list tbody .action-checkbox input').on('change', function() {
  if ($(this).is(':checked')) {
    $(this).parent().parent().addClass('row-selected');
  }
  else {
    $(this).parent().parent().removeClass('row-selected');
  }

  countChecked();
});
$('#result_list thead .action-checkbox-column input').on('change', function() {
  if ($(this).is(':checked')) {
    $('#result_list tbody .action-checkbox input:checkbox').prop('checked', true);
    $('#result_list tbody tr').addClass('row-selected');
  }
  else {
    $('#result_list tbody .action-checkbox input:checkbox').prop('checked', false);
    $('#result_list tbody tr').removeClass('row-selected');
  }
  
  var numOfCheckbox        = $('#result_list tbody .action-checkbox input:checkbox').length;
  var numOfCheckedCheckbox = $('#result_list tbody .action-checkbox input:checkbox:checked').length;
  
  $('#changelist-form .actions .action-counter').html(numOfCheckedCheckbox + ' of ' + numOfCheckbox + ' selected');
});

function countChecked() {
  var numOfCheckbox        = $('#result_list tbody .action-checkbox input:checkbox').length;
  var numOfCheckedCheckbox = $('#result_list tbody .action-checkbox input:checkbox:checked').length;
    
  $('#changelist-form .actions .action-counter').html(numOfCheckedCheckbox + ' of ' + numOfCheckbox + ' selected');
  
  var numOfNotCheckedCheckbox = numOfCheckbox - numOfCheckedCheckbox;
  if (numOfNotCheckedCheckbox==0) {
    $('#result_list thead .action-checkbox-column input:checkbox').prop('checked', true);
  }
  else {
    $('#result_list thead .action-checkbox-column input:checkbox').prop('checked', false);
  }
}
/* MANAGE CHECKBOX */
// TABLE


$(document).ready(function() {
    init_sidebar();
});


$(window).resize(function() {
    /* SETUP SELECT2 WIDTH*/
    $('.select2.select2-container').css('width', '100%');
    /* SETUP SELECT2 WIDTH*/
});
(function($) {
  $(document).ready(function() {
    /* COLLAPSE FIELDSET */
    // if start collapse
    $('.div_fieldset.collapse').parent('fieldset').find('.fieldset_toggle').removeClass('fa-eye-slash').addClass('fa-eye');
    
    $('.collapse-fieldset').on('click', function() {
      var $FIELDSET = $(this).parents('fieldset');
      if ($FIELDSET.find('.div_fieldset').is(':visible')) {
        $FIELDSET.find('.div_fieldset').hide();
        $FIELDSET.find('.fieldset_toggle').removeClass('fa-eye-slash').addClass('fa-eye');
      }
      else {
        $FIELDSET.find('.div_fieldset').show();
        $FIELDSET.find('.fieldset_toggle').removeClass('fa-eye').addClass('fa-eye-slash');
      }
    });
    /* COLLAPSE FIELDSET */
    
    
    /* FIX READONLY FIELD */
    if($('#readonly_field_list').length && $('#readonly_field_list').val().length>0) {
      var readonly_field = $('#readonly_field_list').val();
      var readonly_field_list = readonly_field.split(',');
      $.each(readonly_field_list, function() {
        if($('form input#id_' + this).attr('type') == "text" || $('form input#id_' + this).attr('type') == "number") {
          var value = $('form input#id_' + this).val();

          $('form input#id_' + this).attr('type', 'hidden');
          $('form input#id_' + this).parent('div').append('<div class="readonly form-control">' + value + '</div>');
          $('form input#id_' + this).parents('.form-group').find('.help').remove();
        }
        else if($('form input#id_' + this).attr('type') == "checkbox") {
          var value = $('form input#id_' + this).is(":checked");
          value = value ? 1 : 0;

          $('form input#id_' + this).attr('type', 'hidden').val(value);
          $('form input#id_' + this).parent('div').append('<div class="readonly form-control">' + (value ? 'Yes' : 'No') + '</div>');
          $('form input#id_' + this).parents('.form-group').find('.help').remove();
        }
        else if($('form select#id_' + this).length) {
          var value1 = $('form select#id_' + this).find(":selected").val();
          var value2 = $('form select#id_' + this).find(":selected").text();
          
          $('form select#id_' + this).parent('div').append('<input type="hidden" name="' + this + '" id="id_' + this + '" value="' + value1 + '">');
          $('form select#id_' + this).parent('div').append('<div class="readonly form-control">' + value2 + '</div>');
          $('form select#id_' + this).remove();
          $('form input#id_' + this).parents('.form-group').find('.help').remove();
        }
        else {
          console.log("Error: Unexpected readonly type field (" + this + ")")
        }
      });
    }
    if($('#readonly_inline_field_list').length && $('#readonly_inline_field_list').val().length>0) {
      var readonly_field = jQuery.parseJSON($('#readonly_inline_field_list').val());
      $.each(readonly_field, function(key, value) {
        var readonly_inline     = value['inline'];
        var readonly_field_list = value['fields'].split(',');
        var input_number        = $('#id_' + readonly_inline + '_set-INITIAL_FORMS').val();
        
        $.each(readonly_field_list, function() {
          for(var i=0; i<input_number; i++) {
            if($('div#' + readonly_inline + '_set-group select#id_' + readonly_inline + '_set-' + i + '-' + this).length) {
              var value1 = $('div#' + readonly_inline + '_set-group select#id_' + readonly_inline + '_set-' + i + '-' + this).find(":selected").val();
              var value2 = $('div#' + readonly_inline + '_set-group select#id_' + readonly_inline + '_set-' + i + '-' + this).find(":selected").text();
              
              $('div#' + readonly_inline + '_set-group select#id_' + readonly_inline + '_set-' + i + '-' + this).parent('div').append('<input type="hidden" name="' + readonly_inline + '_set-' + i + '-' + this + '" id="id_' + readonly_inline + '_set-' + i + '-' + this + '" value="' + value1 + '">');
              $('div#' + readonly_inline + '_set-group select#id_' + readonly_inline + '_set-' + i + '-' + this).parent('div').append('<div class="readonly form-control">' + value2 + '</div>');
              $('div#' + readonly_inline + '_set-group select#id_' + readonly_inline + '_set-' + i + '-' + this).remove();
              $('div#' + readonly_inline + '_set-group input#id_' + readonly_inline + '_set-' + i + '-' + this).parents('.form-group').find('.help').remove();
            }
            else {
              console.log("Error: Unexpected readonly type field (" + this + ")")
            }
          }
        });

      });
    }
    /* FIX READONLY FIELD */
    
    
    /* FIX REQUIRED FIELD */
    $('form .form-control.required').parents('.form-group').find('label').addClass('labelRequired');
    /* FIX REQUIRED FIELD */
    
    
    /* TRANSFORM SELECT IN SELECT2 */
    $("form.changeform > fieldset select").each(function() {
      $("#" + this.id).select2();
    });
    /* TRANSFORM SELECT IN SELECT2 */
    
    
    /* SET ADD "inline-related" BUTTON */
    $('.js-inline-admin-formset .add-row a').addClass('btn btn-success');
    $('.js-inline-admin-formset .add-row a').html('<i class="fa fa-plus" aria-hidden="true"></i> Add Another');
    /* SET ADD "inline-related" BUTTON */
    
    
    /* MANAGE FIRST "inline-related" ELEMENT */
    $('.js-inline-admin-formset .inline-related.empty-form').append('<div class="inline-related-empty-hover" title="Click to add another"></div>');
    $('.js-inline-admin-formset .inline-related.empty-form .inline-related-empty-hover').click(function(e) {
      var currentformsetName = $(e.target).parents('.js-inline-admin-formset').attr('id');
      $('.js-inline-admin-formset#' + currentformsetName + ' .add-row a').trigger('click');
    });
    /* MANAGE FIRST "inline-related" ELEMENT */
    
    
    /* EXECUTE FUNCTION */
    if($('#task_form').length) {
      // set 'choice inline element' visibility
      if($('#task_form select#id_answer_type').length) {
        // create
        var value = $('#id_answer_type').find(":selected").text();
        if(value=="Choice") {
          $('#task_form #task_choice_set-group').show();
        }
        else {
          $('#task_form #task_choice_set-group').hide();
        }
      }
      else {
        // update
        var value = $('#task_form #id_answer_type').parent('div').children('div.readonly').text();
        if(value=='Choice') {
          $('#task_form #task_choice_set-group').show();
        }
        else {
          $('#task_form #task_choice_set-group').hide();
        }
      }

      // re-set 'choice inline element' visibility whene answer type change
      $('#task_form #id_answer_type').select2().on('change', function () {
        var value = $(this).find('option:selected').text();
        if(value=='Choice') {
          $('#task_form #task_choice_set-group').show();
        }
        else {
          $('#task_form #task_choice_set-group').hide();
        }
      });
    }
    /* EXECUTE FUNCTION */
  });
})(django.jQuery);
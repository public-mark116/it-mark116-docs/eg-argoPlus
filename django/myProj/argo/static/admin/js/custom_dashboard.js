$(document).ready(function() {
  if($('#dashboard_graph_campaignsMostAnswers_data').length) {
    var graph1_data = jQuery.parseJSON($('#dashboard_graph_campaignsMostAnswers_data').val());
    var graph1Chart_title  = [];
    var graph1Chart_value1 = [];
    var graph1Chart_value2 = [];
    $.each(graph1_data, function(index, value) {
      graph1Chart_title.push(value[0]);
      graph1Chart_value1.push(value[1])
      graph1Chart_value2.push(value[2])
    });

    var graph1 = document.getElementById('dashboard_graph_campaignsMostAnswers');
    new Chart(graph1, {
      type: 'bar',
      data: {
        labels: graph1Chart_title,
        datasets: [{
          data: graph1Chart_value1,
          label: '# of workers',
          backgroundColor: '#26B99A'
        },
        {
          data: graph1Chart_value2,
          label: '# of answers',
          backgroundColor: '#005161'
        }]
      },
      options: {
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero: true
            }
          }]
        }
      }
    });
  }
  
  if($('#dashboard_graph_workersGenre_data').length) {
    var graph2_data = jQuery.parseJSON($('#dashboard_graph_workersGenre_data').val());
    var graph2Chart_title = [];
    var graph2Chart_value = [];
    $.each(graph2_data, function(index, value) {
      graph2Chart_title.push((value[0]=="M" ? "Male" : "Female"));
      graph2Chart_value.push(value[1])
    });
    
    var graph2 = document.getElementById("dashboard_graph_workersGenre");
    new Chart(graph2, {
      data: {
        labels: graph2Chart_title,
        datasets: [{
            data: graph2Chart_value,
            backgroundColor: [
              "#01CB9F",
              "#0093F5"
            ],
        }]
      },
      type: 'pie',
      otpions: {
        legend: false
      }
    });
  }
  
  if($('#dashboard_graph_workersAge_data').length) {
    var graph3_data = jQuery.parseJSON($('#dashboard_graph_workersAge_data').val());
    var graph3Chart_title = [];
    var graph3Chart_value = [];
    $.each(graph3_data, function(index, value) {
      graph3Chart_title.push(value[0]);
      graph3Chart_value.push(value[1])
    });
    
    var graph3 = document.getElementById("dashboard_graph_workersAge");
    new Chart(graph3, {
      data: {
        labels: graph3Chart_title,
        datasets: [{
            data: graph3Chart_value,
            backgroundColor: [
              "#40C2A6",
              "#4DA5DF",
              "#F4A82F",
              "#EA6254",
              "#E5E5E5"
            ],
        }]
      },
      type: 'pie',
      otpions: {
        legend: false
      }
    });
  }
});
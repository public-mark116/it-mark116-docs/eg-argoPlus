from __future__ import unicode_literals
from django.views import generic
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.shortcuts import render, redirect, render_to_response
from django.contrib.auth import authenticate, login, logout, models
from django.contrib.auth.models import User, Group
from django.views.generic import View
from .models import Worker, Campaign as CampaignModel, Worker_Campaign, Task, Task_Status, Answer_Type
from .forms import SignUpForm
from datetime import datetime
from django.conf import settings
from django.core.mail import send_mail

import json

class Signup(View):
    form_class    = SignUpForm
    template_name = 'admin/signup.html'

    def get(self, request):
        form = self.form_class()
        return render(request, self.template_name, {'form': form, 'site_header':settings.SITE_NAME, 'site_title': settings.SITE_NAME.upper()})
    
    def post(self, request):
        form      = self.form_class(request.POST)
        formError = 0
        if form.is_valid():
            user = form.save(commit=False)
            
            # user must be enabled by the administrator
            user.is_active = False
            user.is_staff  = True
            
            if User.objects.filter(username=form.cleaned_data['username']).exists():
                form.errors['username'] = "This username is already associated with an account"
                formError = 1

            if User.objects.filter(email__iexact=form.cleaned_data['email']).exists():
                form.errors['email'] = "This email is already associated with an account"
                formError = 1
                
            if formError!=0:
                return render(request, self.template_name, {'form': form, 'site_header':settings.SITE_NAME, 'site_title': settings.SITE_NAME.upper()})
                
            # check UNIMI user
            emailSplit = user.email.split('@')
            if (emailSplit[1].lower()=="unimi.it") or (emailSplit[1].lower()=="studenti.unimi.it"):
                import requests
                userData = {'username': user.email, 'password': user.password}
                response = requests.post(settings.URL_USERCHECK, data=userData)
                if response.text == "1":
                    user.is_active = True
            
            user.set_password(user.password)
            user.save()

            group = Group.objects.get(name__iexact='requester') 
            group.user_set.add(user)
            
            if user is not None:
                if user.is_active == True:
                    send_mail('ARGO - Start Now!', 'Hi ' + form.cleaned_data['first_name'] + ' ' + form.cleaned_data['last_name'] + ',\nYour profile has been activated, now you can start entering campaigns.\n\nBest regards\nARGO TEAM', settings.EMAIL_FROM, [form.cleaned_data['email']], fail_silently=False)
                else:
                    send_mail('ARGO - Sign Up', 'Hi ' + form.cleaned_data['first_name'] + ' ' + form.cleaned_data['last_name'] + ',\nYour account has been successfully created.\nYou will receive an email as soon as you can access to ARGO.\n\nBest regards\nARGO TEAM', settings.EMAIL_FROM, [form.cleaned_data['email']], fail_silently=False)
                    send_mail('ARGO - New Sign Up', 'New requester has been registered in argo.\nLog In to active the account\n\nBest regards\nARGO TEAM', settings.EMAIL_FROM, User.objects.values_list('email', flat=True).filter(groups__name__iexact='administrator'), fail_silently=False)
                return redirect('admin:index')

        return render(request, self.template_name, {'form': form, 'site_header':settings.SITE_NAME, 'site_title': settings.SITE_NAME.upper()})
        
class Privacy(View):
    template_name = 'admin/privacy.html'

    def get(self, request):
        return render(request, self.template_name, {'site_header':settings.SITE_NAME, 'site_title': settings.SITE_NAME.upper()})